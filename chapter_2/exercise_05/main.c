#include "stm32f4xx.h"
#include "print.h"

// To try this example, short circuit connect PE0 to PA0(USER PUSH-BUTTON)

// (technical paper pg.57 and pg.58)
// TIM4 CH1 -> PD12
// TIM4 CH2 -> PD13
// TIM4 CH3 -> PD14
// TIM4 CH4 -> PD15
// TIM4 ETR -> PE0

// (stm32f4 discovery board datasheet pg.36)
// PD12 -> GREEN LED
// PD13 -> ORANGE LED
// PD14 -> RED LED
// PD15 -> BLUE LED


// (reference manual pg.598)
// One-pulse mode description and setup guide
void init_TIM4_one_puls_mode()
{
	// (technical paper pg.17)
	// TIM4 is on the APB1 bus
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;

	// GPIOD and GPIOE is on the AHB1bus
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN;


	// (reference manual pg.278)
	// setup PD12, PD13, PD14 and P15 to alternate function mode
	GPIOD->MODER &= ~(GPIO_MODER_MODER12 | GPIO_MODER_MODER13 | GPIO_MODER_MODER14 | GPIO_MODER_MODER15);
	GPIOD->MODER |= GPIO_MODER_MODER12_1 | GPIO_MODER_MODER13_1 | GPIO_MODER_MODER14_1 | GPIO_MODER_MODER15_1;   


	// (reference manual pg.283)
	// set PD12, PD13, PD14 and PD15 to AF2
	// ----------------------------------------------
	// (technical paper pg.57 and pg.58)
	// see which peripherals are mapped to this pins
	GPIOD->AFR[1] &= 0x0000FFFF;
	GPIOD->AFR[1] |= 0x22220000;

	// set PE0 to alternate function mode
	GPIOE->MODER &= ~GPIO_MODER_MODER0;
	GPIOE->MODER |= GPIO_MODER_MODER0_1;

	// set PE0 to AF2 
	GPIOE->AFR[0] &= 0xFFFFFFF0;
	GPIOE->AFR[0] |= 0x00000002;


	// (reference manual pg.611)
	// CR1 <=> control register 1
	// reset CMS to value '00' -> edged-aligned mode
	// reset DIR to value '0' -> counter used as upcounter
	TIM4->CR1 &= ~(TIM_CR1_CMS | TIM_CR1_DIR);

	// set ARPE to value '1' -> TIM->ARR register is buffered
	// set OPM to value '1' -> counter stop counting on the next update event
	// 													(clearing the bit CEN)(OPM - one-pulse mode)
	// set URS to value '1' -> only counter overflow/underflow generates an update
	// 												 interrupt	
	TIM4->CR1 |= TIM_CR1_ARPE | TIM_CR1_OPM | TIM_CR1_URS;

	// (reference manual pg.614 and pg.615)
	// SMCR <=> slave mode control register	
	// ETP to value '0'b -> ETR is noninverted, active at high level or rising edge
	// ECE to value '0'b -> external clock mode 2 disabled
	// ETPS to value '00'b -> prescaler OFF
	// ETF to value '0000'b -> no filter, sampling is done at f(DTS)
	// 												 (see CR1 descriptio for more details)
	// MSM to value '0b' -> no action	
	TIM4->SMCR = 0x0; // reset the register value to all '0'

	// TS to value '111'b -> external trigger input(ETRF) is selected as trigger
	// SMS to value '110'b -> trigger mode - the counter starts at a rising edge of the trigger 
	// 												TRGI (but it is not reset). Only the start of the counting is 
	// 												controlled.
	TIM4->SMCR |= TIM_SMCR_TS | (TIM_SMCR_SMS_1 | TIM_SMCR_SMS_2);


	// (reference manual pg.621 and pg.622)
	// CCMRx <=> capture/compare mode register x
	// CC1S - Captura/Compare 1 selection: value '00'b -> CC1 is configured as output
	// CC2S - Captura/Compare 2 selection: value '00'b -> CC2 is configured as output
	TIM4->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S); 

	//OC1M - Output compare 1 mode: value '111'b -> PWM mode2
	//OC2M - Output compare 2 mode: vlaue '111'b -> PWM mode2
	//PWM mode2 -> In upcounting, channel is inactive as long as
	// TIM4->CNT < TIM->CCRx else active. In downcounting, channel 1
	// is active as long as TIM4->CNT > TIM4->CCRx else inactive.
	TIM4->CCMR1 |= TIM_CCMR1_OC1M | TIM_CCMR1_OC2M;

	// OC1PE - Output compare 1 preload enable
	// OC2PE - Output compare 2 preload enable
	// If preload is enabled, then every read/write access the preloaded register.
	// Value from the preloaded register is loaded in the active register at each
	// update event.
	TIM4->CCMR1 |= TIM_CCMR1_OC1PE | TIM_CCMR1_OC2PE;


  // setup for CCMR1 and CCMR2 is similar	
	TIM4->CCMR2 &= ~(TIM_CCMR2_CC3S | TIM_CCMR2_CC4S); 
	
	TIM4->CCMR2 |= TIM_CCMR2_OC3M | TIM_CCMR2_OC4M;

	TIM4->CCMR2 |= TIM_CCMR2_OC3PE | TIM_CCMR2_OC4PE;





	// (reference manual pg.627)
	// PSC <=> prescaler
	TIM4->PSC = 42000 - 1; // TIM4 clock is 2 * APB1_CLK = 84MHz
												 // COUNT_CLK = 84MHz / 42000 = 2KHz
	
	// (reference manual pg.627)
	// ARR <=> auto-reload register
	// The value to which counter will count, after that value is reached, CNT register will
	// reset value to 0, and generates UEV(update event).
	TIM4->ARR = 8000;

	// (reference manual pg.628)
	// CCRx <=> capture/compare register x
	// Active capture/compare register contains value to be compared to the
	// counter TIM4->CNT and signaled to OC1 output.
	TIM4->CCR1 = 1000;
	TIM4->CCR2 = 2000;
	TIM4->CCR3 = 4000;
	TIM4->CCR4 = 6000;



	// (reference manual pg.620)
	// EGR <=> event generation register
	// UG -> Re-initialize the counter and generates an update of the registers.
	TIM4->EGR |= TIM_EGR_UG;


	// (reference manual pg.625)
	// CCER <=> capture compare enable register
	// CC1E -> OC1 signal is output on the corresponding output pin
	// CC2E -> OC2 signal is output on the corresponding output pin
	// CC3E -> OC3 signal is output on the corresponding output pin
	// CC4E -> OC4 signal is output on the corresponding output pin
	// CCxP -> OCx: value '0'b -> active high 
	// 							value '1'b -> active low
	TIM4->CCER &= ~(TIM_CCER_CC1P | TIM_CCER_CC2P | TIM_CCER_CC3P | TIM_CCER_CC4P);
	TIM4->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E;

//	TIM4->CR1 |= TIM_CR1_CEN;
}

int main(void)
{
	
	volatile int i = 0;

	init_UART4();
	print_terminal("UART4 init passed\r\n");
	init_TIM4_one_puls_mode();
	print_terminal("TIM4 init passed\r\n");

	while(1)
	{
		++i;
		if(TIM4->CNT != 0)
			print_terminal("--> %d\r\n", TIM4->CNT);
	}
	return 0;
}
