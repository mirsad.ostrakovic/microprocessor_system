#include "stm32f4xx.h"
#include "string.h"

char msg[256]; 
uint8_t msg_idx = 0u;
uint8_t msg_len = 0u;

void init_UART4()
{

    // enable GPIOC module on AHB bus
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;     

    // enable UART4 module on APB1 bus
    RCC->APB1ENR |= RCC_APB1ENR_UART4EN;


    GPIOC->MODER &= 0xFF0FFFFF; // reset pins 10 and 11 mode
    GPIOC->MODER |= 0x00A00000; // set pins 10 and 11 mode to '10' - alternate function

    GPIOC->OTYPER &= 0xF3FF; // reset pins 10 and 11 output type to '0' - push-pull

    GPIOC->OSPEEDR &= 0xFF0FFFFF; // reset pins 10 and 11 output speed
    GPIOC->OSPEEDR |= 0x00A00000; // set pins 10 and 11 mode to '10' - fast speed

    GPIOC->PUPDR &= 0xFF0FFFFF; // reset pins 10 and 11 pull-up/pull-down configuration
    GPIOC->PUPDR |= 0x00500000; // set pins 10 and 11 pu/pd value to '01' - pull-up

    GPIOC->AFR[1] &= 0xFFFF00FF; // reset alternate function value of pins 10 and 11
    GPIOC->AFR[1] |= 0x00008800; // set alternate function value of pins 10 and 11 to '8' - AF8
   
    // for APB1 clk_freq = 42MHz and OVER8 = 0 and baud_rate = 115200 Bps,  BRR_value = 22.8125
    // BRR[3:0]  ->  DIV_fraction
    // BRR[15:4] ->  DIV_mantissa
    UART4->BRR = 0x016C;


		
		UART4->CR1 |=  USART_CR1_TE | USART_CR1_UE | USART_CR1_TCIE;

		// (definition in file 'core_cm4.h')
		NVIC_EnableIRQ(UART4_IRQn);


		// (reference manual pg.995)
		// UE - USART enable 
		// TCIE - transmission complete interrupt enable
		// TE - transmitter enable
 //   UART4->CR1 |=  USART_CR1_TE | USART_CR1_UE | USART_CR1_TCIE;
 
}

void UART4_IRQHandler()
{
	__disable_irq();
	if(UART4->SR & USART_SR_TC)
	{
		UART4->DR = msg[msg_idx++];

		if(msg_idx == msg_len)
			msg_idx = 0u;
	}
	__enable_irq();	
}                                            


int main(int argc, const char *argv[])
{
		
	static volatile uint32_t i = 0u;
	init_UART4();
	
	while(1)
	{
		__disable_irq();	
	}
	return 0;
}
