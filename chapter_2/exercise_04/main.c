#include "stm32f4xx.h"
#include "print.h"

// (technical paper pg.56)
// TIM5_CH1 is mapped to PA0 AF2

// (technical paper pg.17)
// TIM5 is on APB1 bus

// (reference manual pg.592)
// Input capture mode setup
void init_TIM5_input_capture_mode()
{

	// see GPIO setup in previous examples
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;

	GPIOA->MODER &= ~GPIO_MODER_MODER0;
	GPIOA->MODER |= GPIO_MODER_MODER0_1;

	GPIOA->AFR[0] |= 0x00000002; 




	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN; // enable clock to TIM5

	// (reference manual pg.577)
	// General-purpose timer block diagram for TIM2 to TIM5

	// (reference manual pg.611)
	TIM5->CR1 |= TIM_CR1_ARPE | TIM_CR1_URS; // ARR registe is buffered
																					 // only counter overflow/underflow generates UIE

	TIM5->PSC = 42000-1; // output from prescaler is 84MHz / 42000 = 2kHz
	TIM5->ARR = 0xFFFFFFFF; // can count 2,147,486s without overflow or about 25 days 

	// (reference manual pg.620)
	TIM5->EGR |= TIM_EGR_UG; // update generation which re-initialize the counter 
													 // and the others related registers
	
	
	// (reference manual pg.613)
	// TI1S: value '0' -> TIM5_CH1 pin is connected to T1 input
	// 			 value '1' -> TIM5_CH1, CH2, CH3 are connected to the T1 input (XOR combination)
	// See 'General-purpose timer block diagram for TIM2 to TIM5'
	TIM5->CR2 &= ~TIM_CR2_TI1S; // reset value to '0'

	// (reference manual pg.612 and pg.623)
	// CCMR1 <=> capture/compare mode register 1
	// CC1S: value '00' -> CC1 channel is configured as output
	//			 value '01' -> CC1 channel is configured as input, IC1 is mapped to TI1
  TIM5->CCMR1 &= ~TIM_CCMR1_CC1S;	
	TIM5->CCMR1 |= TIM_CCMR1_CC1S_0;

	// IC1F <=> input capture 1 filter
	// This bits-field defines frequency used to sample T! input and the length of 
	// the digital filter applied to TI1. The digital filter is mad of an event count
	// in which N events are needed to validate a transition on the output:
	// '0000'b -> no filter, sampling is done
	// '0011'b -> f(sampling) = f(ck_int) and N = 8
	// '1111'b -> f(sampling) = f(ck_dts)/32 and N = 8
	TIM5->CCMR1 &= ~TIM_CCMR1_IC1F;
	TIM5->CCMR1 |= 0x30; // value '0011'b 

	// (reference manual pg.625 and pg.626)
	// CCER <=> capture/compare enable register
	// CC1P <=> capture/compare 1 output polarity
	// CC1NP <=> if channel is configured as input, then this bit in conjuction with
	// 					 bit CC1P defines TI1FP1/TI1FP2 polarity
	//				=> value '00' - noninverted/rising edge
	TIM5->CCER &= ~(TIM_CCER_CC1P | TIM_CCER_CC1NE);  


	// (reference manual pg.623)
	// IC1PSC <=> input capture 1 prescaler
	// value '00' => no prescaler
	TIM5->CCMR1 &= ~TIM_CCMR1_IC1PSC;

	// CC1E <==> CC1 channel configured as input: capture enable/disable
	// This bit determines if a capture of the counter value can actually be done
	// into the input capture/compare regitser 1 (CCR1) or not.
	TIM5->CCER |= TIM_CCER_CC1E;

	// (reference manual pg.617 and pg.618)
	TIM5->DIER |= TIM_DIER_CC1IE; // enable interrupt on the capture event on CC1

	// defined in 'core_cm4.h'
	NVIC_EnableIRQ(TIM5_IRQn);

	// (reference manual pg.611 and pg.612)
	TIM5->CR1 |= TIM_CR1_CEN; // enable counter


}

uint32_t TIM5_time_point = 0u;
uint32_t TIM5_event_counter = 0u;

// defined as WEAK in 'startup_stm32f4xx.s'
void  TIM5_IRQHandler()
{

	if(TIM5->SR & TIM_SR_CC1IF) 
	{
		print_terminal("int\r\n");
		TIM5_time_point = TIM5->CCR1;
		++TIM5_event_counter;
	
		TIM5->SR &= ~TIM_SR_CC1IF;	
	}
}


int main(void)
{
	
	uint32_t last_event_counter_val;
	
	init_UART4();
	init_TIM5_input_capture_mode();


	
	
	last_event_counter_val = TIM5_event_counter;

	print_terminal("Initialization passed\r\n");

	while(1)
	{
		if(last_event_counter_val != TIM5_event_counter)
			print_terminal("Timepoint: %d\r\nEvent counter %d\r\n", TIM5_time_point, TIM5_event_counter);

		last_event_counter_val = TIM5_event_counter;

	//	print_terminal("--> Timepoint: %d\r\nEvent counter %d\r\nCounter: %d\r\n", TIM5_time_point, TIM5_event_counter, TIM5->CNT);
	}

	return 0;
}
