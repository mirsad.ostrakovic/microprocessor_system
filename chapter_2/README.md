# CHAPTER 2

## EXERCISE 5(BA)
Cilj zadatka je omoguciti trigerovanje TIM4 periferala prilikom pritiska na USER_BUTTON, nakon cega on treba upaliti:
-zelenu diodu na pinu PD12 nakon 0.5s i drzati je upaljenom 3.5s,
-narandzastu diodu na pinu PD13 nakon 1.0s i drzati je upaljenom 3.0s,
-crvenu diodu na pinu PD14 nakon 2.0s i drzati je upaljenom 2.0s,
-plavu diodu na pinu PD15 nakon 3.0s i drzati je upaljenom 1.0s.
Dakle nakon protekle 4.0s, potrebno je da se sve diode ugase i ostanu u tom stanju do novog pritiska na USER_BUTTON.
Navedenu funkcionalost potrebno je realizirati koristeci ONE-PULSE rezim rada TIM periferala. Rjesenje datog problema treba biti u potpunosti hardverski, dakle nije dozvoljeno koristenje interrupta(ISR rutina), niti setovanje bilog kojeg registra GPIO ili TIM periferala nakon pocetke inicijalizacije.

Za svrhu trigerovanja TIM4 periferala koristiti TIM4_ETR kanal/ulaz koji je povezan za PE0 pinom.
Da bi se mogao koristiti USER_BUTTON kao TIM4_ETR, potrebno je kratko spojiti pin PE0 sa pinom PA0, sa kojim je povezan USER_BUTTON.



