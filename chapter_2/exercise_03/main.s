	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"main.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.section	.rodata
	.align	2
.LC0:
	.ascii	"RCC->BDCR stat\015\012-----------------------------"
	.ascii	"-----------\015\012\000"
	.align	2
.LC1:
	.ascii	"LSE clock ON\015\012\000"
	.align	2
.LC2:
	.ascii	"LSE clock OFF\015\012\000"
	.align	2
.LC3:
	.ascii	"LSE clock ready\015\012\000"
	.align	2
.LC4:
	.ascii	"LSE clock not ready\015\012\000"
	.align	2
.LC5:
	.ascii	"LSE oscillator bypassed\015\012\000"
	.align	2
.LC6:
	.ascii	"LSE oscillator not bypassed\015\012\000"
	.align	2
.LC7:
	.ascii	"RTC clock source selection: \000"
	.align	2
.LC8:
	.ascii	"no clock\015\012\000"
	.align	2
.LC9:
	.ascii	"LSE oscillator\015\012\000"
	.align	2
.LC10:
	.ascii	"LSI oscillator\015\012\000"
	.align	2
.LC11:
	.ascii	"HSE oscillator\015\012\000"
	.align	2
.LC12:
	.ascii	"RTC clock enabled\015\012\000"
	.align	2
.LC13:
	.ascii	"RTC clock disabled\015\012\000"
	.align	2
.LC14:
	.ascii	"Reset the entire Backup domain\015\012\000"
	.align	2
.LC15:
	.ascii	"Reset not activated\015\012\000"
	.align	2
.LC16:
	.ascii	"---------------------------------------------------"
	.ascii	"----\015\012\000"
	.align	2
.LC17:
	.ascii	"RCC->CFGR stat\015\012-----------------------------"
	.ascii	"-----------\015\012\000"
	.align	2
.LC18:
	.ascii	"HSE division factor for RTC clock(need to be set on"
	.ascii	" such value that clock supplied to RTC would be 1MH"
	.ascii	"z):\000"
	.align	2
.LC19:
	.ascii	"%d\015\012\000"
	.text
	.align	2
	.global	print_RTC_stat
	.thumb
	.thumb_func
	.type	print_RTC_stat, %function
print_RTC_stat:
.LFB110:
	.file 1 "main.c"
	.loc 1 8 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 13 0
	ldr	r0, .L21
	bl	print_terminal
	.loc 1 15 0
	ldr	r3, .L21+4
	ldr	r3, [r3, #112]
	str	r3, [r7, #4]
	.loc 1 17 0
	ldr	r3, [r7, #4]
	and	r3, r3, #1
	cmp	r3, #0
	beq	.L2
	.loc 1 18 0
	ldr	r0, .L21+8
	bl	print_terminal
	b	.L3
.L2:
	.loc 1 20 0
	ldr	r0, .L21+12
	bl	print_terminal
.L3:
	.loc 1 23 0
	ldr	r3, [r7, #4]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L4
	.loc 1 24 0
	ldr	r0, .L21+16
	bl	print_terminal
	b	.L5
.L4:
	.loc 1 26 0
	ldr	r0, .L21+20
	bl	print_terminal
.L5:
	.loc 1 29 0
	ldr	r3, [r7, #4]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L6
	.loc 1 30 0
	ldr	r0, .L21+24
	bl	print_terminal
	b	.L7
.L6:
	.loc 1 32 0
	ldr	r0, .L21+28
	bl	print_terminal
.L7:
	.loc 1 35 0
	ldr	r0, .L21+32
	bl	print_terminal
	.loc 1 36 0
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #8
	and	r3, r3, #3
	cmp	r3, #3
	bhi	.L8
	adr	r2, .L10
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L10:
	.word	.L9+1
	.word	.L11+1
	.word	.L12+1
	.word	.L13+1
	.p2align 1
.L9:
	.loc 1 39 0
	ldr	r0, .L21+36
	bl	print_terminal
	.loc 1 40 0
	b	.L8
.L11:
	.loc 1 43 0
	ldr	r0, .L21+40
	bl	print_terminal
	.loc 1 44 0
	b	.L8
.L12:
	.loc 1 47 0
	ldr	r0, .L21+44
	bl	print_terminal
	.loc 1 48 0
	b	.L8
.L13:
	.loc 1 51 0
	ldr	r0, .L21+48
	bl	print_terminal
	.loc 1 52 0
	nop
.L8:
	.loc 1 55 0
	ldr	r3, [r7, #4]
	and	r3, r3, #32768
	cmp	r3, #0
	beq	.L14
	.loc 1 56 0
	ldr	r0, .L21+52
	bl	print_terminal
	b	.L15
.L14:
	.loc 1 58 0
	ldr	r0, .L21+56
	bl	print_terminal
.L15:
	.loc 1 61 0
	ldr	r3, [r7, #4]
	and	r3, r3, #65536
	cmp	r3, #0
	beq	.L16
	.loc 1 62 0
	ldr	r0, .L21+60
	bl	print_terminal
	b	.L17
.L16:
	.loc 1 64 0
	ldr	r0, .L21+64
	bl	print_terminal
.L17:
	.loc 1 67 0
	ldr	r0, .L21+68
	bl	print_terminal
	.loc 1 73 0
	ldr	r0, .L21+72
	bl	print_terminal
	.loc 1 75 0
	ldr	r0, .L21+76
	bl	print_terminal
	.loc 1 76 0
	ldr	r3, .L21+4
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #16
	and	r3, r3, #31
	cmp	r3, #1
	bhi	.L18
	.loc 1 80 0
	ldr	r0, .L21+36
	bl	print_terminal
	.loc 1 81 0
	b	.L1
.L18:
	.loc 1 84 0
	ldr	r3, .L21+4
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #16
	and	r3, r3, #31
	ldr	r0, .L21+80
	mov	r1, r3
	bl	print_terminal
.L1:
	.loc 1 87 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L22:
	.align	2
.L21:
	.word	.LC0
	.word	1073887232
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.cfi_endproc
.LFE110:
	.size	print_RTC_stat, .-print_RTC_stat
	.section	.rodata
	.align	2
.LC20:
	.ascii	"before reset\015\012\000"
	.align	2
.LC21:
	.ascii	"reset\015\012\000"
	.align	2
.LC22:
	.ascii	"after reset\015\012\000"
	.align	2
.LC23:
	.ascii	"before loop 1\015\012\000"
	.align	2
.LC24:
	.ascii	"RCC->BCDR[LSEON] = %d\015\012\000"
	.align	2
.LC25:
	.ascii	"RCC->BDCR[LSERDY] = %d\015\012\000"
	.align	2
.LC26:
	.ascii	"after loop 1\015\012\000"
	.align	2
.LC27:
	.ascii	"after loop\015\012\000"
	.text
	.align	2
	.global	init_RTC_2
	.thumb
	.thumb_func
	.type	init_RTC_2, %function
init_RTC_2:
.LFB111:
	.loc 1 100 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 102 0
	ldr	r2, .L27
	ldr	r3, .L27
	ldr	r3, [r3, #64]
	orr	r3, r3, #268435456
	str	r3, [r2, #64]
	.loc 1 110 0
	ldr	r2, .L27+4
	ldr	r3, .L27+4
	ldr	r3, [r3]
	orr	r3, r3, #256
	str	r3, [r2]
	.loc 1 112 0
	ldr	r0, .L27+8
	bl	print_terminal
	.loc 1 113 0
	ldr	r2, .L27
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	orr	r3, r3, #65536
	str	r3, [r2, #112]
	.loc 1 114 0
	ldr	r0, .L27+12
	bl	print_terminal
	.loc 1 115 0
	ldr	r2, .L27
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	bic	r3, r3, #65536
	str	r3, [r2, #112]
	.loc 1 116 0
	ldr	r0, .L27+16
	bl	print_terminal
	.loc 1 121 0
	ldr	r2, .L27+4
	ldr	r3, .L27+4
	ldr	r3, [r3]
	orr	r3, r3, #256
	str	r3, [r2]
	.loc 1 122 0
	ldr	r2, .L27
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	orr	r3, r3, #1
	str	r3, [r2, #112]
	.loc 1 125 0
	ldr	r0, .L27+20
	bl	print_terminal
	.loc 1 127 0
	b	.L24
.L25:
	.loc 1 129 0
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	and	r3, r3, #1
	ldr	r0, .L27+24
	mov	r1, r3
	bl	print_terminal
	.loc 1 130 0
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	and	r3, r3, #2
	ldr	r0, .L27+28
	mov	r1, r3
	bl	print_terminal
.L24:
	.loc 1 127 0
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L25
	.loc 1 134 0
	ldr	r0, .L27+32
	bl	print_terminal
	.loc 1 136 0
	ldr	r2, .L27
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	orr	r3, r3, #256
	str	r3, [r2, #112]
	.loc 1 138 0
	ldr	r2, .L27
	ldr	r3, .L27
	ldr	r3, [r3, #112]
	orr	r3, r3, #32768
	str	r3, [r2, #112]
	.loc 1 144 0
	ldr	r3, .L27+36
	movs	r2, #202
	str	r2, [r3, #36]
	.loc 1 145 0
	ldr	r3, .L27+36
	movs	r2, #83
	str	r2, [r3, #36]
	.loc 1 159 0
	ldr	r2, .L27+36
	ldr	r3, .L27+36
	ldr	r3, [r3, #12]
	orr	r3, r3, #128
	str	r3, [r2, #12]
	.loc 1 160 0
	nop
.L26:
	.loc 1 160 0 is_stmt 0 discriminator 1
	ldr	r3, .L27+36
	ldr	r3, [r3, #12]
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L26
	.loc 1 162 0 is_stmt 1
	ldr	r0, .L27+40
	bl	print_terminal
	.loc 1 166 0
	ldr	r3, .L27+36
	movs	r2, #255
	str	r2, [r3, #16]
	.loc 1 167 0
	ldr	r2, .L27+36
	ldr	r3, .L27+36
	ldr	r3, [r3, #16]
	orr	r3, r3, #8323072
	str	r3, [r2, #16]
	.loc 1 172 0
	ldr	r2, .L27+36
	ldr	r3, .L27+36
	ldr	r3, [r3, #8]
	bic	r3, r3, #64
	str	r3, [r2, #8]
	.loc 1 182 0
	ldr	r3, .L27+36
	ldr	r2, .L27+44
	str	r2, [r3, #4]
	.loc 1 192 0
	ldr	r3, .L27+36
	ldr	r2, .L27+48
	str	r2, [r3]
	.loc 1 196 0
	ldr	r2, .L27+36
	ldr	r3, .L27+36
	ldr	r3, [r3, #12]
	bic	r3, r3, #128
	str	r3, [r2, #12]
	.loc 1 199 0
	ldr	r3, .L27+36
	movs	r2, #255
	str	r2, [r3, #36]
	.loc 1 200 0
	pop	{r7, pc}
.L28:
	.align	2
.L27:
	.word	1073887232
	.word	1073770496
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	1073752064
	.word	.LC27
	.word	1688355
	.word	1380352
	.cfi_endproc
.LFE111:
	.size	init_RTC_2, .-init_RTC_2
	.align	2
	.global	init_RTC
	.thumb
	.thumb_func
	.type	init_RTC, %function
init_RTC:
.LFB112:
	.loc 1 225 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 227 0
	ldr	r2, .L31
	ldr	r3, .L31
	ldr	r3, [r3, #64]
	orr	r3, r3, #268435456
	str	r3, [r2, #64]
	.loc 1 235 0
	ldr	r2, .L31+4
	ldr	r3, .L31+4
	ldr	r3, [r3]
	orr	r3, r3, #256
	str	r3, [r2]
	.loc 1 239 0
	ldr	r2, .L31
	ldr	r3, .L31
	ldr	r3, [r3, #8]
	orr	r3, r3, #524288
	str	r3, [r2, #8]
	.loc 1 245 0
	ldr	r2, .L31
	ldr	r3, .L31
	ldr	r3, [r3, #112]
	orr	r3, r3, #768
	str	r3, [r2, #112]
	.loc 1 247 0
	ldr	r2, .L31
	ldr	r3, .L31
	ldr	r3, [r3, #112]
	orr	r3, r3, #32768
	str	r3, [r2, #112]
	.loc 1 253 0
	ldr	r3, .L31+8
	movs	r2, #202
	str	r2, [r3, #36]
	.loc 1 254 0
	ldr	r3, .L31+8
	movs	r2, #83
	str	r2, [r3, #36]
	.loc 1 268 0
	ldr	r2, .L31+8
	ldr	r3, .L31+8
	ldr	r3, [r3, #12]
	orr	r3, r3, #128
	str	r3, [r2, #12]
	.loc 1 269 0
	nop
.L30:
	.loc 1 269 0 is_stmt 0 discriminator 1
	ldr	r3, .L31+8
	ldr	r3, [r3, #12]
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L30
	.loc 1 271 0 is_stmt 1
	ldr	r0, .L31+12
	bl	print_terminal
	.loc 1 275 0
	ldr	r3, .L31+8
	movw	r2, #15624
	str	r2, [r3, #16]
	.loc 1 276 0
	ldr	r2, .L31+8
	ldr	r3, .L31+8
	ldr	r3, [r3, #16]
	orr	r3, r3, #4128768
	str	r3, [r2, #16]
	.loc 1 281 0
	ldr	r2, .L31+8
	ldr	r3, .L31+8
	ldr	r3, [r3, #8]
	bic	r3, r3, #64
	str	r3, [r2, #8]
	.loc 1 291 0
	ldr	r3, .L31+8
	ldr	r2, .L31+16
	str	r2, [r3, #4]
	.loc 1 301 0
	ldr	r3, .L31+8
	ldr	r2, .L31+20
	str	r2, [r3]
	.loc 1 305 0
	ldr	r2, .L31+8
	ldr	r3, .L31+8
	ldr	r3, [r3, #12]
	bic	r3, r3, #128
	str	r3, [r2, #12]
	.loc 1 308 0
	ldr	r3, .L31+8
	movs	r2, #255
	str	r2, [r3, #36]
	.loc 1 309 0
	pop	{r7, pc}
.L32:
	.align	2
.L31:
	.word	1073887232
	.word	1073770496
	.word	1073752064
	.word	.LC27
	.word	1688355
	.word	1642496
	.cfi_endproc
.LFE112:
	.size	init_RTC, .-init_RTC
	.section	.rodata
	.align	2
.LC28:
	.ascii	"Day: %d \000"
	.align	2
.LC29:
	.ascii	"Month: %d \000"
	.align	2
.LC30:
	.ascii	"Year: %d\015\012\000"
	.align	2
.LC31:
	.ascii	"Second: %d \000"
	.align	2
.LC32:
	.ascii	"Minute: %d \000"
	.align	2
.LC33:
	.ascii	"Hour: %d\015\012\000"
	.text
	.align	2
	.global	print_RTC
	.thumb
	.thumb_func
	.type	print_RTC, %function
print_RTC:
.LFB113:
	.loc 1 338 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 342 0
	ldr	r3, .L34
	ldr	r3, [r3]
	str	r3, [r7, #4]
	.loc 1 343 0
	ldr	r3, .L34
	ldr	r3, [r3, #4]
	str	r3, [r7]
	.loc 1 346 0
	ldr	r3, [r7]
	lsrs	r3, r3, #4
	and	r2, r3, #3
	mov	r3, r2
	lsls	r3, r3, #2
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7]
	and	r3, r3, #15
	add	r3, r3, r2
	ldr	r0, .L34+4
	mov	r1, r3
	bl	print_terminal
	.loc 1 347 0
	ldr	r3, [r7]
	lsrs	r3, r3, #12
	and	r2, r3, #1
	mov	r3, r2
	lsls	r3, r3, #2
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7]
	lsrs	r3, r3, #8
	and	r3, r3, #15
	add	r3, r3, r2
	ldr	r0, .L34+8
	mov	r1, r3
	bl	print_terminal
	.loc 1 348 0
	ldr	r3, [r7]
	lsrs	r3, r3, #20
	and	r2, r3, #15
	mov	r3, r2
	lsls	r3, r3, #2
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7]
	lsrs	r3, r3, #16
	and	r3, r3, #15
	add	r3, r3, r2
	ldr	r0, .L34+12
	mov	r1, r3
	bl	print_terminal
	.loc 1 350 0
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #4
	and	r2, r3, #7
	mov	r3, r2
	lsls	r3, r3, #2
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7, #4]
	and	r3, r3, #15
	add	r3, r3, r2
	ldr	r0, .L34+16
	mov	r1, r3
	bl	print_terminal
	.loc 1 351 0
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #12
	and	r2, r3, #7
	mov	r3, r2
	lsls	r3, r3, #2
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #8
	and	r3, r3, #15
	add	r3, r3, r2
	ldr	r0, .L34+20
	mov	r1, r3
	bl	print_terminal
	.loc 1 352 0
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #20
	and	r2, r3, #3
	mov	r3, r2
	lsls	r3, r3, #2
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #16
	and	r3, r3, #15
	add	r3, r3, r2
	ldr	r0, .L34+24
	mov	r1, r3
	bl	print_terminal
	.loc 1 354 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L35:
	.align	2
.L34:
	.word	1073752064
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.cfi_endproc
.LFE113:
	.size	print_RTC, .-print_RTC
	.align	2
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
.LFB114:
	.loc 1 357 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 358 0
	bl	init_UART4
	.loc 1 359 0
	bl	init_RTC
	.loc 1 361 0
	bl	print_RTC_stat
.L39:
.LBB2:
	.loc 1 367 0
	bl	print_RTC
	.loc 1 368 0
	movs	r3, #0
	str	r3, [r7, #4]
	b	.L37
.L38:
	.loc 1 368 0 is_stmt 0 discriminator 3
	ldr	r3, [r7, #4]
	adds	r3, r3, #1
	str	r3, [r7, #4]
.L37:
	.loc 1 368 0 discriminator 1
	ldr	r3, [r7, #4]
	ldr	r2, .L40
	cmp	r3, r2
	ble	.L38
.LBE2:
	.loc 1 369 0 is_stmt 1
	b	.L39
.L41:
	.align	2
.L40:
	.word	999999
	.cfi_endproc
.LFE114:
	.size	main, .-main
.Letext0:
	.file 2 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "../../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x56e
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF87
	.byte	0x1
	.4byte	.LASF88
	.4byte	.LASF89
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x2b
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3f
	.4byte	0x57
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x41
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x3
	.byte	0x21
	.4byte	0x3a
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x2c
	.4byte	0x4c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x2d
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF15
	.uleb128 0x5
	.4byte	0xa2
	.uleb128 0x6
	.4byte	0xa2
	.4byte	0xc9
	.uleb128 0x7
	.4byte	0xad
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x97
	.uleb128 0x8
	.byte	0x8
	.byte	0x4
	.2byte	0x2d3
	.4byte	0xf1
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2d5
	.4byte	0xb4
	.byte	0
	.uleb128 0x9
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2d6
	.4byte	0xb4
	.byte	0x4
	.byte	0
	.uleb128 0xa
	.4byte	.LASF16
	.byte	0x4
	.2byte	0x2d7
	.4byte	0xce
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x28c
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xb4
	.byte	0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xb4
	.byte	0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xb4
	.byte	0x8
	.uleb128 0x9
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xb4
	.byte	0xc
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xb4
	.byte	0x10
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xb4
	.byte	0x14
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xb4
	.byte	0x18
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xa2
	.byte	0x1c
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xb4
	.byte	0x20
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xb4
	.byte	0x24
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xb9
	.byte	0x28
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xb4
	.byte	0x30
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xb4
	.byte	0x34
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xb4
	.byte	0x38
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xa2
	.byte	0x3c
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xb4
	.byte	0x40
	.uleb128 0xb
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xb4
	.byte	0x44
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xb9
	.byte	0x48
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xb4
	.byte	0x50
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xb4
	.byte	0x54
	.uleb128 0xb
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xb4
	.byte	0x58
	.uleb128 0xb
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xa2
	.byte	0x5c
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xb4
	.byte	0x60
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xb4
	.byte	0x64
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xb9
	.byte	0x68
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xb4
	.byte	0x70
	.uleb128 0x9
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xb4
	.byte	0x74
	.uleb128 0xb
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xb9
	.byte	0x78
	.uleb128 0xb
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xb4
	.byte	0x80
	.uleb128 0xb
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xb4
	.byte	0x84
	.byte	0
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2fd
	.4byte	0xfd
	.uleb128 0x8
	.byte	0xa0
	.byte	0x4
	.2byte	0x303
	.4byte	0x4a7
	.uleb128 0x9
	.ascii	"TR\000"
	.byte	0x4
	.2byte	0x305
	.4byte	0xb4
	.byte	0
	.uleb128 0x9
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x306
	.4byte	0xb4
	.byte	0x4
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x307
	.4byte	0xb4
	.byte	0x8
	.uleb128 0x9
	.ascii	"ISR\000"
	.byte	0x4
	.2byte	0x308
	.4byte	0xb4
	.byte	0xc
	.uleb128 0xb
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x309
	.4byte	0xb4
	.byte	0x10
	.uleb128 0xb
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x30a
	.4byte	0xb4
	.byte	0x14
	.uleb128 0xb
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x30b
	.4byte	0xb4
	.byte	0x18
	.uleb128 0xb
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x30c
	.4byte	0xb4
	.byte	0x1c
	.uleb128 0xb
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x30d
	.4byte	0xb4
	.byte	0x20
	.uleb128 0x9
	.ascii	"WPR\000"
	.byte	0x4
	.2byte	0x30e
	.4byte	0xb4
	.byte	0x24
	.uleb128 0x9
	.ascii	"SSR\000"
	.byte	0x4
	.2byte	0x30f
	.4byte	0xb4
	.byte	0x28
	.uleb128 0xb
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x310
	.4byte	0xb4
	.byte	0x2c
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x311
	.4byte	0xb4
	.byte	0x30
	.uleb128 0xb
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x312
	.4byte	0xb4
	.byte	0x34
	.uleb128 0xb
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x313
	.4byte	0xb4
	.byte	0x38
	.uleb128 0xb
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x314
	.4byte	0xb4
	.byte	0x3c
	.uleb128 0xb
	.4byte	.LASF55
	.byte	0x4
	.2byte	0x315
	.4byte	0xb4
	.byte	0x40
	.uleb128 0xb
	.4byte	.LASF56
	.byte	0x4
	.2byte	0x316
	.4byte	0xb4
	.byte	0x44
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x317
	.4byte	0xb4
	.byte	0x48
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x4
	.2byte	0x318
	.4byte	0xa2
	.byte	0x4c
	.uleb128 0xb
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x319
	.4byte	0xb4
	.byte	0x50
	.uleb128 0xb
	.4byte	.LASF60
	.byte	0x4
	.2byte	0x31a
	.4byte	0xb4
	.byte	0x54
	.uleb128 0xb
	.4byte	.LASF61
	.byte	0x4
	.2byte	0x31b
	.4byte	0xb4
	.byte	0x58
	.uleb128 0xb
	.4byte	.LASF62
	.byte	0x4
	.2byte	0x31c
	.4byte	0xb4
	.byte	0x5c
	.uleb128 0xb
	.4byte	.LASF63
	.byte	0x4
	.2byte	0x31d
	.4byte	0xb4
	.byte	0x60
	.uleb128 0xb
	.4byte	.LASF64
	.byte	0x4
	.2byte	0x31e
	.4byte	0xb4
	.byte	0x64
	.uleb128 0xb
	.4byte	.LASF65
	.byte	0x4
	.2byte	0x31f
	.4byte	0xb4
	.byte	0x68
	.uleb128 0xb
	.4byte	.LASF66
	.byte	0x4
	.2byte	0x320
	.4byte	0xb4
	.byte	0x6c
	.uleb128 0xb
	.4byte	.LASF67
	.byte	0x4
	.2byte	0x321
	.4byte	0xb4
	.byte	0x70
	.uleb128 0xb
	.4byte	.LASF68
	.byte	0x4
	.2byte	0x322
	.4byte	0xb4
	.byte	0x74
	.uleb128 0xb
	.4byte	.LASF69
	.byte	0x4
	.2byte	0x323
	.4byte	0xb4
	.byte	0x78
	.uleb128 0xb
	.4byte	.LASF70
	.byte	0x4
	.2byte	0x324
	.4byte	0xb4
	.byte	0x7c
	.uleb128 0xb
	.4byte	.LASF71
	.byte	0x4
	.2byte	0x325
	.4byte	0xb4
	.byte	0x80
	.uleb128 0xb
	.4byte	.LASF72
	.byte	0x4
	.2byte	0x326
	.4byte	0xb4
	.byte	0x84
	.uleb128 0xb
	.4byte	.LASF73
	.byte	0x4
	.2byte	0x327
	.4byte	0xb4
	.byte	0x88
	.uleb128 0xb
	.4byte	.LASF74
	.byte	0x4
	.2byte	0x328
	.4byte	0xb4
	.byte	0x8c
	.uleb128 0xb
	.4byte	.LASF75
	.byte	0x4
	.2byte	0x329
	.4byte	0xb4
	.byte	0x90
	.uleb128 0xb
	.4byte	.LASF76
	.byte	0x4
	.2byte	0x32a
	.4byte	0xb4
	.byte	0x94
	.uleb128 0xb
	.4byte	.LASF77
	.byte	0x4
	.2byte	0x32b
	.4byte	0xb4
	.byte	0x98
	.uleb128 0xb
	.4byte	.LASF78
	.byte	0x4
	.2byte	0x32c
	.4byte	0xb4
	.byte	0x9c
	.byte	0
	.uleb128 0xa
	.4byte	.LASF79
	.byte	0x4
	.2byte	0x32d
	.4byte	0x298
	.uleb128 0xc
	.4byte	.LASF82
	.byte	0x1
	.byte	0x7
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x4d7
	.uleb128 0xd
	.4byte	.LASF84
	.byte	0x1
	.byte	0xf
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	.LASF80
	.byte	0x1
	.byte	0x63
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xe
	.4byte	.LASF81
	.byte	0x1
	.byte	0xe0
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xf
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x151
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x52e
	.uleb128 0x10
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x153
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x154
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x164
	.4byte	0x7e
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x560
	.uleb128 0x12
	.4byte	.LBB2
	.4byte	.LBE2-.LBB2
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x16d
	.4byte	0x560
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.byte	0
	.uleb128 0x5
	.4byte	0x7e
	.uleb128 0x14
	.4byte	.LASF91
	.byte	0x5
	.2byte	0x51b
	.4byte	0xc9
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF33:
	.ascii	"AHB1LPENR\000"
.LASF84:
	.ascii	"RCC_BDCR\000"
.LASF61:
	.ascii	"BKP2R\000"
.LASF23:
	.ascii	"APB1RSTR\000"
.LASF27:
	.ascii	"AHB2ENR\000"
.LASF2:
	.ascii	"short int\000"
.LASF65:
	.ascii	"BKP6R\000"
.LASF40:
	.ascii	"BDCR\000"
.LASF54:
	.ascii	"CALR\000"
.LASF46:
	.ascii	"WUTR\000"
.LASF43:
	.ascii	"PLLI2SCFGR\000"
.LASF15:
	.ascii	"sizetype\000"
.LASF16:
	.ascii	"PWR_TypeDef\000"
.LASF90:
	.ascii	"main\000"
.LASF7:
	.ascii	"__uint32_t\000"
.LASF72:
	.ascii	"BKP13R\000"
.LASF4:
	.ascii	"__uint16_t\000"
.LASF42:
	.ascii	"SSCGR\000"
.LASF35:
	.ascii	"AHB3LPENR\000"
.LASF18:
	.ascii	"CFGR\000"
.LASF36:
	.ascii	"RESERVED4\000"
.LASF76:
	.ascii	"BKP17R\000"
.LASF30:
	.ascii	"APB1ENR\000"
.LASF52:
	.ascii	"TSDR\000"
.LASF55:
	.ascii	"TAFCR\000"
.LASF31:
	.ascii	"APB2ENR\000"
.LASF45:
	.ascii	"PRER\000"
.LASF62:
	.ascii	"BKP3R\000"
.LASF28:
	.ascii	"AHB3ENR\000"
.LASF9:
	.ascii	"long long int\000"
.LASF47:
	.ascii	"CALIBR\000"
.LASF80:
	.ascii	"init_RTC_2\000"
.LASF6:
	.ascii	"long int\000"
.LASF69:
	.ascii	"BKP10R\000"
.LASF66:
	.ascii	"BKP7R\000"
.LASF44:
	.ascii	"RCC_TypeDef\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF24:
	.ascii	"APB2RSTR\000"
.LASF56:
	.ascii	"ALRMASSR\000"
.LASF73:
	.ascii	"BKP14R\000"
.LASF79:
	.ascii	"RTC_TypeDef\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF82:
	.ascii	"print_RTC_stat\000"
.LASF59:
	.ascii	"BKP0R\000"
.LASF0:
	.ascii	"signed char\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF85:
	.ascii	"TR_value\000"
.LASF14:
	.ascii	"uint32_t\000"
.LASF48:
	.ascii	"ALRMAR\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF20:
	.ascii	"AHB2RSTR\000"
.LASF12:
	.ascii	"uint16_t\000"
.LASF50:
	.ascii	"SHIFTR\000"
.LASF19:
	.ascii	"AHB1RSTR\000"
.LASF63:
	.ascii	"BKP4R\000"
.LASF51:
	.ascii	"TSTR\000"
.LASF17:
	.ascii	"PLLCFGR\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF37:
	.ascii	"APB1LPENR\000"
.LASF77:
	.ascii	"BKP18R\000"
.LASF70:
	.ascii	"BKP11R\000"
.LASF67:
	.ascii	"BKP8R\000"
.LASF89:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/chapter_2/exercise_03\000"
.LASF13:
	.ascii	"int32_t\000"
.LASF57:
	.ascii	"ALRMBSSR\000"
.LASF87:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF74:
	.ascii	"BKP15R\000"
.LASF34:
	.ascii	"AHB2LPENR\000"
.LASF81:
	.ascii	"init_RTC\000"
.LASF86:
	.ascii	"DR_value\000"
.LASF22:
	.ascii	"RESERVED0\000"
.LASF25:
	.ascii	"RESERVED1\000"
.LASF29:
	.ascii	"RESERVED2\000"
.LASF32:
	.ascii	"RESERVED3\000"
.LASF88:
	.ascii	"main.c\000"
.LASF39:
	.ascii	"RESERVED5\000"
.LASF41:
	.ascii	"RESERVED6\000"
.LASF58:
	.ascii	"RESERVED7\000"
.LASF78:
	.ascii	"BKP19R\000"
.LASF60:
	.ascii	"BKP1R\000"
.LASF26:
	.ascii	"AHB1ENR\000"
.LASF49:
	.ascii	"ALRMBR\000"
.LASF83:
	.ascii	"print_RTC\000"
.LASF5:
	.ascii	"__int32_t\000"
.LASF64:
	.ascii	"BKP5R\000"
.LASF53:
	.ascii	"TSSSR\000"
.LASF71:
	.ascii	"BKP12R\000"
.LASF68:
	.ascii	"BKP9R\000"
.LASF91:
	.ascii	"ITM_RxBuffer\000"
.LASF38:
	.ascii	"APB2LPENR\000"
.LASF75:
	.ascii	"BKP16R\000"
.LASF21:
	.ascii	"AHB3RSTR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
