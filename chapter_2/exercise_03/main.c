#include "stm32f4xx.h"
#include "print.h"

// (reference manual pg.155)
// RTC intro

void print_RTC_stat()
{
	
	// (reference manual pg.196 and pg.197)
	// RCC backup domain control register(BDCR)

	print_terminal("RCC->BDCR stat\r\n----------------------------------------\r\n");

	uint32_t RCC_BDCR = RCC->BDCR;

	if(RCC_BDCR & 0x1) // Bit[0] LSEON
		print_terminal("LSE clock ON\r\n");
	else 
		print_terminal("LSE clock OFF\r\n");


	if(RCC_BDCR & 0x2) // Bit[1] LSERDY
		print_terminal("LSE clock ready\r\n");
	else
		print_terminal("LSE clock not ready\r\n");


	if(RCC_BDCR & 0x4) // Bit[2] LSEBYP
		print_terminal("LSE oscillator bypassed\r\n");
	else
		print_terminal("LSE oscillator not bypassed\r\n");


	print_terminal("RTC clock source selection: ");
	switch((RCC_BDCR >> 8) & 0x3) // Bit[9:8] RTCSEL
	{
		case 0:
			print_terminal("no clock\r\n");
			break;

		case 1:
			print_terminal("LSE oscillator\r\n");
			break;

		case 2:
			print_terminal("LSI oscillator\r\n");
			break;

		case 3:
			print_terminal("HSE oscillator\r\n");
			break;
	}

	if(RCC_BDCR & 0x8000) // Bit[15] RTCEN
		print_terminal("RTC clock enabled\r\n");
	else
		print_terminal("RTC clock disabled\r\n");


	if(RCC_BDCR & 0x10000) // Bit[16] BDRST
		print_terminal("Reset the entire Backup domain\r\n");
	else
		print_terminal("Reset not activated\r\n");


	print_terminal("-------------------------------------------------------\r\n");


	// (reference manual pg.163 and pg.164)
	// RCC clock configuration register(CFGR)

	print_terminal("RCC->CFGR stat\r\n----------------------------------------\r\n");

	print_terminal("HSE division factor for RTC clock(need to be set on such value that clock supplied to RTC would be 1MHz):");
	switch((RCC->CFGR >> 16) & 0x1F)
	{
		case 0:
		case 1:
			print_terminal("no clock\r\n");
			break;

		default:
			print_terminal("%d\r\n", (RCC->CFGR >> 16) & 0x1F);
	}

}


// (reference manual pg.780)
// RTC description

// (reference manual pg.782)
// RTC block diagram

// (reference manual pg.785)
// RTC initialization and configuration

void init_RTC_2()
{

	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	// (reference manual pg.785)
	// (reference manual pg.138)
	// In reset state, RCC->BDCR register and RTC registes, are
	// protected against parasitic write access. So, in order to
	// get the access to this registers, DBP bit in PWR->CR register is
	// need to be set.
	PWR->CR |= PWR_CR_DBP; // disable backup domain write protection

	print_terminal("before reset\r\n");
	RCC->BDCR |=  RCC_BDCR_BDRST;	
	print_terminal("reset\r\n"); 	
	RCC->BDCR &=  ~RCC_BDCR_BDRST;	
	print_terminal("after reset\r\n"); 
	// (reference manual pg.196 and pg.197)
	// also see function 'print_RTC_stat()' 


	PWR->CR |= PWR_CR_DBP; // disable backup domain write protection
	RCC->BDCR |= RCC_BDCR_LSEON; // enable LSE(external low-speed oscillator)


	print_terminal("before loop 1\r\n");

	while((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY)
	{
		print_terminal("RCC->BCDR[LSEON] = %d\r\n", RCC->BDCR & RCC_BDCR_LSEON);
		print_terminal("RCC->BDCR[LSERDY] = %d\r\n", RCC->BDCR & RCC_BDCR_LSERDY);
	}// wait for LSE to be ready


	print_terminal("after loop 1\r\n");

	RCC->BDCR |= RCC_BDCR_RTCSEL_0; // select LSE as RTC source clock

	RCC->BDCR |= RCC_BDCR_RTCEN; // RTC clock enable

	// (reference manual pg.785)
	// (reference manual pg.807 and pg.808)
	// Unlock write protection on the all RTC registers.
	// Writing a wrong key reactivates the write protection.
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;


	// (reference manual pg.785)
	// (reference manual pg.801, pg.802 and pg.803)
	// INIT[7]
	// Initialization mode used to:
	//  -> program time and date register (RTC->TR and RTC-DR)
	//  -> prescaler register (RTC->PRER)
	// Counters are stopped and start counting from the new
	// value when INIT is reset.
	// INITF[6]
	// When this bit is set to 1, the RTC is in initilaization state, and the time, date
	// and prescaler registers can be updated.
	RTC->ISR |= RTC_ISR_INIT; // Bit[7] - INIT
	while((RTC->ISR & RTC_ISR_INITF) != RTC_ISR_INITF); // Bit[6] - INITF
	
	print_terminal("after loop\r\n");

	// (reference manual pg.803 pg.804)
	// RTC prescaler register
	RTC->PRER = 0xFF; // synchronous prescaler 255 + 1
	RTC->PRER |= 0x7F0000; // asynchronous prescaler 127 + 1

	// (reference manual pg.799 and pg.800)
	// FMT[6] -> value '0', 24 hour/day format
	// 				-> value '1', AM/PM hour format
	RTC->CR &= ~RTC_CR_FMT; // reset value of FMT to '0'

	// (reference manual pg.798)
	// DU[3:0] - date units in BCD format
	// DT[5:4] - date tens in BCD format
	// MU[11:8] - month units in BCD format
	// MT[12] - month tens in BCD format
	// WDU[15:13] - week day units ('000'b forbidden)
	// YU[19:16] - year units in BCD format
	// YT[23:20] - year tens in BCD format
	RTC->DR = 0x0019C323;

	// (reference manual pg.797)
	// SU[3:0] - second units in BCD format
	// ST[6:4] - second tens in BCD format
	// MNU[11:8] - minute units in BCD format
	// MNT[14:12] - minute tens in BCD format
	// HU[19:16] - hour units in BCD format
	// HU[21:20] - hour units in BCD format
	// PM[22] - AM/PM notation ('0' -> AM or 24-hour format, '1' -> PM)
	RTC->TR = 0x00151000;

	// (reference manual pg.785)	
	// step 5. in calendar initialization and configuration
	RTC->ISR &= ~RTC_ISR_INIT; 

	// enable write protecion for RTC
	RTC->WPR = 0xFF;
}














// (reference manual pg.780)
// RTC description

// (reference manual pg.782)
// RTC block diagram

// (reference manual pg.785)
// RTC initialization and configuration

void init_RTC()
{

	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	// (reference manual pg.785)
	// (reference manual pg.138)
	// In reset state, RCC->BDCR register and RTC registes, are
	// protected against parasitic write access. So, in order to
	// get the access to this registers, DBP bit in PWR->CR register is
	// need to be set.
	PWR->CR |= PWR_CR_DBP; // disable backup domain write protection


	// (reference manual pg.163 and pg.164)
	RCC->CFGR |= 0x00080000; // divide HSE clock to generate 1MHz clock at RTC


	// (reference manual pg.196 and pg.197)
	// also see function 'print_RTC_stat()' 

	RCC->BDCR |= RCC_BDCR_RTCSEL; // select HSE as RTC source clock

	RCC->BDCR |= RCC_BDCR_RTCEN; // RTC clock enable

	// (reference manual pg.785)
	// (reference manual pg.807 and pg.808)
	// Unlock write protection on the all RTC registers.
	// Writing a wrong key reactivates the write protection.
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;


	// (reference manual pg.785)
	// (reference manual pg.801, pg.802 and pg.803)
	// INIT[7]
	// Initialization mode used to:
	//  -> program time and date register (RTC->TR and RTC-DR)
	//  -> prescaler register (RTC->PRER)
	// Counters are stopped and start counting from the new
	// value when INIT is reset.
	// INITF[6]
	// When this bit is set to 1, the RTC is in initilaization state, and the time, date
	// and prescaler registers can be updated.
	RTC->ISR |= RTC_ISR_INIT; // Bit[7] - INIT
	while((RTC->ISR & RTC_ISR_INITF) != RTC_ISR_INITF); // Bit[6] - INITF
	
	print_terminal("after loop\r\n");

	// (reference manual pg.803 pg.804)
	// RTC prescaler register
	RTC->PRER = 15625 - 1; // synchronous prescaler 15624 + 1
	RTC->PRER |= (64 - 1) << 16; // asynchronous prescaler 63 + 1

	// (reference manual pg.799 and pg.800)
	// FMT[6] -> value '0', 24 hour/day format
	// 				-> value '1', AM/PM hour format
	RTC->CR &= ~RTC_CR_FMT; // reset value of FMT to '0'

	// (reference manual pg.798)
	// DU[3:0] - date units in BCD format
	// DT[5:4] - date tens in BCD format
	// MU[11:8] - month units in BCD format
	// MT[12] - month tens in BCD format
	// WDU[15:13] - week day units ('000'b forbidden)
	// YU[19:16] - year units in BCD format
	// YT[23:20] - year tens in BCD format
	RTC->DR = 0x0019C323;

	// (reference manual pg.797)
	// SU[3:0] - second units in BCD format
	// ST[6:4] - second tens in BCD format
	// MNU[11:8] - minute units in BCD format
	// MNT[14:12] - minute tens in BCD format
	// HU[19:16] - hour units in BCD format
	// HU[21:20] - hour units in BCD format
	// PM[22] - AM/PM notation ('0' -> AM or 24-hour format, '1' -> PM)
	RTC->TR = 0x00191000;

	// (reference manual pg.785)	
	// step 5. in calendar initialization and configuration
	RTC->ISR &= ~RTC_ISR_INIT; 

	// enable write protecion for RTC
	RTC->WPR = 0xFF;
}



























void print_RTC()
{
	uint32_t TR_value;
	uint32_t DR_value;

	TR_value = RTC->TR;
	DR_value = RTC->DR;
	

	print_terminal("Day: %d ", ((DR_value >> 4) & 0x3) * 10 + (DR_value & 0xF));
	print_terminal("Month: %d ", ((DR_value >> 12) & 0x1) * 10 + ((DR_value >> 8) & 0xF));
	print_terminal("Year: %d\r\n", ((DR_value >> 20) & 0xF) * 10 + ((DR_value >> 16) & 0xF));

	print_terminal("Second: %d ", ((TR_value >> 4) & 0x7) * 10 + (TR_value & 0xF));
	print_terminal("Minute: %d ", ((TR_value >> 12) & 0x7) * 10 + ((TR_value >> 8) & 0xF));
	print_terminal("Hour: %d\r\n", ((TR_value >> 20) & 0x3) * 10 + ((TR_value >> 16) & 0xF));

}

int main(void)
{
	init_UART4();
	init_RTC();
	
	print_RTC_stat();

	while(1)
	{
		volatile int i;

		print_RTC();	
		for(i = 0; i < 1000000; ++i);
	}
	return 0;
}
