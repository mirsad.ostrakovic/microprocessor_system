#include "stm32f4xx.h"
#include "print.h"

// (technical paper pg.56)
// PB8 -> TIM10_CH1 on AF3

// (reference manual pg.638)
// General-purpose timer block diagram for TIM10

void init_PWM_TIM10_CH1()
{
	//(technical paper pg.17 - STM32F40x block diagram)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN; // enable GPIOB peripheral
	RCC->APB2ENR |= RCC_APB2ENR_TIM10EN; // enable TIM10 peripheral

	// set PB8 pin in the alternate function mode
	GPIOB->MODER &= 0xFFFCFFFF; 
	GPIOB->MODER |= 0x00020000;

	// reset PB8 to push-pull configuration
	GPIOB->OTYPER &= 0xFEFF;

	// set PB8 to high speed
	GPIOB->OSPEEDR |= 0x00030000;

	// (reference manual pg.283)
	// set PB8 alternate function to AF3
	GPIOB->AFR[1] &= 0xFFFFFFF0;
	GPIOB->AFR[1] |= 0x00000003; // same will be if we write 0x3

	
	// (reference manual pf.650)
	// PWM mode description

	// (reference manual pf.670)
	TIM10->CR1 |= 0x84;

	// (reference manual pg.676)
	TIM10->PSC = 168 - 1; // APB2 timer clock frequency is 168MhZ
												// for details see 'chapter1/exercise_05'
												// prescaled frequency is 1MhZ	

	// set output signal frequency of PWM
	TIM10->ARR = 1000; // output signal frequency is 1kHz

	// (reference manual pg.677)
	TIM10->CCR1 = 500; // duty cycle (1 - TIM10->CCR1 / TIM10->ARR) * 100% = 5%

	// (refence manual pg.673)
	// [1:0] CC1S
	// [2] OC1FE
	// [3] OC1PE
	// [6:4] OC1M
	
	// reset all bits to '0' value
	// set bits OC1PE to '1' and OC1M to '111'
	// OC1PE(value '1') - output compare 1 preload enable (the preloaded value loaded in the active register at each UE)
	// OC1M(value '110') - PWM mode 1 - Channel 1 is active as long as TIM10->CNT < TIM10->CCR1 else inactive	
	// OC1M(value '111') - PWM mode 2 - Channel 1 is inactive as long as TIM10->CNT < TIM10->CCR1 else active
	// CC1S value to '00' - channel is configured as output
	TIM10->CCMR1 &= 0x00;
	TIM10->CCMR1 |= 0x68;

	// (reference manual pg.675)
	// [0]CC1E
	// [1]CC1P
	// [2]CC1P/reserved and must be '0
	// [3]CC1NP
	// If channel is configured as output CC1NP must be kept cleared.
	// CC1P for channel configured as output is:
	// 	-> for value '0' - OC1 active high
	// 	-> for value '1' - OC1 active low
	// CC1E in output configuration mode:
	//  -> '0' -> OC1 not active
	//  -> '1' -> OC1 signal is output to the corresponding output pin
	
	TIM10->CCER &= 0x0;
	TIM10->CCER |= 0x1;

	// (reference manual pg.671 and pg.672)
	TIM10->EGR |= 0x1; // generates an update of the registers


	// In file '../../../Libraries/CMSIS/Include/core_cm4.h'
  // are defined functions for setup NVIC
  // TIM1_UP_TIM10_IRQn is defined in 'stmf4xx.h' - TIM1 Update Interrupt and TIM10 global interrupt
	NVIC_SetPriority(0, TIM1_UP_TIM10_IRQn); // setup TIM10 interrupt priority to 0 - max
  NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn); // enable TIM10 interrupts

  TIM10->DIER |= TIM_DIER_UIE; // UIE - update interrupt enable
	
	TIM10->CR1 |= 0x1; // enable counter
	
}


//defined as 'WEEK' in 'startup_stm32f4xx.s'
void TIM1_UP_TIM10_IRQHandler()
{	
	static volatile uint16_t count = 0u;
	static uint8_t flag = 0x0;

//	print_terminal("i\r\n");

	if(TIM10->SR & TIM_SR_UIF) 
	{
		if(++count > 50u)
		{	
			TIM10->CCR1 = flag == 0x0 ? TIM10->CCR1 * 1.5 : TIM10->CCR1 / 1.5;	

			if(TIM10->CCR1 > 1000u)
			{
				flag = 0x1;
				TIM10->CCR1 = 1024u;
			}
			else if(TIM10->CCR1 == 0u)
			{
				flag = 0x0;
				TIM10->CCR1 = 2u;
			}	

			count = 0u;
		}
		TIM10->SR &= ~TIM_SR_UIF;
	}	
	
}


int main(void)
{
	volatile int i = 0u;

	init_UART4();
	init_PWM_TIM10_CH1();
	
	print_terminal("system initialized\r\n");
	
	while(1)
	{
		++i;
		print_terminal("value %d\r\n", i);
	}
	return 0;
}

