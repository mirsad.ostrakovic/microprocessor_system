typeof Aura === "undefined" && (Aura = {});
Aura.StaticResourceMap = {"oct":{"astrea_oct":1551807981000},"pdfDocIcon":{"astrea_oct":1551807981000},"SurveyForce_SLDS":{"":1522073849000},"favicon":{"":1532418481000},"SurveyForce":{"":1522073848000},"NMTjs":{"LEXMAGICMOVER":1527249616000},"FSECrossRgnDashboard":{"":1491384161000},"NMTicon":{"LEXMAGICMOVER":1527249616000},"JSON2":{"":1470063866000},"SLDS100":{"QR":1539820201000},"SurveyForce_UserGuide":{"":1522073849000},"SLDS104":{"":1467993119000},"dbp_jqry":{"Dashboard_Pal":1542733524000},"SurveyForce_jquery":{"":1522073849000},"SurveyForce_jquery_ui":{"":1522073849000},"SiteSamples":{"":1511539025000},"FSEDashboard":{"":1491384161000},"word":{"astrea_oct":1551807981000},"EXTJS4_0":{"":1432740802000},"myTestData":{"":1478789519000},"SurveyForce_svg4everybody":{"":1522073849000}};

(function() { 
	function initResourceGVP() {
		 if (!$A.getContext() || !$A.get('$Resource')) { 
			 $A.addValueProvider('$Resource', 
			 { 
				 merge : function() {}, 
				 isStorable : function() { return false; }, 
				 get : function(resource) { 
					 var modStamp, rel, abs, name, ns;
					 var nsDelim = resource.indexOf('__');
					 if (nsDelim >= 0) { ns = resource.substring(0, nsDelim); name = resource.substring(nsDelim + 2); } else { name = resource; }
					 var srMap = Aura.StaticResourceMap[name];
					 modStamp = srMap && srMap[ns = ns || Object.keys(srMap)[0]];
					 if (!modStamp) { return; }
					 rel = $A.get('$SfdcSite.pathPrefix');
					 abs = $A.get('$Absolute.url');
					 return [abs || rel || '', '/resource/', modStamp, '/', ns === '' ? name : ns + '__' + name].join('');
				 } 
			 }); 
		 } 
	 }
if(Aura.frameworkJsReady)initResourceGVP();else{Aura.beforeFrameworkInit=Aura.beforeFrameworkInit||[],Aura.beforeFrameworkInit.push(initResourceGVP)}
})();