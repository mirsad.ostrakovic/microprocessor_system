/* In case if consent changes and we have changes in the groups, we need to reload the page */
function ReloadPageOnConsentChange() {
    /* store groups to cookie */
    function storeGroups(groups) {
        /* groups contain commas which is not allowed for cookies according to RFC */
        var cookieValue = encodeURIComponent(groups);
        var cookieDomain = "";
        var domains = window.location.hostname.split(".");
        if (domains.length==1) {
            /* for localhost mostly */
            cookieDomain = domains[0]
        } else if(domains.length > 1) {
            /* for staging and production domains */
            cookieDomain = "." + domains[domains.length-2] + "." + domains[domains.length-1];
        }
        /* set cookie for root path of the 2nd level domain */
        document.cookie = "stPreviousOnetrustActiveGroups=" + cookieValue + "; Max-Age=31536000; Path=/; Domain=" + cookieDomain;
    }
    /* load groups from cookie */
    function loadGroups() {
        var value = "; " + document.cookie;
        var parts = value.split("; stPreviousOnetrustActiveGroups=");
        /* in case if there are multiple cookies, taking the last one, as will be the one for root "/" path */
        if (parts.length >= 2) {
            return decodeURIComponent(parts[parts.length-1].split(";")[0]);
        }
        return "";
    }
    /* retrieve stored cookie with groups configuration */
    var stPreviousOnetrustActiveGroups = loadGroups();
    if (OnetrustActiveGroups != stPreviousOnetrustActiveGroups) {
        /*if the retrieved groups value is not equal to current one, store it and reload the page, as there were changes in consent  */
        storeGroups(OnetrustActiveGroups);
        document.cookie = "stOneTrustReloadPerformed=true; Max-Age=31536000; Path=/; Domain=.st.com";
        location.reload();
    }
}
Optanon.OnConsentChanged(ReloadPageOnConsentChange);