#include "stm32f4xx.h" 

#define true 1

void delay();

int main(void)
{
    
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;

// PD12 - GREEN LED
// PD13 - ORANGE LED
// PD14 - RED LED 
// PD15 - BLUE LED 

// MODER
// 00 - Input(reset state)
// 01 - General purpose output mode
// 10 - Alternate function mode
// 11 - Analog mode

  GPIOD->MODER &= ~0xC0000000;
  GPIOD->MODER |= 0x40000000;

// OTYPER
// 0 - output push-pull (reset state)
// 1 - output open-drain


  //GPIOD->OTYPER &= ~0xF000;
  //GPIOD->OTYPER &= 0xFFFF0FFF;

// OSPEEDR
// 00 - Low speed
// 01 - Medium speed
// 10 - Fast speed
// 11 - High speed

  //GPIOD->OSPEEDR &= 0x00FFFFFF;
  //GPIOD->OSPEEDR |= 0x55000000;

// PUPDR
// 00 - No pull-up, pull-down 
// 01 - Pull-up
// 10 - Pull-down
// 11 - Reserved

  //GPIOD->PUPDR &= 0x00FFFFFF;

  int i = 0;

  while(true)
  {
   // GPIOD->BSRRL = 1 << i;

    delay();
    if(i == 1)
    {
        i = 0;
        GPIOD->ODR |= 0x8000;
    }
    else
    {
        i = 1;
        GPIOD->ODR &= ~0x8000;
    }
    // GPIOD->BSRRH = 1 << i;

    //if(++i == 16)
     // i = 12;
  
  }


}


void delay()
{
    volatile uint32_t count = 8000000;
    while(--count);
}
