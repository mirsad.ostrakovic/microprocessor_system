#include "stm32f4xx.h"


// green LED - PD12
// orange LED - PD13
// red LED - PD14
// blue LED - PD15

// push-button - PA0

void init()
{
    //enabling clock for the GPIOA and GPIOD ports
    
    RCC->AHB1ENR &= ~(RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIODEN);
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIODEN;

    //configure PA0 - as input pin
    GPIOA->MODER &= 0xFFFFFFFC; // pin0 moder value set to 00 - input
    
    GPIOA->PUPDR &= 0xFFFFFFFC; // pin0 pupdr value reset;
    GPIOA->PUPDR |= 0x2; // pin0 pupdr value set to 10 - pull down
 

    //configure PD12, PD13, PD14, PD15 - as ouput pins
    GPIOD->MODER &= 0X00FFFFFFF; // reset pin[12-15] moder value;
    GPIOD->MODER |= 0x55000000;  // set pin[12-15] moder value to 01 - output

    GPIOD->OTYPER &= 0x0FFF; // reset pin[12-15] output type value to 0 - push-pull
    //GPIOD->OTYPER |= 0xF000; // set pin[12-15] output type value to 1 - open-drain

    GPIOD->OSPEEDR &= 0x00FFFFFF; // reset pin[12-15] output speed value
    GPIOD->OSPEEDR |= 0xAA000000; // reset pin[12-15] output speed value to 10 - fast speed 
    
    GPIOD->PUPDR &= 0x00FFFFFF; // reset pin[12-15] pupdr value
    // try to comment line belowe
    GPIOD->PUPDR |= 0x55000000; // set pin[12-15] pupdr value to 01 - pull-up 
    //GPIOD->PUPDR |= 0xAA000000; // set pin[12-15] pupdr value to 10 - pull-down
}


void delay()
{
    volatile int i;
    for(i = 0; i < 1000000; ++i);
}



int main(int argc, const char *argv[])
{
    init();

    uint8_t is_button_pushed = 0u;
    uint8_t last_state = GPIOA->IDR;
    uint8_t new_state

    while(1)
    {

       new_state = GPIOA->IDR & 0x1;

        if(last_state == 0 && new_state == 1)
            is_button_pushed = is_button_pushed > 0 ? 0 : 1;

        if(is_button_pushed)
            GPIOD->BSRRL = 0xF000;
        else
            GPIOD->BSRRH = 0xF000;
   
        last_state = new_state;
        delay();
    
    }

    return 0;
}
