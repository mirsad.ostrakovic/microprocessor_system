#include <stdio.h>
#include <string.h>

#include "stm32f4xx.h"


void sendStringUSART2(const char *string);

uint32_t getAPB1Freq()
{
    // (page 150 - reference manual)
    // APB1_freq = SYSCLK / APB1_prescaler; 
    // APB1_prescaler in (1,2,4,8,16)
   
    // (pages 163, 164 - reference manual)
    // RCC->CFGR bits [12:10] - APB1 prescaler
    // RCC->CFGR bits [15:13] - APB2 prescaler
    // Divided by:
    // 0xx - 1
    // 100 - 2
    // 101 - 4
    // 110 - 8
    // 111 - 16
    // -----------------------------------------
    // RCC->CFGR bits [7:4] - AHB prescaler
    // Divided by:
    // 0xxx - 1
    // 1000 - 2
    // 1001 - 4
    // 1010 - 8
    // 1011 - 16
    // 1100 - 64
    // 1101 - 128
    // 1110 - 256
    // 1111 - 512

    return 0u;
}


void printFrequencyConfiguration()
{
   char *msg;
  
   sendStringUSART2("AHB prescaler\r\n");

   sprintf(msg, "AHB prescaler: %ld\r\n", (RCC->CFGR >> 4) & 0xF);
  
   sprintf(msg, "hehe\r\n");

   if(strlen(msg) == 0)
       sendStringUSART2("Msg len = 0\r\n");

   sendStringUSART2(msg);

   sprintf(msg, "APB1 prescaler: %ld\r\n", (RCC->CFGR >> 10) & 0x7);
   sendStringUSART2(msg);

   sprintf(msg, "APB2 prescaler: %ld\r\n", (RCC->CFGR >> 13) & 0x7);
   sendStringUSART2(msg);
}

void initUSART2()
{
   // PD5 -> TX
   // PD6 -> RX
       
    RCC->AHB1ENR = RCC_AHB1ENR_GPIODEN; // Enable clock GPIO D port
    RCC->APB1ENR = RCC_APB1ENR_USART2EN; // Enable clock for USART2

    //(page 278 - reference manual)
    GPIOD->MODER &= 0xFFFFC3FF; // resert value of 5th and 6th pairs of bits
    GPIOD->MODER |= 0x2800; // set value of 5th and 6th pair to 10(alternate function mode)

    //(page 279 - reference manual)
    GPIOD->OTYPER &= 0xFFFFFF9F; // reset value of 5th and 6th bits, effectively set output type to push-pull

    //(page 279 - reference manual)
    GPIOD->OSPEEDR &= 0xFFFFC3FF; // resert value of 5th and 6th pairs of bits 
    GPIOD->OSPEEDR |= 0x00002800; // set value of 5th and 6th pair to 10 (fast speed) 


    //(page 280 - reference manual)
    GPIOD->PUPDR &= 0xFFFFC3FF; // reset value of 5th and 6th pairs of bits
    GPIOD->PUPDR |= 0x00001400; // set value of 5th and 6th pairs of bits to 01 (pull-up) 

    //(page 283 - reference manual, page )
    GPIOD->AFR[0] &= 0xF00FFFFF; // reset 5th and 6th 4-tuple of bits 
    GPIOD->AFR[0] |= 0x07700000; // set 5th and 6th 4-tuple of bits to value 7(0111 binary),
                                 // which effectively set AF to AF7

    //(page 57 - techinal paper)
    // AF7 on PD5 is mapped to USART2_TX
    // AF7 on PD6 is mapped to USART2_RX

    //(pages 994, 995 - reference manual)
    USART2->CR1 |= USART_CR1_UE | USART_CR1_TE; // UE - USART enable, TE - transmit enable
    USART2->BRR = 0x00001117; // for 9600 bits per second
}



void initUSART3()
{
    
   // new PC10 - TX
   // new PC11 - RX
    
    RCC->AHB1ENR = RCC_AHB1ENR_GPIOCEN; // Enable clock GPIO C port
    RCC->APB1ENR = RCC_APB1ENR_USART3EN; // Enable clock for USART3

    //(page 278 - reference manual)
    GPIOC->MODER &= 0xFF0FFFFF;
    GPIOC->MODER |= 0x00A00000; 

    //(page 279 - reference manual)
    GPIOC->OTYPER &= 0xFFFFF3FF; 

    //(page 279 - reference manual)
    GPIOC->OSPEEDR &= 0xFF0FFFFF;  
    GPIOC->OSPEEDR |= 0x00A00000;  


    //(page 280 - reference manual)
    GPIOC->PUPDR &= 0xFF0FFFFF;
    GPIOC->PUPDR |= 0x00500000;  

    //(page 283 - reference manual, page )
    GPIOC->AFR[1] &= 0xFFFF00FF; 
    GPIOC->AFR[1] |= 0x00007700;
                               

    //(page 57 - techinal paper)
    // AF7 on PD5 is mapped to USART2_TX
    // AF7 on PD6 is mapped to USART2_RX

    //(pages 994, 995 - reference manual)
    USART3->CR1 |= USART_CR1_UE | USART_CR1_TE; // UE - USART enable, TE - transmit enable
    USART3->BRR = 0x00001117; // for 9600 bits per second
}










void sendCharUSART2(char c)
{

   //(pages 991, 992, 993, 994 - reference manual)
   while((USART2->SR & USART_SR_TXE) != USART_SR_TXE); // test is transition data register empty, if not stay in the loop
   USART2->DR = c; 
}

void sendStringUSART2(const char *string)
{
    while(*string != '\0')
        sendCharUSART2(*(string++));
}






void send_char_USART3(char c)
{
    while((USART3->SR & USART_SR_TXE) != USART_SR_TXE); 
        USART3->DR = c; 
}

void print_string_USART3(const char *string)
{
    while(*string != '\0')
        send_char_USART3(*(string++));
}

void print_string(const char *string, void(*pc)(char c))
{
    while(*string != '\0')
        pc(*(string++));
    pc('m');
    pc('n');
}

void print_char(void(*f)(char c), const char *string)
{
    f('5');

    while(*string != '\0')
        f(*(string++));
}



int main()
{

  initUSART3();

  send_char_USART3('-');
  print_string_USART3("Hello from Mirso\r\n");
  print_char(send_char_USART3, "Pozz");

  while(1)
  {
     print_string("Hello people via USART3\r\n", send_char_USART3);
     send_char_USART3('-');
    // sendStringUSART2("Hello people\r\n");
    // printFrequencyConfiguration();
 }
//  while(1)
//    sendStringUSART2("Hello world\r\n");

  return 0;
}

