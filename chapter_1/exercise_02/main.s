	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"main.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	send_char_USART3
	.thumb
	.thumb_func
	.type	send_char_USART3, %function
send_char_USART3:
.LFB116:
	.file 1 "main.c"
	.loc 1 171 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL0:
	.loc 1 172 0
	ldr	r1, .L6
.L2:
	.loc 1 172 0 is_stmt 0 discriminator 1
	ldrh	r3, [r1]
	ldr	r2, .L6
	lsls	r3, r3, #24
	bpl	.L2
	.loc 1 173 0 is_stmt 1
	uxth	r0, r0
.LVL1:
	strh	r0, [r2, #4]	@ movhi
	bx	lr
.L7:
	.align	2
.L6:
	.word	1073760256
	.cfi_endproc
.LFE116:
	.size	send_char_USART3, .-send_char_USART3
	.align	2
	.global	getAPB1Freq
	.thumb
	.thumb_func
	.type	getAPB1Freq, %function
getAPB1Freq:
.LFB110:
	.loc 1 10 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 38 0
	movs	r0, #0
	bx	lr
	.cfi_endproc
.LFE110:
	.size	getAPB1Freq, .-getAPB1Freq
	.align	2
	.global	printFrequencyConfiguration
	.thumb
	.thumb_func
	.type	printFrequencyConfiguration, %function
printFrequencyConfiguration:
.LFB111:
	.loc 1 42 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL2:
.LBB36:
.LBB37:
.LBB38:
.LBB39:
	.loc 1 155 0
	ldr	r2, .L47
	ldr	r0, .L47+4
.LBE39:
.LBE38:
.LBE37:
.LBE36:
	.loc 1 42 0
	push	{r4, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 4, -8
	.cfi_offset 14, -4
	.loc 1 42 0
	movs	r1, #65
.LBB43:
.LBB42:
.LBB41:
.LBB40:
	.loc 1 155 0
	mov	r4, r2
.LVL3:
.L10:
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L10
	.loc 1 156 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LBE40:
.LBE41:
	.loc 1 161 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L10
.LBE42:
.LBE43:
	.loc 1 47 0
	ldr	r3, .L47+8
	ldr	r1, .L47+12
	ldr	r2, [r3, #8]
	movs	r4, #0
	ubfx	r2, r2, #4, #4
	mov	r0, r4
	bl	sprintf
.LVL4:
	.loc 1 49 0
	ldr	r3, .L47+16
	ldr	r0, [r3]	@ unaligned
	str	r0, [r4]	@ unaligned
	.loc 1 51 0
	ldrb	r1, [r4]	@ zero_extendqisi2
	.loc 1 49 0
	ldrh	r2, [r3, #4]	@ unaligned
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
	strh	r2, [r4, #4]	@ unaligned
	strb	r3, [r4, #6]
	.loc 1 51 0
	cbnz	r1, .L13
.LBB44:
.LBB45:
.LBB46:
.LBB47:
	.loc 1 155 0
	ldr	r2, .L47
	ldr	r0, .L47+20
	mov	r4, r2
.LBE47:
.LBE46:
.LBE45:
.LBE44:
	.loc 1 51 0
	movs	r1, #77
.L16:
.LBB51:
.LBB50:
.LBB49:
.LBB48:
	.loc 1 155 0
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L16
	.loc 1 156 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LVL5:
.LBE48:
.LBE49:
	.loc 1 161 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L16
.LBE50:
.LBE51:
.LBB52:
.LBB53:
	movs	r4, #0
	ldrb	r1, [r4]	@ zero_extendqisi2
	cbz	r1, .L21
.L13:
.LBB54:
.LBB55:
	.loc 1 155 0
	ldr	r2, .L47
	movs	r0, #0
	mov	r4, r2
.L19:
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L19
	.loc 1 156 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LVL6:
.LBE55:
.LBE54:
	.loc 1 161 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L19
.L21:
.LBE53:
.LBE52:
	.loc 1 56 0
	ldr	r3, .L47+8
	ldr	r1, .L47+24
	ldr	r2, [r3, #8]
	movs	r4, #0
	ubfx	r2, r2, #10, #3
	mov	r0, r4
	bl	sprintf
.LVL7:
.LBB56:
.LBB57:
	.loc 1 161 0
	ldrb	r1, [r4]	@ zero_extendqisi2
	cbz	r1, .L15
.LBB58:
.LBB59:
	.loc 1 155 0
	ldr	r2, .L47
	movs	r0, #0
	mov	r4, r2
.LVL8:
.L24:
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L24
	.loc 1 156 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LBE59:
.LBE58:
	.loc 1 161 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L24
.L15:
.LBE57:
.LBE56:
	.loc 1 59 0
	ldr	r3, .L47+8
	ldr	r1, .L47+28
	ldr	r2, [r3, #8]
	movs	r4, #0
	ubfx	r2, r2, #13, #3
	mov	r0, r4
	bl	sprintf
.LVL9:
.LBB60:
.LBB61:
	.loc 1 161 0
	ldrb	r1, [r4]	@ zero_extendqisi2
	cbz	r1, .L46
.LBB62:
.LBB63:
	.loc 1 155 0
	ldr	r2, .L47
	movs	r0, #0
	mov	r4, r2
.LVL10:
.L26:
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L26
	.loc 1 156 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LBE63:
.LBE62:
	.loc 1 161 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L26
	pop	{r4, pc}
.LVL11:
.L46:
	pop	{r4, pc}
.LVL12:
.L48:
	.align	2
.L47:
	.word	1073759232
	.word	.LC0
	.word	1073887232
	.word	.LC1
	.word	.LC2
	.word	.LC4
	.word	.LC3
	.word	.LC5
.LBE61:
.LBE60:
	.cfi_endproc
.LFE111:
	.size	printFrequencyConfiguration, .-printFrequencyConfiguration
	.align	2
	.global	initUSART2
	.thumb
	.thumb_func
	.type	initUSART2, %function
initUSART2:
.LFB112:
	.loc 1 64 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 68 0
	ldr	r1, .L51
	.loc 1 72 0
	ldr	r3, .L51+4
	.loc 1 97 0
	ldr	r2, .L51+8
	.loc 1 64 0
	push	{r4}
	.cfi_def_cfa_offset 4
	.cfi_offset 4, -4
	.loc 1 69 0
	mov	r0, #131072
	.loc 1 68 0
	movs	r4, #8
	str	r4, [r1, #48]
	.loc 1 69 0
	str	r0, [r1, #64]
	.loc 1 72 0
	ldr	r1, [r3]
	bic	r1, r1, #15360
	str	r1, [r3]
	.loc 1 73 0
	ldr	r1, [r3]
	orr	r1, r1, #10240
	str	r1, [r3]
	.loc 1 76 0
	ldr	r1, [r3, #4]
	bic	r1, r1, #96
	str	r1, [r3, #4]
	.loc 1 79 0
	ldr	r1, [r3, #8]
	bic	r1, r1, #15360
	str	r1, [r3, #8]
	.loc 1 80 0
	ldr	r1, [r3, #8]
	orr	r1, r1, #10240
	str	r1, [r3, #8]
	.loc 1 84 0
	ldr	r1, [r3, #12]
	bic	r1, r1, #15360
	str	r1, [r3, #12]
	.loc 1 85 0
	ldr	r1, [r3, #12]
	orr	r1, r1, #5120
	str	r1, [r3, #12]
	.loc 1 88 0
	ldr	r1, [r3, #32]
	bic	r1, r1, #267386880
	str	r1, [r3, #32]
	.loc 1 89 0
	ldr	r1, [r3, #32]
	orr	r1, r1, #124780544
	str	r1, [r3, #32]
	.loc 1 97 0
	ldrh	r3, [r2, #12]
	uxth	r3, r3
	orr	r3, r3, #8192
	orrs	r3, r3, r4
	.loc 1 98 0
	movw	r1, #4375
	.loc 1 97 0
	strh	r3, [r2, #12]	@ movhi
	.loc 1 99 0
	ldr	r4, [sp], #4
	.cfi_restore 4
	.cfi_def_cfa_offset 0
	.loc 1 98 0
	strh	r1, [r2, #8]	@ movhi
	.loc 1 99 0
	bx	lr
.L52:
	.align	2
.L51:
	.word	1073887232
	.word	1073875968
	.word	1073759232
	.cfi_endproc
.LFE112:
	.size	initUSART2, .-initUSART2
	.align	2
	.global	initUSART3
	.thumb
	.thumb_func
	.type	initUSART3, %function
initUSART3:
.LFB113:
	.loc 1 104 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 109 0
	ldr	r1, .L55
	.loc 1 113 0
	ldr	r3, .L55+4
	.loc 1 138 0
	ldr	r2, .L55+8
	.loc 1 104 0
	push	{r4}
	.cfi_def_cfa_offset 4
	.cfi_offset 4, -4
	.loc 1 110 0
	mov	r0, #262144
	.loc 1 109 0
	movs	r4, #4
	str	r4, [r1, #48]
	.loc 1 110 0
	str	r0, [r1, #64]
	.loc 1 113 0
	ldr	r1, [r3]
	.loc 1 140 0
	ldr	r4, [sp], #4
	.cfi_restore 4
	.cfi_def_cfa_offset 0
	.loc 1 113 0
	bic	r1, r1, #15728640
	str	r1, [r3]
	.loc 1 114 0
	ldr	r1, [r3]
	orr	r1, r1, #10485760
	str	r1, [r3]
	.loc 1 117 0
	ldr	r1, [r3, #4]
	bic	r1, r1, #3072
	str	r1, [r3, #4]
	.loc 1 120 0
	ldr	r1, [r3, #8]
	bic	r1, r1, #15728640
	str	r1, [r3, #8]
	.loc 1 121 0
	ldr	r1, [r3, #8]
	orr	r1, r1, #10485760
	str	r1, [r3, #8]
	.loc 1 125 0
	ldr	r1, [r3, #12]
	bic	r1, r1, #15728640
	str	r1, [r3, #12]
	.loc 1 126 0
	ldr	r1, [r3, #12]
	orr	r1, r1, #5242880
	str	r1, [r3, #12]
	.loc 1 129 0
	ldr	r1, [r3, #36]
	bic	r1, r1, #65280
	str	r1, [r3, #36]
	.loc 1 130 0
	ldr	r1, [r3, #36]
	orr	r1, r1, #30464
	str	r1, [r3, #36]
	.loc 1 138 0
	ldrh	r3, [r2, #12]
	uxth	r3, r3
	orr	r3, r3, #8192
	orr	r3, r3, #8
	.loc 1 139 0
	movw	r1, #4375
	.loc 1 138 0
	strh	r3, [r2, #12]	@ movhi
	.loc 1 139 0
	strh	r1, [r2, #8]	@ movhi
	.loc 1 140 0
	bx	lr
.L56:
	.align	2
.L55:
	.word	1073887232
	.word	1073874944
	.word	1073760256
	.cfi_endproc
.LFE113:
	.size	initUSART3, .-initUSART3
	.align	2
	.global	sendCharUSART2
	.thumb
	.thumb_func
	.type	sendCharUSART2, %function
sendCharUSART2:
.LFB114:
	.loc 1 152 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL13:
	.loc 1 155 0
	ldr	r1, .L61
.L58:
	.loc 1 155 0 is_stmt 0 discriminator 1
	ldrh	r3, [r1]
	ldr	r2, .L61
	lsls	r3, r3, #24
	bpl	.L58
	.loc 1 156 0 is_stmt 1
	uxth	r0, r0
.LVL14:
	strh	r0, [r2, #4]	@ movhi
	bx	lr
.L62:
	.align	2
.L61:
	.word	1073759232
	.cfi_endproc
.LFE114:
	.size	sendCharUSART2, .-sendCharUSART2
	.align	2
	.global	sendStringUSART2
	.thumb
	.thumb_func
	.type	sendStringUSART2, %function
sendStringUSART2:
.LFB115:
	.loc 1 160 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 161 0
	ldrb	r1, [r0]	@ zero_extendqisi2
	cbz	r1, .L73
.LBB64:
.LBB65:
	.loc 1 155 0
	ldr	r2, .L74
.LBE65:
.LBE64:
	.loc 1 160 0
	push	{r4}
	.cfi_def_cfa_offset 4
	.cfi_offset 4, -4
.LBB67:
.LBB66:
	.loc 1 155 0
	mov	r4, r2
.L65:
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L65
	.loc 1 156 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LBE66:
.LBE67:
	.loc 1 161 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L65
	.loc 1 163 0
	ldr	r4, [sp], #4
	.cfi_restore 4
	.cfi_def_cfa_offset 0
.L73:
	bx	lr
.L75:
	.align	2
.L74:
	.word	1073759232
	.cfi_endproc
.LFE115:
	.size	sendStringUSART2, .-sendStringUSART2
	.align	2
	.global	print_string_USART3
	.thumb
	.thumb_func
	.type	print_string_USART3, %function
print_string_USART3:
.LFB117:
	.loc 1 177 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 178 0
	ldrb	r1, [r0]	@ zero_extendqisi2
	cbz	r1, .L86
.LBB68:
.LBB69:
	.loc 1 172 0
	ldr	r2, .L87
.LBE69:
.LBE68:
	.loc 1 177 0
	push	{r4}
	.cfi_def_cfa_offset 4
	.cfi_offset 4, -4
.LBB71:
.LBB70:
	.loc 1 172 0
	mov	r4, r2
.L78:
	ldrh	r3, [r2]
	lsls	r3, r3, #24
	bpl	.L78
	.loc 1 173 0
	uxth	r1, r1
	strh	r1, [r4, #4]	@ movhi
.LBE70:
.LBE71:
	.loc 1 178 0
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L78
	.loc 1 180 0
	ldr	r4, [sp], #4
	.cfi_restore 4
	.cfi_def_cfa_offset 0
.L86:
	bx	lr
.L88:
	.align	2
.L87:
	.word	1073760256
	.cfi_endproc
.LFE117:
	.size	print_string_USART3, .-print_string_USART3
	.align	2
	.global	print_string
	.thumb
	.thumb_func
	.type	print_string, %function
print_string:
.LFB118:
	.loc 1 183 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL15:
	push	{r4, r5, r6, lr}
	.cfi_def_cfa_offset 16
	.cfi_offset 4, -16
	.cfi_offset 5, -12
	.cfi_offset 6, -8
	.cfi_offset 14, -4
	.loc 1 183 0
	mov	r4, r0
	.loc 1 184 0
	ldrb	r0, [r0]	@ zero_extendqisi2
.LVL16:
	.loc 1 183 0
	mov	r5, r1
	.loc 1 184 0
	cbz	r0, .L92
.LVL17:
.L91:
	.loc 1 185 0
	blx	r5
.LVL18:
	.loc 1 184 0
	ldrb	r0, [r4, #1]!	@ zero_extendqisi2
.LVL19:
	cmp	r0, #0
	bne	.L91
.LVL20:
.L92:
	.loc 1 186 0
	movs	r0, #109
	blx	r5
.LVL21:
	.loc 1 187 0
	mov	r3, r5
	movs	r0, #110
	.loc 1 188 0
	pop	{r4, r5, r6, lr}
	.cfi_restore 14
	.cfi_restore 6
	.cfi_restore 5
	.cfi_restore 4
	.cfi_def_cfa_offset 0
.LVL22:
	.loc 1 187 0
	bx	r3	@ indirect register sibling call
.LVL23:
	.cfi_endproc
.LFE118:
	.size	print_string, .-print_string
	.align	2
	.global	print_char
	.thumb
	.thumb_func
	.type	print_char, %function
print_char:
.LFB119:
	.loc 1 191 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL24:
	push	{r3, r4, r5, lr}
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	.cfi_offset 4, -12
	.cfi_offset 5, -8
	.cfi_offset 14, -4
	.loc 1 191 0
	mov	r4, r1
	mov	r5, r0
	.loc 1 192 0
	movs	r0, #53
.LVL25:
	blx	r5
.LVL26:
	.loc 1 194 0
	ldrb	r0, [r4]	@ zero_extendqisi2
	cbz	r0, .L95
.LVL27:
.L97:
	.loc 1 195 0
	blx	r5
.LVL28:
	.loc 1 194 0
	ldrb	r0, [r4, #1]!	@ zero_extendqisi2
.LVL29:
	cmp	r0, #0
	bne	.L97
.LVL30:
.L95:
	pop	{r3, r4, r5, pc}
	.cfi_endproc
.LFE119:
	.size	print_char, .-print_char
	.section	.text.startup,"ax",%progbits
	.align	2
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
.LFB120:
	.loc 1 201 0
	.cfi_startproc
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r3, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	.cfi_offset 14, -4
	.loc 1 203 0
	bl	initUSART3
.LVL31:
.LBB72:
.LBB73:
	.loc 1 172 0
	ldr	r2, .L120
.L102:
	ldrh	r1, [r2]
	ldr	r3, .L120
	lsls	r5, r1, #24
	bpl	.L102
	.loc 1 173 0
	movs	r0, #45
	ldr	r1, .L120+4
	strh	r0, [r3, #4]	@ movhi
.LVL32:
.LBE73:
.LBE72:
.LBB75:
.LBB76:
.LBB77:
.LBB78:
	.loc 1 172 0
	mov	r2, r3
	mov	r0, r3
.LBE78:
.LBE77:
.LBE76:
.LBE75:
.LBB82:
.LBB74:
	.loc 1 173 0
	movs	r3, #72
.LVL33:
.L103:
.LBE74:
.LBE82:
.LBB83:
.LBB81:
.LBB80:
.LBB79:
	.loc 1 172 0
	ldrh	r4, [r2]
	lsls	r4, r4, #24
	bpl	.L103
	.loc 1 173 0
	uxth	r3, r3
	strh	r3, [r0, #4]	@ movhi
.LBE79:
.LBE80:
	.loc 1 178 0
	ldrb	r3, [r1, #1]!	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L103
.LVL34:
.LBE81:
.LBE83:
.LBB84:
.LBB85:
	.loc 1 192 0
	movs	r0, #53
	ldr	r4, .L120+8
	bl	send_char_USART3
.LVL35:
	movs	r0, #80
.LVL36:
.L106:
	.loc 1 195 0
	bl	send_char_USART3
.LVL37:
	.loc 1 194 0
	ldrb	r0, [r4, #1]!	@ zero_extendqisi2
.LVL38:
	cmp	r0, #0
	bne	.L106
.LBE85:
.LBE84:
.LBB86:
.LBB87:
	.loc 1 172 0
	ldr	r4, .L120
.LVL39:
	mov	r5, r4
.L108:
.LBE87:
.LBE86:
	.loc 1 211 0 discriminator 1
	ldr	r0, .L120+12
	ldr	r1, .L120+16
	bl	print_string
.LVL40:
.L107:
.LBB89:
.LBB88:
	.loc 1 172 0
	ldrh	r3, [r4]
	lsls	r3, r3, #24
	bpl	.L107
	.loc 1 173 0
	movs	r3, #45
	strh	r3, [r5, #4]	@ movhi
	b	.L108
.L121:
	.align	2
.L120:
	.word	1073760256
	.word	.LC7
	.word	.LC6
	.word	.LC8
	.word	send_char_USART3
.LBE88:
.LBE89:
	.cfi_endproc
.LFE120:
	.size	main, .-main
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"AHB prescaler\015\012\000"
.LC1:
	.ascii	"AHB prescaler: %ld\015\012\000"
	.space	3
.LC2:
	.ascii	"hehe\015\012\000"
	.space	1
.LC3:
	.ascii	"APB1 prescaler: %ld\015\012\000"
	.space	2
.LC4:
	.ascii	"Msg len = 0\015\012\000"
	.space	2
.LC5:
	.ascii	"APB2 prescaler: %ld\015\012\000"
	.space	2
.LC6:
	.ascii	"Pozz\000"
	.space	3
.LC7:
	.ascii	"Hello from Mirso\015\012\000"
	.space	1
.LC8:
	.ascii	"Hello people via USART3\015\012\000"
	.text
.Letext0:
	.file 2 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdio.h"
	.file 6 "../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x847
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF66
	.byte	0x1
	.4byte	.LASF67
	.4byte	.LASF68
	.4byte	.Ldebug_ranges0+0xf0
	.4byte	0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x2b
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3f
	.4byte	0x57
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x41
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x5
	.byte	0x4
	.4byte	0x99
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF13
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa6
	.uleb128 0x6
	.4byte	0x99
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x21
	.4byte	0x3a
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2c
	.4byte	0x4c
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x2d
	.4byte	0x5e
	.uleb128 0x7
	.4byte	0xc1
	.uleb128 0x8
	.4byte	0xc1
	.4byte	0xe1
	.uleb128 0x9
	.4byte	0x8c
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.4byte	0xab
	.uleb128 0x7
	.4byte	0xb6
	.uleb128 0xa
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x177
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.2byte	0x290
	.4byte	0xcc
	.byte	0
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x291
	.4byte	0xcc
	.byte	0x4
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x292
	.4byte	0xcc
	.byte	0x8
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x293
	.4byte	0xcc
	.byte	0xc
	.uleb128 0xc
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xcc
	.byte	0x10
	.uleb128 0xc
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xcc
	.byte	0x14
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x296
	.4byte	0xe1
	.byte	0x18
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x297
	.4byte	0xe1
	.byte	0x1a
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x298
	.4byte	0xcc
	.byte	0x1c
	.uleb128 0xc
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x177
	.byte	0x20
	.byte	0
	.uleb128 0x7
	.4byte	0xd1
	.uleb128 0xd
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x29a
	.4byte	0xeb
	.uleb128 0xa
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x317
	.uleb128 0xc
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xcc
	.byte	0
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xcc
	.byte	0x4
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xcc
	.byte	0x8
	.uleb128 0xc
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xcc
	.byte	0xc
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xcc
	.byte	0x10
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xcc
	.byte	0x14
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xcc
	.byte	0x18
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xc1
	.byte	0x1c
	.uleb128 0xb
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xcc
	.byte	0x20
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xcc
	.byte	0x24
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xd1
	.byte	0x28
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xcc
	.byte	0x30
	.uleb128 0xb
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xcc
	.byte	0x34
	.uleb128 0xb
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xcc
	.byte	0x38
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xc1
	.byte	0x3c
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xcc
	.byte	0x40
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xcc
	.byte	0x44
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xd1
	.byte	0x48
	.uleb128 0xb
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xcc
	.byte	0x50
	.uleb128 0xb
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xcc
	.byte	0x54
	.uleb128 0xb
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xcc
	.byte	0x58
	.uleb128 0xb
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xc1
	.byte	0x5c
	.uleb128 0xb
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xcc
	.byte	0x60
	.uleb128 0xb
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xcc
	.byte	0x64
	.uleb128 0xb
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xd1
	.byte	0x68
	.uleb128 0xb
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xcc
	.byte	0x70
	.uleb128 0xc
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xcc
	.byte	0x74
	.uleb128 0xb
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xd1
	.byte	0x78
	.uleb128 0xb
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xcc
	.byte	0x80
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xcc
	.byte	0x84
	.byte	0
	.uleb128 0xd
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x188
	.uleb128 0xa
	.byte	0x1c
	.byte	0x4
	.2byte	0x395
	.4byte	0x3e1
	.uleb128 0xc
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x397
	.4byte	0xe1
	.byte	0
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x398
	.4byte	0xab
	.byte	0x2
	.uleb128 0xc
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x399
	.4byte	0xe1
	.byte	0x4
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x39a
	.4byte	0xab
	.byte	0x6
	.uleb128 0xc
	.ascii	"BRR\000"
	.byte	0x4
	.2byte	0x39b
	.4byte	0xe1
	.byte	0x8
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x39c
	.4byte	0xab
	.byte	0xa
	.uleb128 0xc
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x39d
	.4byte	0xe1
	.byte	0xc
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x39e
	.4byte	0xab
	.byte	0xe
	.uleb128 0xc
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x39f
	.4byte	0xe1
	.byte	0x10
	.uleb128 0xb
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x3a0
	.4byte	0xab
	.byte	0x12
	.uleb128 0xc
	.ascii	"CR3\000"
	.byte	0x4
	.2byte	0x3a1
	.4byte	0xe1
	.byte	0x14
	.uleb128 0xb
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x3a2
	.4byte	0xab
	.byte	0x16
	.uleb128 0xb
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x3a3
	.4byte	0xe1
	.byte	0x18
	.uleb128 0xb
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x3a4
	.4byte	0xab
	.byte	0x1a
	.byte	0
	.uleb128 0xd
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x3a5
	.4byte	0x323
	.uleb128 0xe
	.4byte	.LASF55
	.byte	0x1
	.byte	0x97
	.byte	0x1
	.4byte	0x403
	.uleb128 0xf
	.ascii	"c\000"
	.byte	0x1
	.byte	0x97
	.4byte	0x99
	.byte	0
	.uleb128 0xe
	.4byte	.LASF56
	.byte	0x1
	.byte	0x9f
	.byte	0x1
	.4byte	0x41b
	.uleb128 0x10
	.4byte	.LASF57
	.byte	0x1
	.byte	0x9f
	.4byte	0xa0
	.byte	0
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0x1
	.byte	0xaa
	.byte	0x1
	.4byte	0x431
	.uleb128 0xf
	.ascii	"c\000"
	.byte	0x1
	.byte	0xaa
	.4byte	0x99
	.byte	0
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x1
	.byte	0xb0
	.byte	0x1
	.4byte	0x449
	.uleb128 0x10
	.4byte	.LASF57
	.byte	0x1
	.byte	0xb0
	.4byte	0xa0
	.byte	0
	.uleb128 0xe
	.4byte	.LASF60
	.byte	0x1
	.byte	0xbe
	.byte	0x1
	.4byte	0x46a
	.uleb128 0xf
	.ascii	"f\000"
	.byte	0x1
	.byte	0xbe
	.4byte	0x475
	.uleb128 0x10
	.4byte	.LASF57
	.byte	0x1
	.byte	0xbe
	.4byte	0xa0
	.byte	0
	.uleb128 0x11
	.4byte	0x475
	.uleb128 0x12
	.4byte	0x99
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x46a
	.uleb128 0x13
	.4byte	0x41b
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x498
	.uleb128 0x14
	.4byte	0x427
	.4byte	.LLST0
	.byte	0
	.uleb128 0x15
	.4byte	.LASF69
	.byte	0x1
	.byte	0x9
	.4byte	0xc1
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x16
	.4byte	.LASF70
	.byte	0x1
	.byte	0x29
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x615
	.uleb128 0x17
	.ascii	"msg\000"
	.byte	0x1
	.byte	0x2b
	.4byte	0x93
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x18
	.4byte	0x403
	.4byte	.LBB36
	.4byte	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x2d
	.4byte	0x501
	.uleb128 0x14
	.4byte	0x40f
	.4byte	.LLST1
	.uleb128 0x19
	.4byte	0x3ed
	.4byte	.LBB38
	.4byte	.Ldebug_ranges0+0x18
	.byte	0x1
	.byte	0xa2
	.uleb128 0x1a
	.4byte	0x3f9
	.byte	0
	.byte	0
	.uleb128 0x18
	.4byte	0x403
	.4byte	.LBB44
	.4byte	.Ldebug_ranges0+0x30
	.byte	0x1
	.byte	0x34
	.4byte	0x52f
	.uleb128 0x1a
	.4byte	0x40f
	.uleb128 0x19
	.4byte	0x3ed
	.4byte	.LBB46
	.4byte	.Ldebug_ranges0+0x48
	.byte	0x1
	.byte	0xa2
	.uleb128 0x1a
	.4byte	0x3f9
	.byte	0
	.byte	0
	.uleb128 0x1b
	.4byte	0x403
	.4byte	.LBB52
	.4byte	.LBE52-.LBB52
	.byte	0x1
	.byte	0x36
	.4byte	0x55d
	.uleb128 0x1a
	.4byte	0x40f
	.uleb128 0x1c
	.4byte	0x3ed
	.4byte	.LBB54
	.4byte	.LBE54-.LBB54
	.byte	0x1
	.byte	0xa2
	.uleb128 0x1a
	.4byte	0x3f9
	.byte	0
	.byte	0
	.uleb128 0x1b
	.4byte	0x403
	.4byte	.LBB56
	.4byte	.LBE56-.LBB56
	.byte	0x1
	.byte	0x39
	.4byte	0x58f
	.uleb128 0x14
	.4byte	0x40f
	.4byte	.LLST2
	.uleb128 0x1c
	.4byte	0x3ed
	.4byte	.LBB58
	.4byte	.LBE58-.LBB58
	.byte	0x1
	.byte	0xa2
	.uleb128 0x1a
	.4byte	0x3f9
	.byte	0
	.byte	0
	.uleb128 0x1b
	.4byte	0x403
	.4byte	.LBB60
	.4byte	.LBE60-.LBB60
	.byte	0x1
	.byte	0x3c
	.4byte	0x5c1
	.uleb128 0x14
	.4byte	0x40f
	.4byte	.LLST3
	.uleb128 0x1c
	.4byte	0x3ed
	.4byte	.LBB62
	.4byte	.LBE62-.LBB62
	.byte	0x1
	.byte	0xa2
	.uleb128 0x1a
	.4byte	0x3f9
	.byte	0
	.byte	0
	.uleb128 0x1d
	.4byte	.LVL4
	.4byte	0x833
	.4byte	0x5de
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x5
	.byte	0x3
	.4byte	.LC1
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x74
	.sleb128 0
	.byte	0
	.uleb128 0x1d
	.4byte	.LVL7
	.4byte	0x833
	.4byte	0x5fb
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x5
	.byte	0x3
	.4byte	.LC3
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x74
	.sleb128 0
	.byte	0
	.uleb128 0x1f
	.4byte	.LVL9
	.4byte	0x833
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x5
	.byte	0x3
	.4byte	.LC5
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x74
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x20
	.4byte	.LASF61
	.byte	0x1
	.byte	0x3f
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x20
	.4byte	.LASF62
	.byte	0x1
	.byte	0x67
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x13
	.4byte	0x3ed
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x654
	.uleb128 0x14
	.4byte	0x3f9
	.4byte	.LLST4
	.byte	0
	.uleb128 0x13
	.4byte	0x403
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x684
	.uleb128 0x21
	.4byte	0x40f
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x19
	.4byte	0x3ed
	.4byte	.LBB64
	.4byte	.Ldebug_ranges0+0x60
	.byte	0x1
	.byte	0xa2
	.uleb128 0x1a
	.4byte	0x3f9
	.byte	0
	.byte	0
	.uleb128 0x13
	.4byte	0x431
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x6b4
	.uleb128 0x21
	.4byte	0x43d
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x19
	.4byte	0x41b
	.4byte	.LBB68
	.4byte	.Ldebug_ranges0+0x78
	.byte	0x1
	.byte	0xb3
	.uleb128 0x1a
	.4byte	0x427
	.byte	0
	.byte	0
	.uleb128 0x22
	.4byte	.LASF63
	.byte	0x1
	.byte	0xb6
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x707
	.uleb128 0x23
	.4byte	.LASF57
	.byte	0x1
	.byte	0xb6
	.4byte	0xa0
	.4byte	.LLST5
	.uleb128 0x24
	.ascii	"pc\000"
	.byte	0x1
	.byte	0xb6
	.4byte	0x475
	.4byte	.LLST6
	.uleb128 0x25
	.4byte	.LVL21
	.4byte	0x6f6
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x8
	.byte	0x6d
	.byte	0
	.uleb128 0x26
	.4byte	.LVL23
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x8
	.byte	0x6e
	.byte	0
	.byte	0
	.uleb128 0x27
	.4byte	0x449
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x73c
	.uleb128 0x14
	.4byte	0x455
	.4byte	.LLST7
	.uleb128 0x14
	.4byte	0x45e
	.4byte	.LLST8
	.uleb128 0x28
	.4byte	.LVL26
	.uleb128 0x2
	.byte	0x75
	.sleb128 0
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x8
	.byte	0x35
	.byte	0
	.byte	0
	.uleb128 0x29
	.4byte	.LASF64
	.byte	0x1
	.byte	0xc8
	.4byte	0x7e
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x827
	.uleb128 0x18
	.4byte	0x41b
	.4byte	.LBB72
	.4byte	.Ldebug_ranges0+0x90
	.byte	0x1
	.byte	0xcd
	.4byte	0x76f
	.uleb128 0x2a
	.4byte	0x427
	.byte	0x2d
	.byte	0
	.uleb128 0x18
	.4byte	0x431
	.4byte	.LBB75
	.4byte	.Ldebug_ranges0+0xa8
	.byte	0x1
	.byte	0xce
	.4byte	0x7a1
	.uleb128 0x14
	.4byte	0x43d
	.4byte	.LLST9
	.uleb128 0x19
	.4byte	0x41b
	.4byte	.LBB77
	.4byte	.Ldebug_ranges0+0xc0
	.byte	0x1
	.byte	0xb3
	.uleb128 0x1a
	.4byte	0x427
	.byte	0
	.byte	0
	.uleb128 0x1b
	.4byte	0x449
	.4byte	.LBB84
	.4byte	.LBE84-.LBB84
	.byte	0x1
	.byte	0xcf
	.4byte	0x7e7
	.uleb128 0x14
	.4byte	0x45e
	.4byte	.LLST10
	.uleb128 0x21
	.4byte	0x455
	.uleb128 0x6
	.byte	0x3
	.4byte	send_char_USART3
	.byte	0x9f
	.uleb128 0x1d
	.4byte	.LVL35
	.4byte	0x41b
	.4byte	0x7dd
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x2
	.byte	0x8
	.byte	0x35
	.byte	0
	.uleb128 0x2b
	.4byte	.LVL37
	.4byte	0x41b
	.byte	0
	.uleb128 0x18
	.4byte	0x41b
	.4byte	.LBB86
	.4byte	.Ldebug_ranges0+0xd8
	.byte	0x1
	.byte	0xd4
	.4byte	0x801
	.uleb128 0x2a
	.4byte	0x427
	.byte	0x2d
	.byte	0
	.uleb128 0x2b
	.4byte	.LVL31
	.4byte	0x626
	.uleb128 0x1f
	.4byte	.LVL40
	.4byte	0x6b4
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x5
	.byte	0x3
	.4byte	send_char_USART3
	.uleb128 0x1e
	.uleb128 0x1
	.byte	0x50
	.uleb128 0x5
	.byte	0x3
	.4byte	.LC8
	.byte	0
	.byte	0
	.uleb128 0x2c
	.4byte	.LASF71
	.byte	0x6
	.2byte	0x51b
	.4byte	0xe6
	.uleb128 0x2d
	.4byte	.LASF65
	.byte	0x5
	.byte	0xdd
	.4byte	0x7e
	.uleb128 0x12
	.4byte	0x93
	.uleb128 0x12
	.4byte	0xa0
	.uleb128 0x2e
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x2113
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2113
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LVL0
	.4byte	.LVL1
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL1
	.4byte	.LFE116
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x50
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LVL2
	.4byte	.LVL3
	.2byte	0x6
	.byte	0x3
	.4byte	.LC0
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LVL7
	.4byte	.LVL8
	.2byte	0x1
	.byte	0x54
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LVL9
	.4byte	.LVL10
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL11
	.4byte	.LVL12
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL12
	.4byte	.LFE111
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LVL13
	.4byte	.LVL14
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL14
	.4byte	.LFE114
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x50
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LVL15
	.4byte	.LVL16
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL16
	.4byte	.LVL19
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL19
	.4byte	.LVL20
	.2byte	0x3
	.byte	0x74
	.sleb128 -1
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LVL15
	.4byte	.LVL17
	.2byte	0x1
	.byte	0x51
	.4byte	.LVL17
	.4byte	.LVL22
	.2byte	0x1
	.byte	0x55
	.4byte	.LVL22
	.4byte	.LVL23-1
	.2byte	0x1
	.byte	0x53
	.4byte	.LVL23-1
	.4byte	.LFE118
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LVL24
	.4byte	.LVL25
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL25
	.4byte	.LFE119
	.2byte	0x1
	.byte	0x55
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LVL24
	.4byte	.LVL26-1
	.2byte	0x1
	.byte	0x51
	.4byte	.LVL26-1
	.4byte	.LVL29
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL29
	.4byte	.LVL30
	.2byte	0x3
	.byte	0x74
	.sleb128 -1
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LVL32
	.4byte	.LVL33
	.2byte	0x6
	.byte	0x3
	.4byte	.LC7
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LVL34
	.4byte	.LVL36
	.2byte	0x6
	.byte	0x3
	.4byte	.LC6
	.byte	0x9f
	.4byte	.LVL36
	.4byte	.LVL38
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL38
	.4byte	.LVL39
	.2byte	0x3
	.byte	0x74
	.sleb128 -1
	.byte	0x9f
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LBB36
	.4byte	.LBE36
	.4byte	.LBB43
	.4byte	.LBE43
	.4byte	0
	.4byte	0
	.4byte	.LBB38
	.4byte	.LBE38
	.4byte	.LBB41
	.4byte	.LBE41
	.4byte	0
	.4byte	0
	.4byte	.LBB44
	.4byte	.LBE44
	.4byte	.LBB51
	.4byte	.LBE51
	.4byte	0
	.4byte	0
	.4byte	.LBB46
	.4byte	.LBE46
	.4byte	.LBB49
	.4byte	.LBE49
	.4byte	0
	.4byte	0
	.4byte	.LBB64
	.4byte	.LBE64
	.4byte	.LBB67
	.4byte	.LBE67
	.4byte	0
	.4byte	0
	.4byte	.LBB68
	.4byte	.LBE68
	.4byte	.LBB71
	.4byte	.LBE71
	.4byte	0
	.4byte	0
	.4byte	.LBB72
	.4byte	.LBE72
	.4byte	.LBB82
	.4byte	.LBE82
	.4byte	0
	.4byte	0
	.4byte	.LBB75
	.4byte	.LBE75
	.4byte	.LBB83
	.4byte	.LBE83
	.4byte	0
	.4byte	0
	.4byte	.LBB77
	.4byte	.LBE77
	.4byte	.LBB80
	.4byte	.LBE80
	.4byte	0
	.4byte	0
	.4byte	.LBB86
	.4byte	.LBE86
	.4byte	.LBB89
	.4byte	.LBE89
	.4byte	0
	.4byte	0
	.4byte	.Ltext0
	.4byte	.Letext0
	.4byte	.LFB120
	.4byte	.LFE120
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF41:
	.ascii	"AHB1LPENR\000"
.LASF52:
	.ascii	"RCC_TypeDef\000"
.LASF19:
	.ascii	"OSPEEDR\000"
.LASF31:
	.ascii	"APB1RSTR\000"
.LASF35:
	.ascii	"AHB2ENR\000"
.LASF58:
	.ascii	"send_char_USART3\000"
.LASF60:
	.ascii	"print_char\000"
.LASF2:
	.ascii	"short int\000"
.LASF12:
	.ascii	"sizetype\000"
.LASF48:
	.ascii	"BDCR\000"
.LASF51:
	.ascii	"PLLI2SCFGR\000"
.LASF64:
	.ascii	"main\000"
.LASF7:
	.ascii	"__uint32_t\000"
.LASF4:
	.ascii	"__uint16_t\000"
.LASF54:
	.ascii	"USART_TypeDef\000"
.LASF27:
	.ascii	"AHB1RSTR\000"
.LASF26:
	.ascii	"CFGR\000"
.LASF43:
	.ascii	"AHB3LPENR\000"
.LASF55:
	.ascii	"sendCharUSART2\000"
.LASF38:
	.ascii	"APB1ENR\000"
.LASF66:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O2 -fsingle-precision-constant\000"
.LASF18:
	.ascii	"OTYPER\000"
.LASF0:
	.ascii	"signed char\000"
.LASF36:
	.ascii	"AHB3ENR\000"
.LASF68:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/exercise_02\000"
.LASF9:
	.ascii	"long long int\000"
.LASF20:
	.ascii	"PUPDR\000"
.LASF6:
	.ascii	"long int\000"
.LASF57:
	.ascii	"string\000"
.LASF61:
	.ascii	"initUSART2\000"
.LASF62:
	.ascii	"initUSART3\000"
.LASF22:
	.ascii	"BSRRH\000"
.LASF17:
	.ascii	"MODER\000"
.LASF32:
	.ascii	"APB2RSTR\000"
.LASF21:
	.ascii	"BSRRL\000"
.LASF53:
	.ascii	"GTPR\000"
.LASF65:
	.ascii	"sprintf\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF39:
	.ascii	"APB2ENR\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF16:
	.ascii	"uint32_t\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF28:
	.ascii	"AHB2RSTR\000"
.LASF14:
	.ascii	"uint16_t\000"
.LASF70:
	.ascii	"printFrequencyConfiguration\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF25:
	.ascii	"PLLCFGR\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF45:
	.ascii	"APB1LPENR\000"
.LASF13:
	.ascii	"char\000"
.LASF15:
	.ascii	"int32_t\000"
.LASF63:
	.ascii	"print_string\000"
.LASF42:
	.ascii	"AHB2LPENR\000"
.LASF50:
	.ascii	"SSCGR\000"
.LASF30:
	.ascii	"RESERVED0\000"
.LASF33:
	.ascii	"RESERVED1\000"
.LASF37:
	.ascii	"RESERVED2\000"
.LASF40:
	.ascii	"RESERVED3\000"
.LASF44:
	.ascii	"RESERVED4\000"
.LASF47:
	.ascii	"RESERVED5\000"
.LASF49:
	.ascii	"RESERVED6\000"
.LASF56:
	.ascii	"sendStringUSART2\000"
.LASF34:
	.ascii	"AHB1ENR\000"
.LASF5:
	.ascii	"__int32_t\000"
.LASF24:
	.ascii	"GPIO_TypeDef\000"
.LASF29:
	.ascii	"AHB3RSTR\000"
.LASF67:
	.ascii	"main.c\000"
.LASF71:
	.ascii	"ITM_RxBuffer\000"
.LASF59:
	.ascii	"print_string_USART3\000"
.LASF23:
	.ascii	"LCKR\000"
.LASF46:
	.ascii	"APB2LPENR\000"
.LASF69:
	.ascii	"getAPB1Freq\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
