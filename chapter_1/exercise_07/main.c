#include "stm32f4xx.h"
#include "print.h"

#define IDLE_STATE 0x1
#define PRESSED_STATE 0x2
#define DEBOUNCE_STATE 0x3

uint32_t user_button_count;
uint8_t user_button_state = IDLE_STATE;



void init_user_button()
{

    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; // enable clock for GPIOA port
    GPIOA->MODER &= 0xFFFFFFFC; // set PA0 pin value to 00b which corresponde


    // (reference manual pg.66)
    // SYSCFG is located on APB2 bus

    // (reference manual pg.286)
    // SYSCFG is used to manage the external interrupt line
    // connection to the GPIOs
 
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN; 

    
    // (reference manula pg.289)
    // Each SYSCFG_EXTICRx register is 16bits and each is splitted
    // in 4-bits tuple, which represent GPIO port from which x pin is taken
    // as interrupt. So, for example, if EXTI10[3:0] is 0000b then PA[10] is
    // connected to EXTI10 interrupt line, if EXTI10[3:0] is 0111 then PH[10] 
    // is connected to EXTI10 interrupt line.

    SYSCFG->EXTICR[0] &= ~SYSCFG_EXTICR1_EXTI0; // clear EXTI0 configuration to 0
    SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI0_PA; // select GPIO port A to EXTI0 line
                                                  // effectively PA0 is mapped to EXTI0


    // (reference manual pg.381) 
    EXTI->IMR |= 0x1; // set bit 0 to value '1' which effectively
                      // unmask interrupt requests from EXTI0 line

    // (reference manual pg.381)
    EXTI->EMR &= 0xFFFFFFFE;// reset bit 0 to value '0' which effectively
                            // mask event requests from EXTI0 line
                            // 'event mode' is used in WFE for low power feature

    
    // (reference manual pg.382)
    EXTI->RTSR |= 0x1; // enabling rising trigger for EXTI0
                       // bit 0 of RTST is set to value '1'

    // (reference manual pg.382)
    EXTI->FTSR &= 0xFFFFFFFE; // disabling falling trigger for EXTI0
                              // bit 0 of FTSR is set to value '0'

    // (function 'NVIC_EnableIRQ' defined in 'core_cm4.h')
    // (const/enum EXTI0_IRQn defined in 'stm32f4xx.h')
    NVIC_EnableIRQ(EXTI0_IRQn);

}


                
    // (function 'EXTI0_IRQHandler' defined in file 'startup_stm32f4xxx.s' as WEAK)

void EXTI0_IRQHandler()
{
    if(EXTI->PR & 0x1)
    {
        if(user_button_state == IDLE_STATE)
        {
            user_button_count++;
            user_button_state = PRESSED_STATE; 
        }
    
        EXTI->PR |= 0x1; 
    }    
}


uint8_t is_timer_started;
uint32_t timer;


void user_input_state_machine()
{
    switch(user_button_state)
    {
        case IDLE_STATE:
            break;

        case PRESSED_STATE:
            if((GPIOA->IDR & 0x1) == 0x0)
            {
                user_button_state = DEBOUNCE_STATE;
                is_timer_started = 0x1; 
                timer = 0u; 
            }
            break; 

        case DEBOUNCE_STATE:
            if(timer > 1000000u)
            { 
                user_button_state = IDLE_STATE;
                is_timer_started = 0x0;
            }
            break;
    }
}




int main(int argc, const char *argv[])
{
    volatile uint32_t ubc_old_value;

    init_UART4();
    init_user_button();
  
    ubc_old_value = user_button_count;


    print_terminal("example7 started\r\n");

    while(1)
    {
       if(ubc_old_value != user_button_count)
           print_terminal("--->user_button_cout %d\r\n", user_button_count);
        ubc_old_value = user_button_count;
        //print_terminal("user_button_count %d\r\n", user_button_count); 

        user_input_state_machine();
  
        if(is_timer_started)
            ++timer;
    }

    return 0;
}
