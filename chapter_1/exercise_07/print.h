#ifndef PRINT_H
#define PRINT_H

#include "stm32f4xx.h"
#include <stdarg.h>


// UART4
// TX - PC10
// RX - PC11

void init_UART4();

void print_char(char);
void print_integer(int val);
void print_hex(int val);
void print_string(const char *str);
void print_terminal(const char *fmt, ...);

#endif
