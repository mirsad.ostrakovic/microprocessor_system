	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"main.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.comm	timer1_value,4,4
	.comm	timer2_value,4,4
	.comm	timer3_value,4,4
	.global	pc15_delay
	.data
	.align	2
	.type	pc15_delay, %object
	.size	pc15_delay, 4
pc15_delay:
	.word	1000000
	.text
	.align	2
	.global	restart_timer1
	.thumb
	.thumb_func
	.type	restart_timer1, %function
restart_timer1:
.LFB110:
	.file 1 "main.c"
	.loc 1 10 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 11 0
	ldr	r3, .L2
	movs	r2, #0
	str	r2, [r3]
	.loc 1 12 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L3:
	.align	2
.L2:
	.word	timer1_value
	.cfi_endproc
.LFE110:
	.size	restart_timer1, .-restart_timer1
	.align	2
	.global	restart_timer2
	.thumb
	.thumb_func
	.type	restart_timer2, %function
restart_timer2:
.LFB111:
	.loc 1 15 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 16 0
	ldr	r3, .L5
	movs	r2, #0
	str	r2, [r3]
	.loc 1 17 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L6:
	.align	2
.L5:
	.word	timer2_value
	.cfi_endproc
.LFE111:
	.size	restart_timer2, .-restart_timer2
	.align	2
	.global	restart_timer3
	.thumb
	.thumb_func
	.type	restart_timer3, %function
restart_timer3:
.LFB112:
	.loc 1 20 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 21 0
	ldr	r3, .L8
	movs	r2, #0
	str	r2, [r3]
	.loc 1 22 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L9:
	.align	2
.L8:
	.word	timer3_value
	.cfi_endproc
.LFE112:
	.size	restart_timer3, .-restart_timer3
	.align	2
	.global	increment_timer1
	.thumb
	.thumb_func
	.type	increment_timer1, %function
increment_timer1:
.LFB113:
	.loc 1 29 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 30 0
	ldr	r3, .L11
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L11
	str	r3, [r2]
	.loc 1 31 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L12:
	.align	2
.L11:
	.word	timer1_value
	.cfi_endproc
.LFE113:
	.size	increment_timer1, .-increment_timer1
	.align	2
	.global	increment_timer2
	.thumb
	.thumb_func
	.type	increment_timer2, %function
increment_timer2:
.LFB114:
	.loc 1 35 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 36 0
	ldr	r3, .L14
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L14
	str	r3, [r2]
	.loc 1 37 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L15:
	.align	2
.L14:
	.word	timer2_value
	.cfi_endproc
.LFE114:
	.size	increment_timer2, .-increment_timer2
	.align	2
	.global	increment_timer3
	.thumb
	.thumb_func
	.type	increment_timer3, %function
increment_timer3:
.LFB115:
	.loc 1 41 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 42 0
	ldr	r3, .L17
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L17
	str	r3, [r2]
	.loc 1 43 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L18:
	.align	2
.L17:
	.word	timer3_value
	.cfi_endproc
.LFE115:
	.size	increment_timer3, .-increment_timer3
	.align	2
	.global	get_timer1_value
	.thumb
	.thumb_func
	.type	get_timer1_value, %function
get_timer1_value:
.LFB116:
	.loc 1 50 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 51 0
	ldr	r3, .L21
	ldr	r3, [r3]
	.loc 1 52 0
	mov	r0, r3
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L22:
	.align	2
.L21:
	.word	timer1_value
	.cfi_endproc
.LFE116:
	.size	get_timer1_value, .-get_timer1_value
	.align	2
	.global	get_timer2_value
	.thumb
	.thumb_func
	.type	get_timer2_value, %function
get_timer2_value:
.LFB117:
	.loc 1 55 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 56 0
	ldr	r3, .L25
	ldr	r3, [r3]
	.loc 1 57 0
	mov	r0, r3
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L26:
	.align	2
.L25:
	.word	timer2_value
	.cfi_endproc
.LFE117:
	.size	get_timer2_value, .-get_timer2_value
	.align	2
	.global	get_timer3_value
	.thumb
	.thumb_func
	.type	get_timer3_value, %function
get_timer3_value:
.LFB118:
	.loc 1 61 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 62 0
	ldr	r3, .L29
	ldr	r3, [r3]
	.loc 1 63 0
	mov	r0, r3
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L30:
	.align	2
.L29:
	.word	timer3_value
	.cfi_endproc
.LFE118:
	.size	get_timer3_value, .-get_timer3_value
	.align	2
	.global	init_UART4
	.thumb
	.thumb_func
	.type	init_UART4, %function
init_UART4:
.LFB119:
	.loc 1 69 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 72 0
	ldr	r2, .L32
	ldr	r3, .L32
	ldr	r3, [r3, #48]
	orr	r3, r3, #4
	str	r3, [r2, #48]
	.loc 1 75 0
	ldr	r2, .L32
	ldr	r3, .L32
	ldr	r3, [r3, #64]
	orr	r3, r3, #524288
	str	r3, [r2, #64]
	.loc 1 78 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3]
	bic	r3, r3, #15728640
	str	r3, [r2]
	.loc 1 79 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3]
	orr	r3, r3, #10485760
	str	r3, [r2]
	.loc 1 81 0
	ldr	r1, .L32+4
	ldr	r3, .L32+4
	ldr	r2, [r3, #4]
	movw	r3, #62463
	ands	r3, r3, r2
	str	r3, [r1, #4]
	.loc 1 83 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3, #8]
	bic	r3, r3, #15728640
	str	r3, [r2, #8]
	.loc 1 84 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3, #8]
	orr	r3, r3, #10485760
	str	r3, [r2, #8]
	.loc 1 86 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #15728640
	str	r3, [r2, #12]
	.loc 1 87 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3, #12]
	orr	r3, r3, #5242880
	str	r3, [r2, #12]
	.loc 1 89 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3, #36]
	bic	r3, r3, #65280
	str	r3, [r2, #36]
	.loc 1 90 0
	ldr	r2, .L32+4
	ldr	r3, .L32+4
	ldr	r3, [r3, #36]
	orr	r3, r3, #34816
	str	r3, [r2, #36]
	.loc 1 94 0
	ldr	r2, .L32+8
	ldr	r3, .L32+8
	ldrh	r3, [r3, #12]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #8192
	orr	r3, r3, #8
	uxth	r3, r3
	strh	r3, [r2, #12]	@ movhi
	.loc 1 99 0
	ldr	r3, .L32+8
	mov	r2, #364
	strh	r2, [r3, #8]	@ movhi
	.loc 1 101 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L33:
	.align	2
.L32:
	.word	1073887232
	.word	1073874944
	.word	1073761280
	.cfi_endproc
.LFE119:
	.size	init_UART4, .-init_UART4
	.align	2
	.global	print_char
	.thumb
	.thumb_func
	.type	print_char, %function
print_char:
.LFB120:
	.loc 1 104 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	strb	r3, [r7, #7]
	.loc 1 105 0
	nop
.L35:
	.loc 1 105 0 is_stmt 0 discriminator 1
	ldr	r3, .L36
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L35
	.loc 1 106 0 is_stmt 1
	ldr	r3, .L36
	ldrb	r2, [r7, #7]	@ zero_extendqisi2
	uxth	r2, r2
	strh	r2, [r3, #4]	@ movhi
	.loc 1 107 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L37:
	.align	2
.L36:
	.word	1073761280
	.cfi_endproc
.LFE120:
	.size	print_char, .-print_char
	.align	2
	.global	print_string
	.thumb
	.thumb_func
	.type	print_string, %function
print_string:
.LFB121:
	.loc 1 111 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 112 0
	b	.L39
.L40:
	.loc 1 113 0
	ldr	r3, [r7, #4]
	adds	r2, r3, #1
	str	r2, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
.L39:
	.loc 1 112 0
	ldr	r3, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L40
	.loc 1 114 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE121:
	.size	print_string, .-print_string
	.align	2
	.global	print_number
	.thumb
	.thumb_func
	.type	print_number, %function
print_number:
.LFB122:
	.loc 1 117 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #48
	.cfi_def_cfa_offset 56
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 119 0
	movs	r3, #0
	strb	r3, [r7, #47]
	.loc 1 120 0
	ldr	r3, [r7, #4]
	lsrs	r3, r3, #31
	uxtb	r3, r3
	strb	r3, [r7, #46]
	.loc 1 122 0
	ldr	r3, [r7, #4]
	cmp	r3, #0
	bne	.L42
	.loc 1 124 0
	movs	r0, #48
	bl	print_char
.L42:
	.loc 1 127 0
	b	.L43
.L44:
	.loc 1 129 0
	ldrb	r2, [r7, #47]	@ zero_extendqisi2
	uxtb	r3, r2
	adds	r3, r3, #1
	uxtb	r3, r3
	strb	r3, [r7, #47]
	sxtb	r0, r2
	ldr	r2, [r7, #4]
	ldr	r3, .L47
	smull	r1, r3, r3, r2
	asrs	r1, r3, #2
	asrs	r3, r2, #31
	subs	r1, r1, r3
	mov	r3, r1
	lsls	r3, r3, #2
	add	r3, r3, r1
	lsls	r3, r3, #1
	subs	r1, r2, r3
	uxtb	r3, r1
	adds	r3, r3, #48
	uxtb	r2, r3
	add	r3, r7, #48
	add	r3, r3, r0
	strb	r2, [r3, #-36]
	.loc 1 130 0
	ldr	r3, [r7, #4]
	ldr	r2, .L47
	smull	r1, r2, r2, r3
	asrs	r2, r2, #2
	asrs	r3, r3, #31
	subs	r3, r2, r3
	str	r3, [r7, #4]
.L43:
	.loc 1 127 0
	ldr	r3, [r7, #4]
	cmp	r3, #0
	bne	.L44
	.loc 1 133 0
	b	.L45
.L46:
	.loc 1 135 0
	ldrb	r3, [r7, #47]	@ zero_extendqisi2
	subs	r3, r3, #1
	uxtb	r3, r3
	strb	r3, [r7, #47]
	ldrsb	r3, [r7, #47]
	add	r2, r7, #48
	add	r3, r3, r2
	ldrb	r3, [r3, #-36]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
.L45:
	.loc 1 133 0
	ldrsb	r3, [r7, #47]
	cmp	r3, #-1
	bne	.L46
	.loc 1 138 0
	mov	r0, r3
	adds	r7, r7, #48
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L48:
	.align	2
.L47:
	.word	1717986919
	.cfi_endproc
.LFE122:
	.size	print_number, .-print_number
	.section	.rodata
	.align	2
.LC0:
	.ascii	"button clicked\015\012\000"
	.align	2
.LC1:
	.ascii	"new delay: \000"
	.align	2
.LC2:
	.ascii	"\015\012\000"
	.text
	.align	2
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
.LFB123:
	.loc 1 142 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r7, lr}
	.cfi_def_cfa_offset 12
	.cfi_offset 4, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #28
	.cfi_def_cfa_offset 40
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	str	r1, [r7]
	.loc 1 144 0
	bl	init_UART4
	.loc 1 146 0
	mov	r3, #1000
	str	r3, [r7, #12]
	.loc 1 147 0
	movs	r3, #10
	str	r3, [r7, #20]
	.loc 1 148 0
	movs	r3, #255
	strb	r3, [r7, #19]
	.loc 1 149 0
	movs	r3, #1
	strb	r3, [r7, #18]
	.loc 1 150 0
	movs	r3, #0
	strb	r3, [r7, #17]
	.loc 1 152 0
	ldr	r2, .L62
	ldr	r3, .L62
	ldr	r3, [r3, #48]
	orr	r3, r3, #9
	str	r3, [r2, #48]
	.loc 1 159 0
	ldr	r2, .L62
	ldr	r3, .L62
	ldr	r3, [r3, #48]
	orr	r3, r3, #4
	str	r3, [r2, #48]
	.loc 1 163 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3]
	bic	r3, r3, #-1073741824
	str	r3, [r2]
	.loc 1 164 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3]
	orr	r3, r3, #1073741824
	str	r3, [r2]
	.loc 1 166 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3, #4]
	bic	r3, r3, #-1073741824
	str	r3, [r2, #4]
	.loc 1 168 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3, #8]
	bic	r3, r3, #-1073741824
	str	r3, [r2, #8]
	.loc 1 169 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3, #8]
	orr	r3, r3, #-2147483648
	str	r3, [r2, #8]
	.loc 1 171 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #-1073741824
	str	r3, [r2, #12]
	.loc 1 177 0
	ldr	r2, .L62+8
	ldr	r3, .L62+8
	ldr	r3, [r3]
	bic	r3, r3, #-1073741824
	str	r3, [r2]
	.loc 1 178 0
	ldr	r2, .L62+8
	ldr	r3, .L62+8
	ldr	r3, [r3]
	orr	r3, r3, #1073741824
	str	r3, [r2]
	.loc 1 180 0
	ldr	r2, .L62+8
	ldr	r3, .L62+8
	ldr	r3, [r3, #4]
	bic	r3, r3, #-1073741824
	str	r3, [r2, #4]
	.loc 1 182 0
	ldr	r2, .L62+8
	ldr	r3, .L62+8
	ldr	r3, [r3, #8]
	bic	r3, r3, #-1073741824
	str	r3, [r2, #8]
	.loc 1 183 0
	ldr	r2, .L62+8
	ldr	r3, .L62+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #-2147483648
	str	r3, [r2, #8]
	.loc 1 185 0
	ldr	r2, .L62+8
	ldr	r3, .L62+8
	ldr	r3, [r3, #12]
	bic	r3, r3, #-1073741824
	str	r3, [r2, #12]
	.loc 1 189 0
	ldr	r2, .L62+12
	ldr	r3, .L62+12
	ldr	r3, [r3]
	bic	r3, r3, #3
	str	r3, [r2]
	.loc 1 191 0
	ldr	r2, .L62+12
	ldr	r3, .L62+12
	ldr	r3, [r3, #12]
	bic	r3, r3, #3
	str	r3, [r2, #12]
	.loc 1 196 0
	bl	restart_timer1
	.loc 1 197 0
	bl	restart_timer3
.L61:
	.loc 1 201 0
	ldrb	r3, [r7, #18]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L50
	.loc 1 203 0
	ldr	r3, .L62+12
	ldr	r3, [r3, #16]
	and	r3, r3, #1
	cmp	r3, #0
	beq	.L51
	.loc 1 205 0
	movs	r3, #0
	strb	r3, [r7, #18]
	.loc 1 206 0
	bl	restart_timer2
	.loc 1 208 0
	movs	r3, #1
	strb	r3, [r7, #17]
	b	.L57
.L51:
	.loc 1 212 0
	ldrb	r3, [r7, #17]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L53
	.loc 1 214 0
	ldr	r0, .L62+16
	bl	print_string
	.loc 1 216 0
	ldr	r3, [r7, #20]
	subs	r3, r3, #2
	str	r3, [r7, #20]
	.loc 1 218 0
	ldr	r3, [r7, #20]
	cmp	r3, #0
	bgt	.L54
	.loc 1 220 0
	movs	r3, #10
	str	r3, [r7, #20]
.L54:
	.loc 1 224 0
	ldr	r3, [r7, #12]
	cmp	r3, #500
	bcc	.L55
	.loc 1 224 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #12]
	movw	r2, #1500
	cmp	r3, r2
	bls	.L56
.L55:
	.loc 1 225 0 is_stmt 1
	ldrb	r3, [r7, #19]	@ zero_extendqisi2
	negs	r3, r3
	uxtb	r3, r3
	strb	r3, [r7, #19]
.L56:
	.loc 1 227 0
	ldr	r0, .L62+20
	bl	print_string
	.loc 1 228 0
	ldr	r0, [r7, #20]
	bl	print_number
	.loc 1 229 0
	ldr	r0, .L62+24
	bl	print_string
.L53:
	.loc 1 232 0
	movs	r3, #0
	strb	r3, [r7, #17]
	b	.L57
.L50:
	.loc 1 237 0
	bl	get_timer2_value
	mov	r2, r0
	ldr	r3, .L62+28
	cmp	r2, r3
	bls	.L58
	.loc 1 238 0
	movs	r3, #1
	strb	r3, [r7, #18]
.L58:
	.loc 1 240 0
	bl	increment_timer2
.L57:
	.loc 1 244 0
	ldr	r3, [r7, #12]
	mov	r2, #1000
	mul	r4, r2, r3
	bl	get_timer1_value
	mov	r3, r0
	cmp	r4, r3
	bcs	.L59
	.loc 1 246 0
	bl	restart_timer1
.L59:
	.loc 1 250 0
	bl	increment_timer1
	.loc 1 253 0
	bl	get_timer3_value
	mov	r2, r0
	ldr	r3, .L62+32
	ldr	r3, [r3]
	cmp	r2, r3
	bls	.L60
	.loc 1 256 0
	bl	restart_timer3
	.loc 1 257 0
	ldr	r2, .L62+4
	ldr	r3, .L62+4
	ldr	r3, [r3, #20]
	eor	r3, r3, #32768
	str	r3, [r2, #20]
.L60:
	.loc 1 262 0
	bl	increment_timer3
	.loc 1 265 0
	b	.L61
.L63:
	.align	2
.L62:
	.word	1073887232
	.word	1073874944
	.word	1073875968
	.word	1073872896
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	100000
	.word	pc15_delay
	.cfi_endproc
.LFE123:
	.size	main, .-main
.Letext0:
	.file 2 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "../../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x640
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF88
	.byte	0x1
	.4byte	.LASF89
	.4byte	.LASF90
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x2
	.byte	0x1b
	.4byte	0x30
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x2
	.byte	0x1d
	.4byte	0x42
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x2
	.byte	0x2b
	.4byte	0x5b
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x2
	.byte	0x3f
	.4byte	0x6d
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x2
	.byte	0x41
	.4byte	0x7f
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x3
	.byte	0x14
	.4byte	0x25
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x3
	.byte	0x15
	.4byte	0x37
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x3
	.byte	0x21
	.4byte	0x50
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x3
	.byte	0x2c
	.4byte	0x62
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x3
	.byte	0x2d
	.4byte	0x74
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF19
	.uleb128 0x5
	.4byte	0xce
	.uleb128 0x6
	.4byte	0xce
	.4byte	0xf5
	.uleb128 0x7
	.4byte	0xd9
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0xb8
	.uleb128 0x5
	.4byte	0xc3
	.uleb128 0x8
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x18b
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x290
	.4byte	0xe0
	.byte	0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x291
	.4byte	0xe0
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x292
	.4byte	0xe0
	.byte	0x8
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x293
	.4byte	0xe0
	.byte	0xc
	.uleb128 0xa
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xe0
	.byte	0x10
	.uleb128 0xa
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xe0
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x296
	.4byte	0xf5
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x297
	.4byte	0xf5
	.byte	0x1a
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x298
	.4byte	0xe0
	.byte	0x1c
	.uleb128 0xa
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x18b
	.byte	0x20
	.byte	0
	.uleb128 0x5
	.4byte	0xe5
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x29a
	.4byte	0xff
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x32b
	.uleb128 0xa
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xe0
	.byte	0
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xe0
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xe0
	.byte	0x8
	.uleb128 0xa
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xe0
	.byte	0xc
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xe0
	.byte	0x10
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xe0
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xe0
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xce
	.byte	0x1c
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xe0
	.byte	0x20
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xe0
	.byte	0x24
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xe5
	.byte	0x28
	.uleb128 0x9
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xe0
	.byte	0x30
	.uleb128 0x9
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xe0
	.byte	0x34
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xe0
	.byte	0x38
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xce
	.byte	0x3c
	.uleb128 0x9
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xe0
	.byte	0x40
	.uleb128 0x9
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xe0
	.byte	0x44
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xe5
	.byte	0x48
	.uleb128 0x9
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xe0
	.byte	0x50
	.uleb128 0x9
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xe0
	.byte	0x54
	.uleb128 0x9
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xe0
	.byte	0x58
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xce
	.byte	0x5c
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xe0
	.byte	0x60
	.uleb128 0x9
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xe0
	.byte	0x64
	.uleb128 0x9
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xe5
	.byte	0x68
	.uleb128 0x9
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xe0
	.byte	0x70
	.uleb128 0xa
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xe0
	.byte	0x74
	.uleb128 0x9
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xe5
	.byte	0x78
	.uleb128 0x9
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xe0
	.byte	0x80
	.uleb128 0x9
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xe0
	.byte	0x84
	.byte	0
	.uleb128 0xb
	.4byte	.LASF55
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x19c
	.uleb128 0x8
	.byte	0x1c
	.byte	0x4
	.2byte	0x395
	.4byte	0x3f5
	.uleb128 0xa
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x397
	.4byte	0xf5
	.byte	0
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x398
	.4byte	0xb8
	.byte	0x2
	.uleb128 0xa
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x399
	.4byte	0xf5
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x39a
	.4byte	0xb8
	.byte	0x6
	.uleb128 0xa
	.ascii	"BRR\000"
	.byte	0x4
	.2byte	0x39b
	.4byte	0xf5
	.byte	0x8
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x39c
	.4byte	0xb8
	.byte	0xa
	.uleb128 0xa
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x39d
	.4byte	0xf5
	.byte	0xc
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x39e
	.4byte	0xb8
	.byte	0xe
	.uleb128 0xa
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x39f
	.4byte	0xf5
	.byte	0x10
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x3a0
	.4byte	0xb8
	.byte	0x12
	.uleb128 0xa
	.ascii	"CR3\000"
	.byte	0x4
	.2byte	0x3a1
	.4byte	0xf5
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x3a2
	.4byte	0xb8
	.byte	0x16
	.uleb128 0x9
	.4byte	.LASF56
	.byte	0x4
	.2byte	0x3a3
	.4byte	0xf5
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x3a4
	.4byte	0xb8
	.byte	0x1a
	.byte	0
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x3a5
	.4byte	0x337
	.uleb128 0xc
	.4byte	.LASF58
	.byte	0x1
	.byte	0x9
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF59
	.byte	0x1
	.byte	0xe
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF60
	.byte	0x1
	.byte	0x13
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF61
	.byte	0x1
	.byte	0x1c
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF62
	.byte	0x1
	.byte	0x22
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF63
	.byte	0x1
	.byte	0x28
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xd
	.4byte	.LASF64
	.byte	0x1
	.byte	0x31
	.4byte	0xce
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xd
	.4byte	.LASF65
	.byte	0x1
	.byte	0x36
	.4byte	0xce
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xd
	.4byte	.LASF66
	.byte	0x1
	.byte	0x3c
	.4byte	0xce
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF67
	.byte	0x1
	.byte	0x44
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xe
	.4byte	.LASF69
	.byte	0x1
	.byte	0x67
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x4d9
	.uleb128 0xf
	.ascii	"c\000"
	.byte	0x1
	.byte	0x67
	.4byte	0x4d9
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.byte	0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF68
	.uleb128 0x10
	.4byte	.LASF70
	.byte	0x1
	.byte	0x6e
	.4byte	.LFB121
	.4byte	.LFE121-.LFB121
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x504
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x1
	.byte	0x6e
	.4byte	0x504
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.byte	0x4
	.4byte	0x50a
	.uleb128 0x13
	.4byte	0x4d9
	.uleb128 0x14
	.4byte	.LASF74
	.byte	0x1
	.byte	0x74
	.4byte	0x94
	.4byte	.LFB122
	.4byte	.LFE122-.LFB122
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x561
	.uleb128 0xf
	.ascii	"val\000"
	.byte	0x1
	.byte	0x74
	.4byte	0x94
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x15
	.4byte	.LASF72
	.byte	0x1
	.byte	0x76
	.4byte	0x561
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x16
	.ascii	"idx\000"
	.byte	0x1
	.byte	0x77
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.uleb128 0x15
	.4byte	.LASF73
	.byte	0x1
	.byte	0x78
	.4byte	0xad
	.uleb128 0x2
	.byte	0x91
	.sleb128 -10
	.byte	0
	.uleb128 0x6
	.4byte	0x4d9
	.4byte	0x571
	.uleb128 0x7
	.4byte	0xd9
	.byte	0x1f
	.byte	0
	.uleb128 0x14
	.4byte	.LASF75
	.byte	0x1
	.byte	0x8d
	.4byte	0x94
	.4byte	.LFB123
	.4byte	.LFE123-.LFB123
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x5ed
	.uleb128 0x11
	.4byte	.LASF76
	.byte	0x1
	.byte	0x8d
	.4byte	0x94
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x11
	.4byte	.LASF77
	.byte	0x1
	.byte	0x8d
	.4byte	0x5ed
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x15
	.4byte	.LASF78
	.byte	0x1
	.byte	0x92
	.4byte	0xce
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x15
	.4byte	.LASF79
	.byte	0x1
	.byte	0x93
	.4byte	0xc3
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF80
	.byte	0x1
	.byte	0x94
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -21
	.uleb128 0x15
	.4byte	.LASF81
	.byte	0x1
	.byte	0x95
	.4byte	0xad
	.uleb128 0x2
	.byte	0x91
	.sleb128 -22
	.uleb128 0x15
	.4byte	.LASF82
	.byte	0x1
	.byte	0x96
	.4byte	0xad
	.uleb128 0x2
	.byte	0x91
	.sleb128 -23
	.byte	0
	.uleb128 0x12
	.byte	0x4
	.4byte	0x504
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x5
	.2byte	0x51b
	.4byte	0xfa
	.uleb128 0x18
	.4byte	.LASF84
	.byte	0x1
	.byte	0x4
	.4byte	0xce
	.uleb128 0x5
	.byte	0x3
	.4byte	timer1_value
	.uleb128 0x18
	.4byte	.LASF85
	.byte	0x1
	.byte	0x5
	.4byte	0xce
	.uleb128 0x5
	.byte	0x3
	.4byte	timer2_value
	.uleb128 0x18
	.4byte	.LASF86
	.byte	0x1
	.byte	0x6
	.4byte	0xce
	.uleb128 0x5
	.byte	0x3
	.4byte	timer3_value
	.uleb128 0x18
	.4byte	.LASF87
	.byte	0x1
	.byte	0x7
	.4byte	0xce
	.uleb128 0x5
	.byte	0x3
	.4byte	pc15_delay
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF44:
	.ascii	"AHB1LPENR\000"
.LASF81:
	.ascii	"is_elapsed_time\000"
.LASF67:
	.ascii	"init_UART4\000"
.LASF56:
	.ascii	"GTPR\000"
.LASF34:
	.ascii	"APB1RSTR\000"
.LASF38:
	.ascii	"AHB2ENR\000"
.LASF69:
	.ascii	"print_char\000"
.LASF14:
	.ascii	"int8_t\000"
.LASF68:
	.ascii	"char\000"
.LASF64:
	.ascii	"get_timer1_value\000"
.LASF19:
	.ascii	"sizetype\000"
.LASF51:
	.ascii	"BDCR\000"
.LASF54:
	.ascii	"PLLI2SCFGR\000"
.LASF80:
	.ascii	"direction\000"
.LASF11:
	.ascii	"long long int\000"
.LASF9:
	.ascii	"__uint32_t\000"
.LASF72:
	.ascii	"buff\000"
.LASF61:
	.ascii	"increment_timer1\000"
.LASF62:
	.ascii	"increment_timer2\000"
.LASF63:
	.ascii	"increment_timer3\000"
.LASF5:
	.ascii	"__uint16_t\000"
.LASF53:
	.ascii	"SSCGR\000"
.LASF4:
	.ascii	"short int\000"
.LASF46:
	.ascii	"AHB3LPENR\000"
.LASF29:
	.ascii	"CFGR\000"
.LASF36:
	.ascii	"RESERVED1\000"
.LASF43:
	.ascii	"RESERVED3\000"
.LASF15:
	.ascii	"uint8_t\000"
.LASF47:
	.ascii	"RESERVED4\000"
.LASF41:
	.ascii	"APB1ENR\000"
.LASF57:
	.ascii	"USART_TypeDef\000"
.LASF26:
	.ascii	"LCKR\000"
.LASF21:
	.ascii	"OTYPER\000"
.LASF84:
	.ascii	"timer1_value\000"
.LASF76:
	.ascii	"argc\000"
.LASF2:
	.ascii	"signed char\000"
.LASF78:
	.ascii	"delay\000"
.LASF39:
	.ascii	"AHB3ENR\000"
.LASF73:
	.ascii	"minus_flag\000"
.LASF23:
	.ascii	"PUPDR\000"
.LASF8:
	.ascii	"long int\000"
.LASF86:
	.ascii	"timer3_value\000"
.LASF71:
	.ascii	"string\000"
.LASF55:
	.ascii	"RCC_TypeDef\000"
.LASF25:
	.ascii	"BSRRH\000"
.LASF20:
	.ascii	"MODER\000"
.LASF35:
	.ascii	"APB2RSTR\000"
.LASF1:
	.ascii	"__uint8_t\000"
.LASF0:
	.ascii	"__int8_t\000"
.LASF24:
	.ascii	"BSRRL\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF42:
	.ascii	"APB2ENR\000"
.LASF12:
	.ascii	"long long unsigned int\000"
.LASF18:
	.ascii	"uint32_t\000"
.LASF13:
	.ascii	"unsigned int\000"
.LASF31:
	.ascii	"AHB2RSTR\000"
.LASF66:
	.ascii	"get_timer3_value\000"
.LASF10:
	.ascii	"long unsigned int\000"
.LASF30:
	.ascii	"AHB1RSTR\000"
.LASF77:
	.ascii	"argv\000"
.LASF28:
	.ascii	"PLLCFGR\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF48:
	.ascii	"APB1LPENR\000"
.LASF65:
	.ascii	"get_timer2_value\000"
.LASF16:
	.ascii	"uint16_t\000"
.LASF75:
	.ascii	"main\000"
.LASF17:
	.ascii	"int32_t\000"
.LASF88:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF70:
	.ascii	"print_string\000"
.LASF45:
	.ascii	"AHB2LPENR\000"
.LASF33:
	.ascii	"RESERVED0\000"
.LASF74:
	.ascii	"print_number\000"
.LASF40:
	.ascii	"RESERVED2\000"
.LASF22:
	.ascii	"OSPEEDR\000"
.LASF89:
	.ascii	"main.c\000"
.LASF50:
	.ascii	"RESERVED5\000"
.LASF52:
	.ascii	"RESERVED6\000"
.LASF37:
	.ascii	"AHB1ENR\000"
.LASF7:
	.ascii	"__int32_t\000"
.LASF85:
	.ascii	"timer2_value\000"
.LASF87:
	.ascii	"pc15_delay\000"
.LASF58:
	.ascii	"restart_timer1\000"
.LASF59:
	.ascii	"restart_timer2\000"
.LASF60:
	.ascii	"restart_timer3\000"
.LASF90:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/chapter_1/provjera\000"
.LASF27:
	.ascii	"GPIO_TypeDef\000"
.LASF32:
	.ascii	"AHB3RSTR\000"
.LASF83:
	.ascii	"ITM_RxBuffer\000"
.LASF82:
	.ascii	"old_input_val\000"
.LASF79:
	.ascii	"delay_mul\000"
.LASF49:
	.ascii	"APB2LPENR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
