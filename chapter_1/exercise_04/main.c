#include "stm32f4xx.h"
#include <stdarg.h>

void print_char(char);


void print_integer(int val)
{
    char buffer[32];
    int idx = -1;
    int tmp;

    if(val == 0) // special case
    {
        print_char('0');
        return;
    }

    if(val < 0)
    {
        buffer[++idx] = '-';
        val = -val;
    }

    tmp = val;

    while(tmp)
    {
        tmp /= 10;
        idx++;
    }

    buffer[idx+1] = '\0';
    
    while(val)
    {
        buffer[idx--] = '0' + val % 10; 
        val /= 10;
    }

    print_string(buffer);

}

void print_hex(int val)
{
    static const char hex_map[] = "0123456789ABCDEF";
    int idx = sizeof(int) * 2;
    
    print_char('0');
    print_char('x');

    for(; idx >= 0 ; --idx)
        print_char(hex_map[val >> (4*idx) & 0xF]);
        
}


void print_string(const char *str)
{
    while(*str != '\0')
        print_char(*(str++));
}


void print_terminal(const char *fmt, ...)
{

    va_list ap;
    va_start(ap, fmt);

    char flag = 0x0;

    while(1)
    {
        switch(*fmt)
        {
            case '\0':
                va_end(ap);
                return;

            case '%':
                flag = 0x1;
                break;

            case 'd':
               if(flag)
               {
                   print_integer(va_arg(ap, int));
                   flag = 0x0;
               }
               else
                   print_char('d');
    
               break;

            case 'x':
               if(flag)
               {
                   print_hex(va_arg(ap, int));
                   flag = 0x0;
               }
               else
                   print_char('x');
               break;

            default:
                flag = 0x0;  
                print_char(*fmt);
        }

        ++fmt;
    }
}



// UART4
// TX - PC10
// RX - PC11
// AF8

void init_UART4()
{

    // enable GPIOC module on AHB bus
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;     

    // enable UART4 module on APB1 bus
    RCC->APB1ENR |= RCC_APB1ENR_UART4EN;


    GPIOC->MODER &= 0xFF0FFFFF; // reset pins 10 and 11 mode
    GPIOC->MODER |= 0x00A00000; // set pins 10 and 11 mode to '10' - alternate function

    GPIOC->OTYPER &= 0xF3FF; // reset pins 10 and 11 output type to '0' - push-pull

    GPIOC->OSPEEDR &= 0xFF0FFFFF; // reset pins 10 and 11 output speed
    GPIOC->OSPEEDR |= 0x00A00000; // set pins 10 and 11 mode to '10' - fast speed

    GPIOC->PUPDR &= 0xFF0FFFFF; // reset pins 10 and 11 pull-up/pull-down configuration
    GPIOC->PUPDR |= 0x00500000; // set pins 10 and 11 pu/pd value to '01' - pull-up

    GPIOC->AFR[1] &= 0xFFFF00FF; // reset alternate function value of pins 10 and 11
    GPIOC->AFR[1] |= 0x00008800; // set alternate function value of pins 10 and 11 to '8' - AF8



    UART4->CR1 |=  USART_CR1_TE | USART_CR1_UE;
    
    // for APB1 clk_freq = 42MHz and OVER8 = 0 and baud_rate = 115200 Bps,  BRR_value = 22.8125
    // BRR[3:0]  ->  DIV_fraction
    // BRR[15:4] ->  DIV_mantissa
    UART4->BRR = 0x016C;

}

void print_char(char c) 
{
    
    while((((volatile USART_TypeDef *)UART4)->SR & USART_SR_TC) != USART_SR_TC);
    UART4->DR = c;
}




void delay_ms(uint16_t ms)
{
    volatile int i = 1000000;
    while(i--);
}


void print_clock_stat()
{

    int AHB_divider, APB1_divider, APB2_divider;
  
    int PLLN, PLLM, PLLP, PLLQ;


    print_terminal("system core clock: %d\r\n", SystemCoreClock);

    // CFGR - clock configuration register
    // 
    print_terminal("AHB prescaler: %d\r\n", (RCC->CFGR >> 4) & 0xF); 
    print_terminal("APB1 prescaler: %d\r\n", (RCC->CFGR >> 10) & 0x7);
    print_terminal("APB2 prescaler: %d\r\n", (RCC->CFGR >> 13) & 0x7);
    
    print_terminal("System clock: ");

    switch((RCC->CFGR >> 2) & 0x3)
    {
        case 0x0:
            print_terminal("HSI");
            break;

        case 0x1:
            print_terminal("HSE");
            break;

        case 0x2:
            print_terminal("PLL");
            break;

        case 0x3:
            print_terminal("not used");
            break;
    }

    print_terminal("\r\n");

 
    switch((RCC->CFGR >> 4) & 0xF)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            AHB_divider = 1;
            break;

        case 8:
            AHB_divider = 2;
            break;

        case 9:
            AHB_divider = 4;
            break;

        case 10:
            AHB_divider = 8;
            break;

        case 11:
            AHB_divider = 16;
            break;

        case 12:
            AHB_divider = 64;
            break;

        case 13:
            AHB_divider = 128;
            break;

        case 14:
            AHB_divider = 256;
            break;

        case 15:
            AHB_divider = 512;
            break;

    }

    print_terminal("AHB divider: %d\r\n", AHB_divider);
    print_terminal("AHB clock: %d\r\n", SystemCoreClock / AHB_divider);


    switch((RCC->CFGR >> 10) & 0x7)
    {
        case 0:
        case 1:
        case 2:
        case 3:
            APB1_divider = 1;
            break;

        case 4:
            APB1_divider = 2;
            break;

        case 5:
            APB1_divider = 4;
            break;

        case 6:
            APB1_divider = 8;

        case 7:
            APB1_divider = 16;
    }
    
    print_terminal("APB1 divider: %d\r\n", APB1_divider);
    print_terminal("APB1 clock: %d\r\n", SystemCoreClock / AHB_divider / APB1_divider);

    switch((RCC->CFGR >> 13) & 0x7)
    {
        case 0:
        case 1:
        case 2:
        case 3:
            APB2_divider = 1;       
            break;

        case 4:
            APB2_divider = 2;
            break;

        case 5:
            APB2_divider = 4;
            break;

        case 6:
            APB2_divider = 8;
            break;
        
        case 7:
            APB2_divider = 16;
            break;
    }

    print_terminal("APB2 divider: %d\r\n", APB2_divider);
    print_terminal("APB2 clock: %d\r\n",  SystemCoreClock / AHB_divider / APB2_divider);

    PLLM = RCC->PLLCFGR & 0x3F;
    PLLN = (RCC->PLLCFGR >> 6) & 0x1FF;
    PLLP = (RCC->PLLCFGR >> 16) & 0x3;
    PLLQ = (RCC->PLLCFGR >> 24) & 0xF;

    print_terminal("PLLM: %d\r\n", PLLM);
    print_terminal("PLLN: %d\r\n", PLLN);
    print_terminal("PLLP: %d\r\n", PLLP);
    print_terminal("PLLQ: %d\r\n", PLLQ);

    if((RCC->PLLCFGR >> 22) & 0x1)
    {
        print_terminal("HSE clock selected for PLL source\r\n");
        print_terminal("HSE frequenty on F407 is 8MHz\r\n"); 
    }
    else
        print_terminal("HSI oscillator clock selected as PLL source\r\n");
   
    
}



int main(int argc, const char *argv[])
{
    init_UART4();

    char c = 0x0;

    //print_clock_stat(); 

    while(1)
    {
        print_terminal("[%d]\r\n", c);
        print_terminal("[%x]\r\n", c++);
        delay_ms(100);
    }
    return 0;
}
