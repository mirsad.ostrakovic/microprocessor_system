#include "stm32f4xx.h"
#include "print.h"
#include "print_stat.h"

volatile uint32_t TIM2_ticks = 0u;

void delay_ms(uint16_t ms)
{
    volatile int i = 1000000;
    while(i--);
}




void wait_ms(uint16_t ms)
{
    if(ms == 0)
        return;

    if(ms > 5000u)
        ms = 5000u;

    RCC->APB2ENR |= RCC_APB2ENR_TIM10EN;        
    TIM10->PSC = 16800 - 1;
    TIM10->ARR = 10 * ms;
    TIM10->CR1 |= TIM_CR1_URS | TIM_CR1_ARPE;
    TIM10->EGR |= TIM_EGR_UG;
    TIM10->CR1 |= TIM_CR1_CEN;

    while((TIM10->SR & 0x1) != 0x1);
    
    TIM10->SR &= ~0x1;
    TIM10->CR1 &= ~TIM_CR1_CEN;
}







void init_SystemTimer()
{
 
    // (technical paper pg.17 - Device overview)
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // enable TIM2 peripheral on APB1 bus

    // (reference manual page.579)


    // (reference manual pg.627)
    // -1 is added because a TIMx internally add +1 on the PSC value
    TIM2->PSC = 84 - 1; // (reference manual pg.203 and pg.204)
                         // APB1 clock is 42MhZ and because in RCC_DCKCFGR
                         // TIMPRE = 0 and APB1 divider != 1 in RCC_CFGR, 
                         // timers internal clock on APB1 is 2 * APB1 clock -> 84MhZ

    // (reference manual pg.627)
    TIM2->ARR = 1000; // effective clock is 1MhZ, with ARR = 1000, 
                      // overflow will happened every 1ms <==> interrupt will happened 

    // (reference manual pg.611 and pg.612)
    // CMS value '00' -> edge-aligned mode, counter count up or down depending on the DIR bit
    // DIR value '0' -> counter counting up, '1' -> counter counting down
    // ARPE value '0' -> TIMx_ARR is not buffered , '1' -> TIMx_ARR is buffered
    // URS value '0' -> UG and counter overflow/underflow generates UI - update interrupt
    //     value '1' -> overflow/underflow generates UI only
    //     update interrupt set UIF bit in TIMx_SR register
    // CEN value '0' -> counter disabled, '1' -> counter enabled
    
    TIM2->CR1 &= ~(TIM_CR1_CMS | TIM_CR1_DIR); // reset value of CMS and DIR to 0
    TIM2->CR1 |= TIM_CR1_ARPE | TIM_CR1_URS; // set value of ARPE and URS to 1

    // (reference manual pg.620)
    // UG -> re-initialize the counter and generated an update of the registers
    //       prescaler counter is cleared too (prescaler ratio is not affected)
    //       counter is cleared od set to TIMx_ARR depending on DIR bit
    TIM2->EGR = TIM_EGR_UG;

    // (reference manuak pg.617)
    TIM2->DIER |= TIM_DIER_UIE; // UIE - update interrupt enable


    // ('stm32f4xx.h' - enum IRQn aka. IRQn_Type)
    // TIM2_IRQn is defined as 28

    // In file '../../../Libraries/CMSIS/Include/core_cm4.h'
    // are defined functions for setup NVIC
    NVIC_SetPriority(TIM2_IRQn, 0); // setup TIM2 interrupt priority to 0 - max
    NVIC_EnableIRQ(TIM2_IRQn); // enable TIM2 interrupts



    //(see description of 'init_STOPWATCH()')
    TIM2->CR1 |= TIM_CR1_CEN;
}


// In file '../../../Libraries/CMSIS/ST/STM32F4XX/Source/Templates/arm/startup_stm32f4xx.s'
// is defined the interrupt vector table and label 'TIM2_IRQHandler' which is 'weakly' linked to
// the default implementation in the same file
// {Linker will link with 'weak' link to default implementation if and only if any other implementation is not provided.}

void TIM2_IRQHandler()
{

    // this code does not work properly (ask professor why)
    //++TIM2_ticks;
    //TIM2->SR &= ~TIM_SR_UIF;    
    
    if(TIM2->SR & TIM_SR_UIF)
    {
        ++TIM2_ticks;
        TIM2->SR &= ~TIM_SR_UIF;
        //TIM2->SR = 0x0;    
    } 
}


uint32_t get_TIM2_ticks()
{
    return TIM2_ticks;
}









void init_STOPWATCH()
{
    // (technical paper pg.17 - Device overview)
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // enable TIM2 peripheral on APB1 bus

    // (reference manual page.579)


    // (reference manual pg.627)
    // -1 is added because a TIMx internally add +1 on the PSC value
    TIM2->PSC = 84 - 1; // (reference manual pg.203 and pg.204)
                         // APB1 clock is 42MhZ and because in RCC_DCKCFGR
                         // TIMPRE = 0 and APB2 divider != 1 in RCC_CFGR, 
                         // timers internal clock on APB1 is 2 * APB1 clock -> 84MhZ

    // (reference manual pg.627)
    TIM2->ARR = 0xFFFFFFFF; // effective clock is 1MhZ, with ARR = 0xFFFFFFFF, 
                            // overflow will happened every 4294,9673s > 1h

    // (reference manual pg.611 and pg.612)
    // CMS value '00' -> edge-aligned mode, counter count up or down depending on the DIR bit
    // DIR value '0' -> counter counting up, '1' -> counter counting down
    // ARPE value '0' -> TIMx_ARR is not buffered , '1' -> TIMx_ARR is buffered
    // URS value '0' -> UG and counter overflow/underflow generates UI - update interrupt
    //     value '1' -> overflow/underflow generates UI only
    //     update interrupt set UIF bit in TIMx_SR register
    // CEN value '0' -> counter disabled, '1' -> counter enabled
    
    TIM2->CR1 &= ~(TIM_CR1_CMS | TIM_CR1_DIR); // reset value of CMS and DIR to 0
    TIM2->CR1 |= TIM_CR1_ARPE | TIM_CR1_URS; // set value of ARPE and URS to 1

    // (reference manual pg.620)
    // UG -> re-initialize the counter and generated an update of the registers
    //       prescaler counter is cleared too (prescaler ratio is not affected)
    //       counter is cleared od set to TIMx_ARR depending on DIR bit
    TIM2->EGR = TIM_EGR_UG;

}


void start_STOPWATCH()
{
    //(see description of 'init_STOPWATCH()')
    TIM2->CR1 |= TIM_CR1_CEN;
}

uint32_t stop_STOPWATCH()
{
    uint32_t us_elapsed;
    

    //(see description of 'init_STOPWATCH()')
    TIM2->CR1 &= ~TIM_CR1_CEN; //stop counter
    
    //(reference manual pg.627)
    us_elapsed = TIM2->CNT;
    
    //(see description of 'init_STOPWATCH()')
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // disable TIM2 peripheral on APB1 bus

    return us_elapsed;
}





int main(int argc, const char *argv[])
{
    init_UART4();
    init_SystemTimer();
    
        
    char c = 0x0;
    
    //uint32_t elapsed_us;

    //init_STOPWATCH();
    //start_STOPWATCH();
    
    print_clock_stat(); 

    //elapsed_us = stop_STOPWATCH();

    //print_terminal("print_clock_status() executed for: %d us\r\n", elapsed_us);
    print_terminal("SystemTicks: %d\r\n", get_TIM2_ticks());

    //init_STOPWATCH();
    //start_STOPWATCH();
    //volatile unsigned short i = 0x1;
    //while(i++);
    //elapsed_us = stop_STOPWATCH();

    //print_terminal("loop executed for: %d us\r\n", elapsed_us);



    while(1)
    {
        print_terminal("[%d]\r\n", c);
        print_terminal("[%x]\r\n", c++);
        print_terminal("SystemTicks: %d\r\n", get_TIM2_ticks());
        wait_ms(1000);
    }
    return 0;
}
