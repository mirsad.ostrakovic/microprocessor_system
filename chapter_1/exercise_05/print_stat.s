	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"print_stat.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.section	.rodata
	.align	2
.LC0:
	.ascii	"system core clock: %d\015\012\000"
	.align	2
.LC1:
	.ascii	"AHB prescaler: %d\015\012\000"
	.align	2
.LC2:
	.ascii	"APB1 prescaler: %d\015\012\000"
	.align	2
.LC3:
	.ascii	"APB2 prescaler: %d\015\012\000"
	.align	2
.LC4:
	.ascii	"System clock: \000"
	.align	2
.LC5:
	.ascii	"HSI\000"
	.align	2
.LC6:
	.ascii	"HSE\000"
	.align	2
.LC7:
	.ascii	"PLL\000"
	.align	2
.LC8:
	.ascii	"not used\000"
	.align	2
.LC9:
	.ascii	"\015\012\000"
	.align	2
.LC10:
	.ascii	"AHB divider: %d\015\012\000"
	.align	2
.LC11:
	.ascii	"AHB clock: %d\015\012\000"
	.align	2
.LC12:
	.ascii	"APB1 divider: %d\015\012\000"
	.align	2
.LC13:
	.ascii	"APB1 clock: %d\015\012\000"
	.align	2
.LC14:
	.ascii	"APB2 divider: %d\015\012\000"
	.align	2
.LC15:
	.ascii	"APB2 clock: %d\015\012\000"
	.align	2
.LC16:
	.ascii	"PLLM: %d\015\012\000"
	.align	2
.LC17:
	.ascii	"PLLN: %d\015\012\000"
	.align	2
.LC18:
	.ascii	"PLLP: %d\015\012\000"
	.align	2
.LC19:
	.ascii	"PLLQ: %d\015\012\000"
	.align	2
.LC20:
	.ascii	"HSE clock selected for PLL source\015\012\000"
	.align	2
.LC21:
	.ascii	"HSE frequenty on F407 is 8MHz\015\012\000"
	.align	2
.LC22:
	.ascii	"HSI oscillator clock selected as PLL source\015\012"
	.ascii	"\000"
	.align	2
.LC23:
	.ascii	"TIMPRE = 1\015\012\000"
	.align	2
.LC24:
	.ascii	"timer on APB1 clock = HCLK\015\012\000"
	.align	2
.LC25:
	.ascii	"time on APB1 clock = 4 * APB1_clock == %d\015\012\000"
	.align	2
.LC26:
	.ascii	"timer on APB2 clock = HCLK\015\012\000"
	.align	2
.LC27:
	.ascii	"time on APB2 clock = 4 * APB2_clock == %d\015\012\000"
	.align	2
.LC28:
	.ascii	"TIMPRE = 0\015\012\000"
	.align	2
.LC29:
	.ascii	"timer on APB1 clock = APB1_clock == %d\015\012\000"
	.align	2
.LC30:
	.ascii	"timer on APB1 clock = 2 * APB1_clock == %d\015\012\000"
	.align	2
.LC31:
	.ascii	"timer on APB2 clock = APB2_clock == %d\015\012\000"
	.align	2
.LC32:
	.ascii	"timer on APB2 clock = 2 * APB2_clock == %d\015\012\000"
	.text
	.align	2
	.global	print_clock_stat
	.thumb
	.thumb_func
	.type	print_clock_stat, %function
print_clock_stat:
.LFB110:
	.file 1 "print_stat.c"
	.loc 1 6 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #32
	.cfi_def_cfa_offset 40
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 13 0
	ldr	r3, .L46
	ldr	r3, [r3]
	ldr	r0, .L46+4
	mov	r1, r3
	bl	print_terminal
	.loc 1 17 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #4
	and	r3, r3, #15
	ldr	r0, .L46+12
	mov	r1, r3
	bl	print_terminal
	.loc 1 18 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #10
	and	r3, r3, #7
	ldr	r0, .L46+16
	mov	r1, r3
	bl	print_terminal
	.loc 1 19 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #13
	and	r3, r3, #7
	ldr	r0, .L46+20
	mov	r1, r3
	bl	print_terminal
	.loc 1 21 0
	ldr	r0, .L46+24
	bl	print_terminal
	.loc 1 23 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #2
	and	r3, r3, #3
	cmp	r3, #3
	bhi	.L2
	adr	r2, .L4
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L4:
	.word	.L3+1
	.word	.L5+1
	.word	.L6+1
	.word	.L7+1
	.p2align 1
.L3:
	.loc 1 26 0
	ldr	r0, .L46+28
	bl	print_terminal
	.loc 1 27 0
	b	.L2
.L5:
	.loc 1 30 0
	ldr	r0, .L46+32
	bl	print_terminal
	.loc 1 31 0
	b	.L2
.L6:
	.loc 1 34 0
	ldr	r0, .L46+36
	bl	print_terminal
	.loc 1 35 0
	b	.L2
.L7:
	.loc 1 38 0
	ldr	r0, .L46+40
	bl	print_terminal
	.loc 1 39 0
	nop
.L2:
	.loc 1 42 0
	ldr	r0, .L46+44
	bl	print_terminal
	.loc 1 45 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #4
	and	r3, r3, #15
	cmp	r3, #15
	bhi	.L8
	adr	r2, .L10
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L10:
	.word	.L9+1
	.word	.L9+1
	.word	.L9+1
	.word	.L9+1
	.word	.L9+1
	.word	.L9+1
	.word	.L9+1
	.word	.L9+1
	.word	.L11+1
	.word	.L12+1
	.word	.L13+1
	.word	.L14+1
	.word	.L15+1
	.word	.L16+1
	.word	.L17+1
	.word	.L18+1
	.p2align 1
.L9:
	.loc 1 55 0
	movs	r3, #1
	str	r3, [r7, #28]
	.loc 1 56 0
	b	.L8
.L11:
	.loc 1 59 0
	movs	r3, #2
	str	r3, [r7, #28]
	.loc 1 60 0
	b	.L8
.L12:
	.loc 1 63 0
	movs	r3, #4
	str	r3, [r7, #28]
	.loc 1 64 0
	b	.L8
.L13:
	.loc 1 67 0
	movs	r3, #8
	str	r3, [r7, #28]
	.loc 1 68 0
	b	.L8
.L14:
	.loc 1 71 0
	movs	r3, #16
	str	r3, [r7, #28]
	.loc 1 72 0
	b	.L8
.L15:
	.loc 1 75 0
	movs	r3, #64
	str	r3, [r7, #28]
	.loc 1 76 0
	b	.L8
.L16:
	.loc 1 79 0
	movs	r3, #128
	str	r3, [r7, #28]
	.loc 1 80 0
	b	.L8
.L17:
	.loc 1 83 0
	mov	r3, #256
	str	r3, [r7, #28]
	.loc 1 84 0
	b	.L8
.L18:
	.loc 1 87 0
	mov	r3, #512
	str	r3, [r7, #28]
	.loc 1 88 0
	nop
.L8:
	.loc 1 92 0
	ldr	r0, .L46+48
	ldr	r1, [r7, #28]
	bl	print_terminal
	.loc 1 93 0
	ldr	r3, .L46
	ldr	r2, [r3]
	ldr	r3, [r7, #28]
	udiv	r3, r2, r3
	ldr	r0, .L46+52
	mov	r1, r3
	bl	print_terminal
	.loc 1 96 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #10
	and	r3, r3, #7
	cmp	r3, #7
	bhi	.L19
	adr	r2, .L21
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L21:
	.word	.L20+1
	.word	.L20+1
	.word	.L20+1
	.word	.L20+1
	.word	.L22+1
	.word	.L23+1
	.word	.L24+1
	.word	.L25+1
	.p2align 1
.L20:
	.loc 1 102 0
	movs	r3, #1
	str	r3, [r7, #24]
	.loc 1 103 0
	b	.L19
.L22:
	.loc 1 106 0
	movs	r3, #2
	str	r3, [r7, #24]
	.loc 1 107 0
	b	.L19
.L23:
	.loc 1 110 0
	movs	r3, #4
	str	r3, [r7, #24]
	.loc 1 111 0
	b	.L19
.L24:
	.loc 1 114 0
	movs	r3, #8
	str	r3, [r7, #24]
.L25:
	.loc 1 117 0
	movs	r3, #16
	str	r3, [r7, #24]
.L19:
	.loc 1 120 0
	ldr	r0, .L46+56
	ldr	r1, [r7, #24]
	bl	print_terminal
	.loc 1 121 0
	ldr	r3, .L46
	ldr	r2, [r3]
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #24]
	udiv	r3, r2, r3
	ldr	r0, .L46+60
	mov	r1, r3
	bl	print_terminal
	.loc 1 123 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #8]
	lsrs	r3, r3, #13
	and	r3, r3, #7
	cmp	r3, #7
	bhi	.L26
	adr	r2, .L28
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L28:
	.word	.L27+1
	.word	.L27+1
	.word	.L27+1
	.word	.L27+1
	.word	.L29+1
	.word	.L30+1
	.word	.L31+1
	.word	.L32+1
	.p2align 1
.L27:
	.loc 1 129 0
	movs	r3, #1
	str	r3, [r7, #20]
	.loc 1 130 0
	b	.L26
.L29:
	.loc 1 133 0
	movs	r3, #2
	str	r3, [r7, #20]
	.loc 1 134 0
	b	.L26
.L30:
	.loc 1 137 0
	movs	r3, #4
	str	r3, [r7, #20]
	.loc 1 138 0
	b	.L26
.L31:
	.loc 1 141 0
	movs	r3, #8
	str	r3, [r7, #20]
	.loc 1 142 0
	b	.L26
.L32:
	.loc 1 145 0
	movs	r3, #16
	str	r3, [r7, #20]
	.loc 1 146 0
	nop
.L26:
	.loc 1 149 0
	ldr	r0, .L46+64
	ldr	r1, [r7, #20]
	bl	print_terminal
	.loc 1 150 0
	ldr	r3, .L46
	ldr	r2, [r3]
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #20]
	udiv	r3, r2, r3
	ldr	r0, .L46+68
	mov	r1, r3
	bl	print_terminal
	.loc 1 152 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #4]
	and	r3, r3, #63
	str	r3, [r7, #16]
	.loc 1 153 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #4]
	lsrs	r3, r3, #6
	ubfx	r3, r3, #0, #9
	str	r3, [r7, #12]
	.loc 1 154 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #4]
	lsrs	r3, r3, #16
	and	r3, r3, #3
	str	r3, [r7, #8]
	.loc 1 155 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #4]
	lsrs	r3, r3, #24
	and	r3, r3, #15
	str	r3, [r7, #4]
	.loc 1 157 0
	ldr	r0, .L46+72
	ldr	r1, [r7, #16]
	bl	print_terminal
	.loc 1 158 0
	ldr	r0, .L46+76
	ldr	r1, [r7, #12]
	bl	print_terminal
	.loc 1 159 0
	ldr	r0, .L46+80
	ldr	r1, [r7, #8]
	bl	print_terminal
	.loc 1 160 0
	ldr	r0, .L46+84
	ldr	r1, [r7, #4]
	bl	print_terminal
	.loc 1 162 0
	ldr	r3, .L46+8
	ldr	r3, [r3, #4]
	lsrs	r3, r3, #22
	and	r3, r3, #1
	cmp	r3, #0
	beq	.L33
	.loc 1 164 0
	ldr	r0, .L46+88
	bl	print_terminal
	.loc 1 165 0
	ldr	r0, .L46+92
	bl	print_terminal
	b	.L34
.L33:
	.loc 1 168 0
	ldr	r0, .L46+96
	bl	print_terminal
.L34:
	.loc 1 172 0
	ldr	r3, .L46+100
	ldr	r3, [r3]
	lsrs	r3, r3, #24
	and	r3, r3, #1
	cmp	r3, #0
	beq	.L35
	.loc 1 174 0
	ldr	r0, .L46+104
	bl	print_terminal
	.loc 1 177 0
	ldr	r3, [r7, #24]
	cmp	r3, #1
	beq	.L36
	.loc 1 177 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #24]
	cmp	r3, #2
	beq	.L36
	.loc 1 177 0 discriminator 2
	ldr	r3, [r7, #24]
	cmp	r3, #4
	bne	.L37
.L36:
	.loc 1 178 0 is_stmt 1
	ldr	r0, .L46+108
	bl	print_terminal
	b	.L38
.L47:
	.align	2
.L46:
	.word	SystemCoreClock
	.word	.LC0
	.word	1073887232
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	1073887792
	.word	.LC23
	.word	.LC24
.L37:
	.loc 1 180 0
	ldr	r3, .L48
	ldr	r3, [r3]
	lsls	r2, r3, #2
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #24]
	udiv	r3, r2, r3
	ldr	r0, .L48+4
	mov	r1, r3
	bl	print_terminal
.L38:
	.loc 1 183 0
	ldr	r3, [r7, #20]
	cmp	r3, #1
	beq	.L39
	.loc 1 183 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #20]
	cmp	r3, #2
	beq	.L39
	.loc 1 183 0 discriminator 2
	ldr	r3, [r7, #20]
	cmp	r3, #4
	bne	.L40
.L39:
	.loc 1 184 0 is_stmt 1
	ldr	r0, .L48+8
	bl	print_terminal
	b	.L1
.L40:
	.loc 1 186 0
	ldr	r3, .L48
	ldr	r3, [r3]
	lsls	r2, r3, #2
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #20]
	udiv	r3, r2, r3
	ldr	r0, .L48+12
	mov	r1, r3
	bl	print_terminal
	b	.L1
.L35:
	.loc 1 190 0
	ldr	r0, .L48+16
	bl	print_terminal
	.loc 1 193 0
	ldr	r3, [r7, #24]
	cmp	r3, #1
	bne	.L43
	.loc 1 194 0
	ldr	r3, .L48
	ldr	r2, [r3]
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #24]
	udiv	r3, r2, r3
	ldr	r0, .L48+20
	mov	r1, r3
	bl	print_terminal
	b	.L44
.L43:
	.loc 1 196 0
	ldr	r3, .L48
	ldr	r3, [r3]
	lsls	r2, r3, #1
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #24]
	udiv	r3, r2, r3
	ldr	r0, .L48+24
	mov	r1, r3
	bl	print_terminal
.L44:
	.loc 1 199 0
	ldr	r3, [r7, #20]
	cmp	r3, #1
	bne	.L45
	.loc 1 200 0
	ldr	r3, .L48
	ldr	r2, [r3]
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #20]
	udiv	r3, r2, r3
	ldr	r0, .L48+28
	mov	r1, r3
	bl	print_terminal
	b	.L1
.L45:
	.loc 1 202 0
	ldr	r3, .L48
	ldr	r3, [r3]
	lsls	r2, r3, #1
	ldr	r3, [r7, #28]
	udiv	r2, r2, r3
	ldr	r3, [r7, #20]
	udiv	r3, r2, r3
	ldr	r0, .L48+32
	mov	r1, r3
	bl	print_terminal
.L1:
	.loc 1 206 0
	adds	r7, r7, #32
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L49:
	.align	2
.L48:
	.word	SystemCoreClock
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.cfi_endproc
.LFE110:
	.size	print_clock_stat, .-print_clock_stat
.Letext0:
	.file 2 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "../../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.file 6 "../../../../STM32F407/Libraries/CMSIS/ST/STM32F4xx/Include/system_stm32f4xx.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2ec
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF51
	.byte	0x1
	.4byte	.LASF52
	.4byte	.LASF53
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3f
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x41
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x3
	.byte	0x2c
	.4byte	0x41
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x3
	.byte	0x2d
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x5
	.4byte	0x8c
	.uleb128 0x6
	.4byte	0x8c
	.4byte	0xb3
	.uleb128 0x7
	.4byte	0x97
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x81
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x247
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0x9e
	.byte	0
	.uleb128 0xa
	.4byte	.LASF14
	.byte	0x4
	.2byte	0x2e0
	.4byte	0x9e
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF15
	.byte	0x4
	.2byte	0x2e1
	.4byte	0x9e
	.byte	0x8
	.uleb128 0x9
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0x9e
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF16
	.byte	0x4
	.2byte	0x2e3
	.4byte	0x9e
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x4
	.2byte	0x2e4
	.4byte	0x9e
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x2e5
	.4byte	0x9e
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x2e6
	.4byte	0x8c
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x2e7
	.4byte	0x9e
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x2e8
	.4byte	0x9e
	.byte	0x24
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xa3
	.byte	0x28
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x2ea
	.4byte	0x9e
	.byte	0x30
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x2eb
	.4byte	0x9e
	.byte	0x34
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x2ec
	.4byte	0x9e
	.byte	0x38
	.uleb128 0xa
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x2ed
	.4byte	0x8c
	.byte	0x3c
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x2ee
	.4byte	0x9e
	.byte	0x40
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2ef
	.4byte	0x9e
	.byte	0x44
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xa3
	.byte	0x48
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2f1
	.4byte	0x9e
	.byte	0x50
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2f2
	.4byte	0x9e
	.byte	0x54
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2f3
	.4byte	0x9e
	.byte	0x58
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2f4
	.4byte	0x8c
	.byte	0x5c
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2f5
	.4byte	0x9e
	.byte	0x60
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2f6
	.4byte	0x9e
	.byte	0x64
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xa3
	.byte	0x68
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2f8
	.4byte	0x9e
	.byte	0x70
	.uleb128 0x9
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0x9e
	.byte	0x74
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xa3
	.byte	0x78
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2fb
	.4byte	0x9e
	.byte	0x80
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2fc
	.4byte	0x9e
	.byte	0x84
	.byte	0
	.uleb128 0xb
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2fd
	.4byte	0xb8
	.uleb128 0xc
	.4byte	.LASF54
	.byte	0x1
	.byte	0x5
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x2d8
	.uleb128 0xd
	.4byte	.LASF42
	.byte	0x1
	.byte	0x8
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.4byte	.LASF43
	.byte	0x1
	.byte	0x8
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.4byte	.LASF44
	.byte	0x1
	.byte	0x8
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.4byte	.LASF45
	.byte	0x1
	.byte	0xa
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xd
	.4byte	.LASF46
	.byte	0x1
	.byte	0xa
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xd
	.4byte	.LASF47
	.byte	0x1
	.byte	0xa
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0x1
	.byte	0xa
	.4byte	0x73
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xe
	.4byte	.LASF55
	.byte	0x1
	.byte	0xd
	.4byte	0x73
	.uleb128 0xf
	.byte	0
	.byte	0
	.uleb128 0x10
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x51b
	.4byte	0xb3
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x6
	.byte	0x35
	.4byte	0x8c
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF36:
	.ascii	"RESERVED5\000"
.LASF37:
	.ascii	"BDCR\000"
.LASF23:
	.ascii	"AHB1ENR\000"
.LASF39:
	.ascii	"SSCGR\000"
.LASF5:
	.ascii	"__int32_t\000"
.LASF30:
	.ascii	"AHB1LPENR\000"
.LASF43:
	.ascii	"APB1_divider\000"
.LASF18:
	.ascii	"AHB3RSTR\000"
.LASF19:
	.ascii	"RESERVED0\000"
.LASF22:
	.ascii	"RESERVED1\000"
.LASF26:
	.ascii	"RESERVED2\000"
.LASF29:
	.ascii	"RESERVED3\000"
.LASF33:
	.ascii	"RESERVED4\000"
.LASF28:
	.ascii	"APB2ENR\000"
.LASF38:
	.ascii	"RESERVED6\000"
.LASF24:
	.ascii	"AHB2ENR\000"
.LASF54:
	.ascii	"print_clock_stat\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF31:
	.ascii	"AHB2LPENR\000"
.LASF51:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF49:
	.ascii	"ITM_RxBuffer\000"
.LASF14:
	.ascii	"PLLCFGR\000"
.LASF7:
	.ascii	"long unsigned int\000"
.LASF25:
	.ascii	"AHB3ENR\000"
.LASF20:
	.ascii	"APB1RSTR\000"
.LASF6:
	.ascii	"__uint32_t\000"
.LASF45:
	.ascii	"PLLN\000"
.LASF27:
	.ascii	"APB1ENR\000"
.LASF47:
	.ascii	"PLLP\000"
.LASF48:
	.ascii	"PLLQ\000"
.LASF44:
	.ascii	"APB2_divider\000"
.LASF21:
	.ascii	"APB2RSTR\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF40:
	.ascii	"PLLI2SCFGR\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF34:
	.ascii	"APB1LPENR\000"
.LASF52:
	.ascii	"print_stat.c\000"
.LASF46:
	.ascii	"PLLM\000"
.LASF15:
	.ascii	"CFGR\000"
.LASF16:
	.ascii	"AHB1RSTR\000"
.LASF11:
	.ascii	"int32_t\000"
.LASF13:
	.ascii	"sizetype\000"
.LASF32:
	.ascii	"AHB3LPENR\000"
.LASF8:
	.ascii	"long long int\000"
.LASF42:
	.ascii	"AHB_divider\000"
.LASF50:
	.ascii	"SystemCoreClock\000"
.LASF55:
	.ascii	"print_terminal\000"
.LASF2:
	.ascii	"short int\000"
.LASF35:
	.ascii	"APB2LPENR\000"
.LASF41:
	.ascii	"RCC_TypeDef\000"
.LASF12:
	.ascii	"uint32_t\000"
.LASF4:
	.ascii	"long int\000"
.LASF17:
	.ascii	"AHB2RSTR\000"
.LASF0:
	.ascii	"signed char\000"
.LASF53:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/chapter_1/exercise_05\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
