#include "print_stat.h"



void print_clock_stat()
{

    int AHB_divider, APB1_divider, APB2_divider;
  
    int PLLN, PLLM, PLLP, PLLQ;


    print_terminal("system core clock: %d\r\n", SystemCoreClock);

    // CFGR - clock configuration register
    // 
    print_terminal("AHB prescaler: %d\r\n", (RCC->CFGR >> 4) & 0xF); 
    print_terminal("APB1 prescaler: %d\r\n", (RCC->CFGR >> 10) & 0x7);
    print_terminal("APB2 prescaler: %d\r\n", (RCC->CFGR >> 13) & 0x7);
    
    print_terminal("System clock: ");

    switch((RCC->CFGR >> 2) & 0x3)
    {
        case 0x0:
            print_terminal("HSI");
            break;

        case 0x1:
            print_terminal("HSE");
            break;

        case 0x2:
            print_terminal("PLL");
            break;

        case 0x3:
            print_terminal("not used");
            break;
    }

    print_terminal("\r\n");

 
    switch((RCC->CFGR >> 4) & 0xF)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            AHB_divider = 1;
            break;

        case 8:
            AHB_divider = 2;
            break;

        case 9:
            AHB_divider = 4;
            break;

        case 10:
            AHB_divider = 8;
            break;

        case 11:
            AHB_divider = 16;
            break;

        case 12:
            AHB_divider = 64;
            break;

        case 13:
            AHB_divider = 128;
            break;

        case 14:
            AHB_divider = 256;
            break;

        case 15:
            AHB_divider = 512;
            break;

    }

    print_terminal("AHB divider: %d\r\n", AHB_divider);
    print_terminal("AHB clock: %d\r\n", SystemCoreClock / AHB_divider);


    switch((RCC->CFGR >> 10) & 0x7)
    {
        case 0:
        case 1:
        case 2:
        case 3:
            APB1_divider = 1;
            break;

        case 4:
            APB1_divider = 2;
            break;

        case 5:
            APB1_divider = 4;
            break;

        case 6:
            APB1_divider = 8;

        case 7:
            APB1_divider = 16;
    }
    
    print_terminal("APB1 divider: %d\r\n", APB1_divider);
    print_terminal("APB1 clock: %d\r\n", SystemCoreClock / AHB_divider / APB1_divider);

    switch((RCC->CFGR >> 13) & 0x7)
    {
        case 0:
        case 1:
        case 2:
        case 3:
            APB2_divider = 1;       
            break;

        case 4:
            APB2_divider = 2;
            break;

        case 5:
            APB2_divider = 4;
            break;

        case 6:
            APB2_divider = 8;
            break;
        
        case 7:
            APB2_divider = 16;
            break;
    }

    print_terminal("APB2 divider: %d\r\n", APB2_divider);
    print_terminal("APB2 clock: %d\r\n",  SystemCoreClock / AHB_divider / APB2_divider);

    PLLM = RCC->PLLCFGR & 0x3F;
    PLLN = (RCC->PLLCFGR >> 6) & 0x1FF;
    PLLP = (RCC->PLLCFGR >> 16) & 0x3;
    PLLQ = (RCC->PLLCFGR >> 24) & 0xF;

    print_terminal("PLLM: %d\r\n", PLLM);
    print_terminal("PLLN: %d\r\n", PLLN);
    print_terminal("PLLP: %d\r\n", PLLP);
    print_terminal("PLLQ: %d\r\n", PLLQ);

    if((RCC->PLLCFGR >> 22) & 0x1)
    {
        print_terminal("HSE clock selected for PLL source\r\n");
        print_terminal("HSE frequenty on F407 is 8MHz\r\n"); 
    }
    else
        print_terminal("HSI oscillator clock selected as PLL source\r\n");
   
  
    // RCC_DCKCFGR addresss offset is 0x8c
    if((*(((uint32_t *)(RCC_BASE)) + 0x8c) >> 24) & 0x1)
    {
        print_terminal("TIMPRE = 1\r\n");
        
        
        if(APB1_divider == 1 || APB1_divider == 2 || APB1_divider == 4)
            print_terminal("timer on APB1 clock = HCLK\r\n");
        else
            print_terminal("time on APB1 clock = 4 * APB1_clock == %d\r\n", 4 * SystemCoreClock / AHB_divider / APB1_divider);
        
       
        if(APB2_divider == 1 || APB2_divider == 2 || APB2_divider == 4)
            print_terminal("timer on APB2 clock = HCLK\r\n");
        else
            print_terminal("time on APB2 clock = 4 * APB2_clock == %d\r\n", 4 * SystemCoreClock / AHB_divider / APB2_divider);
    }
    else
    {
        print_terminal("TIMPRE = 0\r\n");
        
        
        if(APB1_divider == 1)
            print_terminal("timer on APB1 clock = APB1_clock == %d\r\n", SystemCoreClock / AHB_divider / APB1_divider);
        else
            print_terminal("timer on APB1 clock = 2 * APB1_clock == %d\r\n", 2 * SystemCoreClock / AHB_divider / APB1_divider);


        if(APB2_divider == 1)
            print_terminal("timer on APB2 clock = APB2_clock == %d\r\n", SystemCoreClock / AHB_divider / APB2_divider);
        else
            print_terminal("timer on APB2 clock = 2 * APB2_clock == %d\r\n", 2 * SystemCoreClock / AHB_divider / APB2_divider);
    
    }

}

