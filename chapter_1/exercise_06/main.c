#include "stm32f4xx.h"


uint32_t timer1_value;
uint32_t timer2_value;

void restart_timer1()
{
    timer1_value = 0;
}

void restart_timer2()
{
    timer2_value = 0;
}

void increment_timer1()
{
    ++timer1_value;
}


void increment_timer2()
{
    ++timer2_value;
}

uint32_t get_timer1_value()
{
    return timer1_value;
}

uint32_t get_timer2_value()
{
    return timer2_value;
}






void init_UART4()
{

    // enable GPIOC module on AHB bus
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;     

    // enable UART4 module on APB1 bus
    RCC->APB1ENR |= RCC_APB1ENR_UART4EN;


    GPIOC->MODER &= 0xFF0FFFFF; // reset pins 10 and 11 mode
    GPIOC->MODER |= 0x00A00000; // set pins 10 and 11 mode to '10' - alternate function

    GPIOC->OTYPER &= 0xF3FF; // reset pins 10 and 11 output type to '0' - push-pull

    GPIOC->OSPEEDR &= 0xFF0FFFFF; // reset pins 10 and 11 output speed
    GPIOC->OSPEEDR |= 0x00A00000; // set pins 10 and 11 mode to '10' - fast speed

    GPIOC->PUPDR &= 0xFF0FFFFF; // reset pins 10 and 11 pull-up/pull-down configuration
    GPIOC->PUPDR |= 0x00500000; // set pins 10 and 11 pu/pd value to '01' - pull-up

    GPIOC->AFR[1] &= 0xFFFF00FF; // reset alternate function value of pins 10 and 11
    GPIOC->AFR[1] |= 0x00008800; // set alternate function value of pins 10 and 11 to '8' - AF8



    UART4->CR1 |=  USART_CR1_TE | USART_CR1_UE;
    
    // for APB1 clk_freq = 42MHz and OVER8 = 0 and baud_rate = 115200 Bps,  BRR_value = 22.8125
    // BRR[3:0]  ->  DIV_fraction
    // BRR[15:4] ->  DIV_mantissa
    UART4->BRR = 0x016C;

}

void print_char(char c) 
{
    while((((volatile USART_TypeDef *)UART4)->SR & USART_SR_TC) != USART_SR_TC);
    UART4->DR = c;
}


void print_string(const char *string)
{
    while(*string != '\0')
        print_char(*(string++));
}

print_number(int val)
{
    char buff[32];
    int8_t idx = 0u;
    uint8_t minus_flag = val < 0;

    if(val == 0)
    {
        print_char('0');
    }

    while(val)
    {
        buff[idx++] = '0' +  val % 10;
        val /= 10;
    }

    while(idx != -1)
    {
        print_char(buff[--idx]);
    }

}


int main(int argc, const char *argv[])
{

    init_UART4();

    uint32_t delay = 1000u; 
    int8_t direction = -1;  
    uint8_t is_elapsed_time = 0x1; 
    uint8_t old_input_val = 0x0;

    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOAEN; 



    // setup output pin
    GPIOD->MODER &= 0x3FFFFFFF;
    GPIOD->MODER |= 0x40000000; // GPIOD pin15 set as output

    GPIOD->OTYPER &= 0x3FFFFFFF; // GPIOD pin15 set in push-pull configuration

    GPIOD->OSPEEDR &= 0x3FFFFFFF; 
    GPIOD->OSPEEDR |= 0x80000000; // GPIOD speed set to 10 - high speed

    GPIOD->PUPDR &= 0x3FFFFFFF;


    // setup input pin
    GPIOA->MODER &= 0xFFFFFFFC;

    GPIOA->PUPDR &= 0xFFFFFFFC;

    //GPIOD->PUPDR |= 0x1; // pull-up
    //GPIOD->PUPDR |= 0x2; // pull-down

    restart_timer1();

    while(1)
    {

        if(is_elapsed_time)
        {
            if(GPIOA->IDR & 0x1)
            {
                is_elapsed_time = 0; 
                 restart_timer2(); 
            
                old_input_val = 0x1; 
            }
            else
            {
                if(old_input_val)
                {
                    print_string("button clicked\r\n");

                    delay += direction * 50;
                    if(delay < 500u || delay > 1500u)
                        direction = -direction;

                    print_string("new delay: ");
                    print_number(delay);
                    print_string("\r\n");
                }

                old_input_val = 0x0;
            }
        }
        else
        {
            if(100000 < get_timer2_value())
                is_elapsed_time = 1;
            
            increment_timer2(); 
        }


        if(delay * 1000 < get_timer1_value())
        {
            restart_timer1();
            GPIOD->ODR ^= 0x8000; 
        }
       
        increment_timer1();

    }


    return 0;
}
