#ifndef PRINT_H
#define PRINT_H

//#include "stm32f4xx.h"
//#include <stdarg.h>

extern void print_char(char c);

void print(const char *fmt, ...);


void print_string(const char *str);
void print_integer(int val);

#endif
