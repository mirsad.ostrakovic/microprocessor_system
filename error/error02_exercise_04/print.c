#include "print.h"


void print_string(const char *str)
{
    while(*str != '\0')
        print_char(*(str++));
}

void print_integer(int val)
{
    char buffer[32];
    int idx = -1;
    int tmp;

    if(val < 0)
    {
        buffer[++idx] = '-';
        val = -val;
    }

    tmp = val;

    while(tmp)
    {
        tmp /= 10;
        idx++;
    }

    buffer[idx+1] = '\0';
    
    while(val)
    {
        buffer[idx--] = '0' + val % 10; 
        val /= 10;
    }

    print_string(buffer);

}

void print(const char *fmt, ...)
{

//    va_list ap;
//    va_start(ap, fmt);

    unsigned char flag = 0u;

    print_char('#');
    print_char('#');
    print_char(*fmt);
    print_char(*fmt);


    while(*fmt != '\0');
    {
        print_char(*(fmt++));

        /*
        switch(*fmt)
        {    
            case '%':
                flag = 1u; 
                ++fmt; 
                break;

            case 'd':
                if(flag)
                {
                    int arg = va_arg(ap, int);
                    flag = 0u;
                    print_integer(arg);
                    fmt++;
                }
                else
                    putchar_(*(fmt++));
                break;        
            default:
                putchar_(*(fmt++));
        }
    */

    }

//    va_end(ap);
}

