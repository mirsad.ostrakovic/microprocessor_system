	.cpu arm7tdmi
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"print.c"
	.text
	.align	2
	.global	print_string
	.syntax unified
	.arm
	.fpu softvfp
	.type	print_string, %function
print_string:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	b	.L2
.L3:
	ldr	r3, [fp, #-8]
	add	r2, r3, #1
	str	r2, [fp, #-8]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
.L2:
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L3
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.size	print_string, .-print_string
	.align	2
	.global	print_integer
	.syntax unified
	.arm
	.fpu softvfp
	.type	print_integer, %function
print_integer:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #48
	str	r0, [fp, #-48]
	mvn	r3, #0
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	bge	.L5
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	sub	r2, fp, #44
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	mov	r2, #45
	strb	r2, [r3]
	ldr	r3, [fp, #-48]
	rsb	r3, r3, #0
	str	r3, [fp, #-48]
.L5:
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	b	.L6
.L7:
	ldr	r3, [fp, #-12]
	ldr	r2, .L10
	smull	r1, r2, r3, r2
	asr	r2, r2, #2
	asr	r3, r3, #31
	sub	r3, r2, r3
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L6:
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L7
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	sub	r2, fp, #4
	add	r3, r2, r3
	mov	r2, #0
	strb	r2, [r3, #-40]
	b	.L8
.L9:
	ldr	r0, [fp, #-8]
	sub	r3, r0, #1
	str	r3, [fp, #-8]
	ldr	r1, [fp, #-48]
	ldr	r3, .L10
	smull	r2, r3, r1, r3
	asr	r2, r3, #2
	asr	r3, r1, #31
	sub	r2, r2, r3
	mov	r3, r2
	lsl	r3, r3, #2
	add	r3, r3, r2
	lsl	r3, r3, #1
	sub	r2, r1, r3
	and	r3, r2, #255
	add	r3, r3, #48
	and	r2, r3, #255
	sub	r3, fp, #4
	add	r3, r3, r0
	strb	r2, [r3, #-40]
	ldr	r3, [fp, #-48]
	ldr	r2, .L10
	smull	r1, r2, r3, r2
	asr	r2, r2, #2
	asr	r3, r3, #31
	sub	r3, r2, r3
	str	r3, [fp, #-48]
.L8:
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	bne	.L9
	sub	r3, fp, #44
	mov	r0, r3
	bl	print_string
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
.L11:
	.align	2
.L10:
	.word	1717986919
	.size	print_integer, .-print_integer
	.align	2
	.global	print
	.syntax unified
	.arm
	.fpu softvfp
	.type	print, %function
print:
	@ Function supports interworking.
	@ args = 4, pretend = 16, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 1
	push	{r0, r1, r2, r3}
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	mov	r3, #0
	strb	r3, [fp, #-5]
	mov	r0, #35
	bl	print_char
	mov	r0, #35
	bl	print_char
	ldr	r3, [fp, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
	ldr	r3, [fp, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
	nop
.L13:
	ldr	r3, [fp, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L13
	ldr	r3, [fp, #4]
	add	r2, r3, #1
	str	r2, [fp, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	add	sp, sp, #16
	bx	lr
	.size	print, .-print
	.ident	"GCC: (15:6.3.1+svn253039-1build1) 6.3.1 20170620"
