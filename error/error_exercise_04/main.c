#include "stm32f4xx.h"
#include "print.h"

// UART4
// TX - PC10
// RX - PC11
// AF8

void init_UART4()
{

    // enable GPIOC module on AHB bus
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;     

    // enable UART4 module on APB1 bus
    RCC->APB1ENR |= RCC_APB1ENR_UART4EN;


    GPIOC->MODER &= 0xFF0FFFFF; // reset pins 10 and 11 mode
    GPIOC->MODER |= 0x00A00000; // set pins 10 and 11 mode to '10' - alternate function

    GPIOC->OTYPER &= 0xF3FF; // reset pins 10 and 11 output type to '0' - push-pull

    GPIOC->OSPEEDR &= 0xFF0FFFFF; // reset pins 10 and 11 output speed
    GPIOC->OSPEEDR |= 0x00A00000; // set pins 10 and 11 mode to '10' - fast speed

    GPIOC->PUPDR &= 0xFF0FFFFF; // reset pins 10 and 11 pull-up/pull-down configuration
    GPIOC->PUPDR |= 0x00500000; // set pins 10 and 11 pu/pd value to '01' - pull-up

    GPIOC->AFR[1] &= 0xFFFF00FF; // reset alternate function value of pins 10 and 11
    GPIOC->AFR[1] |= 0x00008800; // set alternate function value of pins 10 and 11 to '8' - AF8



    UART4->CR1 |=  USART_CR1_TE | USART_CR1_UE;
    
    // for APB1 clk_freq = 42MHz and OVER8 = 0 and baud_rate = 115200 Bps,  BRR_value = 22.8125
    // BRR[3:0]  ->  DIV_fraction
    // BRR[15:4] ->  DIV_mantissa
    UART4->BRR = 0x016C;

}

void print_char(char c)
{
    while((UART4->SR & USART_SR_TC) != USART_SR_TC);
    UART4->DR = c;
}




void delay_ms(uint16_t ms)
{
    volatile int i = 100000;
    while(i--);
}

#define print_string(STR) print_string_(STR)
#define true 1

int main(int argc, const char *argv[])
{
    init_UART4();

    char c = 0x0;



    print_integer(12345);
    print_string("print_str");
    print_string("---@----\r\n");
   // print("---------------\r\n");

    while(true)
    {
     //   print("[%d]\r\n", c);
        delay_ms(100);
    }
    return 0;
}
